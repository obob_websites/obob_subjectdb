# simple makefile to simplify repetetive build env management tasks under posix

# caution: testing won't work on windows, see README

flake:
	@if command -v flake8 > /dev/null; then \
		echo "Running flake8"; \
		flake8 --count mridb misc mridb obob_subjectdb subjectdb\
		th_bootstrap th_django user_manager; \
	else \
		echo "flake8 not found, please install it!"; \
		exit 1; \
	fi;
	@echo "flake8 passed"

code_quality:
	docker run --interactive --tty --rm --env \
	CODECLIMATE_CODE="$(CURDIR)" --volume "$(CURDIR)":/code \
	--volume /var/run/docker.sock:/var/run/docker.sock \
	--volume /tmp/cc:/tmp/cc  codeclimate/codeclimate analyze

update_deps:
	pip install -U --only-binary numpy -r requirements.txt
	npm install

webpack_dev:
	 npx webpack --watch -c webpack.dev.js

webpack_prod:
	 npx webpack -c webpack.prod.js

create_venv:
	mamba create -c conda-forge -p ./.venv python openldap python-ldap

start_testldap:
	cd ldap_test; ./create_all.sh
