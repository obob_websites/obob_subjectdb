import os
from pathlib import Path

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'obob_subjectdb.settings')

import django  # noqa E402

django.setup()

from mridb import models  # noqa E402

for cur_subject in models.Subject.objects.all():
    for cur_mri in cur_subject.mri_set.all():
        if not Path(cur_mri.mri_file.full_file_path).exists():
            print(f'Missing file for subject {cur_subject.subject_id}: {cur_mri.mri_file.file_name}')
