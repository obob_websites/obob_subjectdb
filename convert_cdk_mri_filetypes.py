import os.path

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'obob_subjectdb.settings')
import django

django.setup()
import zipfile  # noqa E402

from mridb import models  # noqa E402


def convert_cdk_mri_filetypes():
    all_nifti_mris = models.MRI.objects.filter(file_type__file_type='NIFTI')
    for cur_mri in all_nifti_mris:
        with zipfile.ZipFile(cur_mri.mri_file.full_file_path) as cur_mri_zip:
            all_files = cur_mri_zip.namelist()
            if len(all_files) == 1 and all_files[0] == 'mri.nii':
                cur_mri.file_type = models.FileType.objects.get_or_create(file_type='Converted CDK MRI')[0]
                cur_mri.save()


if __name__ == '__main__':
    convert_cdk_mri_filetypes()
