import pathlib
import tarfile
import tempfile

import nibabel
import numpy as np
import pydicom
from dicom2nifti.convert_dicom import dicom_array_to_nifti

src_fname = pathlib.Path('/home/th/temp/RSNE17_s043_19960418gbsh.tar.xz')
target_fname = pathlib.Path('test_datasets/cdktarxz_norm/cdktarxz_norm.tar.xz')
nifti_out_folder = pathlib.Path('test_datasets/converted_cdktarxz_norm')

nifti_out_folder.mkdir(parents=True, exist_ok=True)

ds_anonym_fields = [
    'InstitutionAddress',
    'AcquisitionDate',
    'AcquisitionTime',
    'ContentDate',
    'ContentTime',
    'InstanceCreationDate',
    'InstanceCreationTime',
    'PatientID',
    'PatientName',
    'PatientSex',
    'PatientSize',
    'PatientWeight',
    'PerformingPhysicianName',
    'ReferringPhysicianName',
    'PatientBirthDate',
    'PatientAge',
]

dicom_folder_name = 'testpat'
all_dicoms = list()
flip_axis = 0

with tarfile.open(src_fname, 'r:xz') as src, tempfile.TemporaryDirectory() as target_folder:
    target_folder_path = pathlib.Path(target_folder)
    n_files = len(src.getnames())
    for idx_file, cur_file in enumerate(src):
        if (idx_file % 100) == 0:
            print(f'Working on file {idx_file}/{n_files}')
        if cur_file.isfile():
            with src.extractfile(cur_file) as ds_file:
                ds = pydicom.read_file(ds_file)
                if ds.SeriesDescription in (
                    'MPRAGE_p2_1mmiso',
                    'MPRAGE_1iso_2p',
                    'OPTWANG2014_MPRAGE_1iso_2p',
                    'MPRAGE_p2',
                    'HCPLIFE_T1w_MPR1_vx08',
                    'T1w_MPR_HCPLIFE',
                ):
                    ds_file.seek(0)
                    if 'NORM' in ds.ImageType:
                        all_dicoms.append(ds)
                    pixel_data = np.ones(shape=ds.pixel_array.shape, dtype=ds.pixel_array.dtype) * idx_file
                    ds.PixelData = pixel_data.tobytes()
                    for cur_field in ds_anonym_fields:
                        delattr(ds, cur_field)
                    dicom_folder_path = target_folder_path / dicom_folder_name
                    dicom_folder_path.mkdir(parents=True, exist_ok=True)
                    pure_name = pathlib.Path(cur_file.name).name
                    ds.save_as(dicom_folder_path / pure_name)

    with tarfile.open(target_fname, 'w:xz') as target_file:
        target_file.add(dicom_folder_path, arcname=dicom_folder_name)

    dicom_array_to_nifti(all_dicoms, nifti_out_folder / 'mri_tmp.nii', reorient_nifti=True)
    nifti = nibabel.load(nifti_out_folder / 'mri_tmp.nii')
    nifti_data = nibabel.orientations.flip_axis(nifti.get_fdata(), flip_axis)
    nifti_affine = nifti.affine.copy()
    nifti_affine[flip_axis, :] = -nifti_affine[flip_axis, :]

    nifti_new = nibabel.Nifti1Image(nifti_data, nifti_affine, nifti.header)
    nifti_new.to_filename(nifti_out_folder / 'mri.nii')

    (nifti_out_folder / 'mri_tmp.nii').unlink()
