import pathlib
import tarfile

import nibabel
import numpy as np
from dicom2nifti.convert_dicom import dicom_array_to_nifti

from mridb.utils.mri import _process_tarfile

src_fname = pathlib.Path('test_datasets/cdktarxz_norm/cdktarxz_norm.tar.xz')
nifti_out_folder = pathlib.Path('test_datasets/converted_cdktarxz_norm')

nifti_out_folder.mkdir(parents=True, exist_ok=True)

ds_anonym_fields = [
    'InstitutionAddress',
    'AcquisitionDate',
    'AcquisitionTime',
    'ContentDate',
    'ContentTime',
    'InstanceCreationDate',
    'InstanceCreationTime',
    'PatientID',
    'PatientName',
    'PatientSex',
    'PatientSize',
    'PatientWeight',
    'PerformingPhysicianName',
    'ReferringPhysicianName',
    'PatientBirthDate',
    'PatientAge',
]

dicom_folder_name = 'testpat'
flip_axis = 0

with tarfile.open(src_fname) as dicom_tar:
    all_dicoms = _process_tarfile(dicom_tar)

dicom_array_to_nifti(all_dicoms, nifti_out_folder / 'mri_tmp.nii', reorient_nifti=True)
nifti = nibabel.load(nifti_out_folder / 'mri_tmp.nii')
nifti_data = nibabel.orientations.flip_axis(np.asanyarray(nifti.dataobj), flip_axis)
nifti_affine = nifti.affine.copy()
nifti_affine[flip_axis, :] = -nifti_affine[flip_axis, :]

nifti_new = nibabel.Nifti1Image(nifti_data, nifti_affine, nifti.header)
nifti_new.to_filename(nifti_out_folder / 'mri.nii')

(nifti_out_folder / 'mri_tmp.nii').unlink()
