#!/usr/bin/env bash

# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

if ! [ -f $OBOB_SUBJECTDB_KEY_DIR/obob_subjectdb.crt ] && ! [ -f $OBOB_SUBJECTDB_KEY_DIR/obob_subjectdb.key ]; then
    openssl req -x509 -newkey rsa:2048 -passout pass:none -out $OBOB_SUBJECTDB_KEY_DIR/obob_subjectdb.crt -keyout $OBOB_SUBJECTDB_KEY_DIR/obob_subjectdb.key -subj '/CN=www.test.com'
    openssl rsa -in $OBOB_SUBJECTDB_KEY_DIR/obob_subjectdb.key -out $OBOB_SUBJECTDB_KEY_DIR/obob_subjectdb.key -passin pass:none
fi

if ! [ -f $OBOB_SUBJECTDB_KEY_DIR/django_secret_key ]; then
    openssl rand -base64 50 | tr -d '\n' > $OBOB_SUBJECTDB_KEY_DIR/django_secret_key
fi

if ! [ -f $OBOB_SUBJECTDB_KEY_DIR/dhparam.pem ]; then
    openssl dhparam -out $OBOB_SUBJECTDB_KEY_DIR/dhparam.pem 2048
fi

./manage.py migrate && ./manage.py crontab add && ./manage.py updatedata && ./manage.py get_js_packages