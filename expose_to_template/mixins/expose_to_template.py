# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.


class ExposeContextItem:
    def __init__(self, tag, description, getfun_or_field=None, is_valid=True, get_from_context=False):
        self.tag = tag
        self.description = description

        if getfun_or_field is None:
            getfun_or_field = tag

        if isinstance(getfun_or_field, str):
            if get_from_context:
                self.getfun = lambda o, c: c.get(getfun_or_field, '')
            else:
                self.getfun = lambda o, c: getattr(o, getfun_or_field, '')
        else:
            self.getfun = getfun_or_field

        if callable(is_valid):
            self._is_valid = is_valid
        else:
            self._is_valid = lambda o, c: is_valid

    def get(self, object, context):
        return self.getfun(object, context)

    def is_valid(self, object, context):
        return self._is_valid(object, context)


class ExposeToTemplateMixin:
    expose_fields = []

    def get_expose_context(self, context, ignore_fields=None):
        expose_context = dict()

        if ignore_fields is None:
            ignore_fields = []

        for field in self.expose_fields:
            if field.is_valid(self, context) and field.tag not in ignore_fields:
                expose_context[field.tag] = field.get(self, context)

        return expose_context

    def get_expose_descriptions(self, include_invalid=False):
        desc = dict()
        for field in self.expose_fields:
            if not include_invalid and not field.is_valid(self, {}):
                continue

            desc[field.tag] = field.description

        return desc


class ExposeToTemplateViewMixin(ExposeToTemplateMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['expose_descriptions'] = self.get_expose_descriptions()

        return context
