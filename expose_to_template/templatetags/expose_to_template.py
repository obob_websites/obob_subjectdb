# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from classytags.helpers import InclusionTag
from django import template

register = template.Library()


class ExposedDescriptionsTag(InclusionTag):
    name = 'exposed_descriptions'
    template = 'expose_to_template/exposed_descriptions.html'

    def get_context(self, context, **kwargs):
        return {'expose_descriptions': context['expose_descriptions']}


register.tag('exposed_descriptions', ExposedDescriptionsTag)
