import datetime
import shutil
import time
from pathlib import Path

import django.utils.timezone
from django.conf import settings

from mridb import models


def empty_trash():
    deleted_subjects = models.Subject.objects.filter(
        deleted=True,
        datetime_deleted__lt=(django.utils.timezone.now() - datetime.timedelta(days=int(settings.EMPTY_TRASHCAN_DAYS))),
    )
    deleted_subjects.delete()

    deleted_mris = models.MRI.objects.filter(
        deleted=True,
        datetime_deleted__lt=(django.utils.timezone.now() - datetime.timedelta(days=int(settings.EMPTY_TRASHCAN_DAYS))),
    )
    deleted_mris.delete()


def clean_mri_temp():
    tmpdir = Path(settings.BOOTSTRAP_FILEINPUT['UPLOAD_TEMP'])
    for entry in tmpdir.iterdir():
        if not entry.is_dir():
            continue

        if time.time() - entry.stat().st_ctime >= settings.CLEAN_MRI_TEMP_SECONDS:
            shutil.rmtree(entry)
