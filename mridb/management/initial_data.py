# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import contextlib
import copy

import django.contrib.auth.management
from django.apps import apps

standard_mri_types = ('T1', 'T2', 'DTI', 'BEM')

standard_groups = {
    'Investigator/Technical Staff': {
        'perms': ['add_subject', 'change_subject', 'add_mri', 'change_mri', 'delete_subject', 'delete_mri'],
        'verbose_name': 'Investigator/Technical Staff',
        'section': 'general',
    }
}

standard_groups['Lab Manager'] = copy.deepcopy(standard_groups['Investigator/Technical Staff'])
standard_groups['Lab Manager']['verbose_name'] = 'Lab Manager'
standard_groups['Lab Manager']['perms'].append('view_edit_all_mri')

old_groups = ['Add and Edit Subjects', 'Delete Subjects', 'Add and Edit MRIs', 'Delete MRIs']


def create_mritypes(app_config, **kwargs):
    MRITypes = apps.get_model(app_config.name, 'MRIType').objects  # noqa N806

    for type in standard_mri_types:
        MRITypes.get_or_create(mri_type=type)


def create_b1019547_user(app_config, **kwargs):
    User = apps.get_model(app_config.label, 'User').objects  # noqa N806
    b1019547 = User.get_or_create(username='b1019547')[0]
    b1019547.is_superuser = True
    b1019547.set_unusable_password()
    b1019547.save()


def create_groups(app_config, **kwargs):
    django.contrib.auth.management.create_permissions(apps.get_app_config('mridb'))
    Permission = apps.get_model('auth', 'Permission').objects  # noqa N806
    Group = apps.get_model('auth', 'Group').objects  # noqa N806

    for current_group, attrs in standard_groups.items():
        temp_group = Group.get_or_create(name=current_group)[0]
        temp_group.extendedgroup.section = attrs['section']
        temp_group.extendedgroup.priority = 2
        temp_group.extendedgroup.verbose_name = attrs['verbose_name']
        temp_group.extendedgroup.save()
        temp_group.permissions.clear()
        for perm in attrs['perms']:
            temp_group.permissions.add(Permission.get(codename=perm))

    delete_old_groups(app_config, **kwargs)


def delete_old_groups(app_config, **kwargs):
    Groups = apps.get_model('auth', 'Group')  # noqa N806
    for group in old_groups:
        with contextlib.suppress(Groups.DoesNotExist):
            Groups.objects.get(name=group).delete()
