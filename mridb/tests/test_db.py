from pathlib import Path

from django.conf import settings
from django.core.exceptions import ValidationError
from django.test import SimpleTestCase, TestCase

import mridb.checkmri
import mridb.checkmri.checkmri
from mridb import models


class MRIDBTests(TestCase):
    def test_add_subject(self):
        subject = models.Subject(subject_id='19800908i', added_by_id=1)
        with self.assertRaises(ValidationError):
            subject.full_clean()
        subject = models.Subject(subject_id='19800908igdb', added_by_id=1)
        subject.full_clean()
        subject.save()

    def test_uppercase_subject(self):
        subject = models.Subject(subject_id='19800908IGDB', added_by_id=1)
        subject.full_clean()
        subject.save()

        subject = models.Subject.objects.get(subject_id='19800908igdb')


class MRICheckerTests(SimpleTestCase):
    def test_mri_check_and_detect(self):
        test_mri_dir = Path(settings.BASE_DIR) / 'test_datasets'

        all_mri_classes = mridb.checkmri.checkmri.CheckMRIBase.__subclasses__()

        all_mris = {
            Path('nifti_sbg') / 'test_nifti.nii': mridb.checkmri.checkmri.NIFTI,
            Path('nifti_sbg') / 'test_nifti.nii.gz': mridb.checkmri.checkmri.NIFTI,
            Path('dicom') / 'test_subject -0002-0001-00001.dcm': mridb.checkmri.checkmri.Dicom,
            Path('dicom'): mridb.checkmri.checkmri.Dicom,
            Path('analyze') / 'test_analyze.hdr': mridb.checkmri.checkmri.Analyze,
            Path('analyze') / 'test_analyze.img': mridb.checkmri.checkmri.Analyze,
            Path('cdktarbz2') / 'cdktarbz_testmri.tar.bz2': mridb.checkmri.checkmri.CDKMRI,
            Path('cdktarxz') / 'cdktarxz_testmri.tar.xz': mridb.checkmri.checkmri.CDKMRI,
            Path('cdktarxz_norm') / 'cdktarxz_norm.tar.xz': mridb.checkmri.checkmri.CDKMRI,
        }

        for mri_file, mri_class in all_mris.items():
            this_mri_class = mridb.checkmri.detect(test_mri_dir / mri_file)
            self.assertIsInstance(this_mri_class, mri_class)
            if mri_class == mridb.checkmri.checkmri.Dicom:
                self.assertFalse(this_mri_class.check(test_mri_dir / mri_file).status)
            else:
                self.assertTrue(this_mri_class.check(test_mri_dir / mri_file).status)

            for other_class in [other_class for other_class in all_mri_classes if mri_class != other_class]:
                x = other_class()
                self.assertFalse(x.check(test_mri_dir / mri_file).status)
