# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime
import os
import os.path
import time
import zipfile
from pathlib import Path

import polling
import six
from django.conf import settings
from django.contrib import auth
from django.utils import timezone
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC  # noqa N812

import mridb.management.cronjobs
import th_django.test
import user_manager.tests.helpers
from mridb import models

from .helpers import UITestMixin


class MRIDBUITests(th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin):
    def test_add_subject(self):
        self.index()
        self.login('b1019547')

        self.add_mri_subject('19800908igdb')
        self.add_mri_subject('19800908igdC')

        self.logout()

    def test_upload_download_mri(self):
        self._check_upload_download_mri(
            Path(settings.BASE_DIR, 'test_datasets', 'cdktarbz2', 'cdktarbz_testmri.tar.bz2'),
            Path(settings.BASE_DIR, 'test_datasets', 'converted_cdktarbz2', 'mri.nii'),
        )
        self._check_upload_download_mri(
            Path(settings.BASE_DIR, 'test_datasets', 'cdktarxz', 'cdktarxz_testmri.tar.xz'),
            Path(settings.BASE_DIR, 'test_datasets', 'converted_cdktarxz', 'mri.nii'),
        )
        self._check_upload_download_mri(
            Path(settings.BASE_DIR, 'test_datasets', 'cdktarxz_norm', 'cdktarxz_norm.tar.xz'),
            Path(settings.BASE_DIR, 'test_datasets', 'converted_cdktarxz_norm', 'mri.nii'),
        )
        self._check_upload_download_mri(Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg', 'test_nifti.nii'))
        self._check_upload_download_mri(list(Path(settings.BASE_DIR, 'test_datasets', 'analyze').glob('*')))
        self._check_upload_download_mri(
            list(Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg').glob('test_nifti.nii.gz'))
        )

    def test_delete_undelete(self):
        subjects = ['19800908ihgb', '19800908ihga', '19800908ihgc', '19800908ihgd']
        test_mri = Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg', 'test_nifti.nii')

        self.index()
        self.login('b1019547')

        for sub in subjects:
            self.add_mri_subject_db(sub)
            self.upload_mri(sub, test_mri, notes='MRI1')
            self.upload_mri(sub, test_mri, notes='MRI2')

        self.delete_mri(subjects[0])
        self.wait_for_table()
        self.assertEqual(models.Subject.objects.get(subject_id=subjects[0]).get_active_mris()[0].note, 'MRI2')
        self.assertEqual(models.Subject.objects.get(subject_id=subjects[0]).get_deleted_mris()[0].note, 'MRI1')

        self.delete_mri(subjects[1], 1)
        self.wait_for_table()
        self.assertEqual(models.Subject.objects.get(subject_id=subjects[1]).get_active_mris()[0].note, 'MRI1')
        self.assertEqual(models.Subject.objects.get(subject_id=subjects[1]).get_deleted_mris()[0].note, 'MRI2')

        self.delete_subject(subjects[2])
        self.wait_for_table()
        self.assertTrue(models.Subject.objects.get(subject_id=subjects[2]).deleted)

        self.index()
        self.click_navbutton()
        self.wait_until(EC.element_to_be_clickable((By.ID, 'add_subject')))
        delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == subjects[2]
        ]
        self.assertEqual(len(delete_button), 0)
        delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == subjects[0]
        ]
        self.assertEqual(len(delete_button), 1)

        self.show_view('mridb.views.display.SubjectDetail', kwargs={'subject_id': subjects[2]})
        self.assertEqual(self.selenium.find_element_by_tag_name('h1').text, 'OOOPS!! That should not have happened!')

        self.undelete_mri(subjects[0])
        # time.sleep(2)
        self.assertEqual(len(models.Subject.objects.get(subject_id=subjects[0]).get_active_mris()), 2)

        self.undelete_subject(subjects[2])
        self.assertFalse(models.Subject.objects.get(subject_id=subjects[1]).deleted)

        self.delete_mri(subjects[0])
        self.delete_subject(subjects[2])
        self.delete_subject(subjects[3])

        # now we set some deletion dates to 40 days ago to
        # control our thrashcan empty methods...

        tmp = models.Subject.objects.get(subject_id=subjects[0]).get_deleted_mris()[0]
        tmp.datetime_deleted = timezone.now() - datetime.timedelta(days=40)
        tmp.save()

        tmp = models.Subject.objects.get(subject_id=subjects[2])
        tmp.datetime_deleted = timezone.now() - datetime.timedelta(days=40)
        tmp.save()

        mridb.management.cronjobs.empty_trash()

        with self.assertRaises(models.Subject.DoesNotExist):
            models.Subject.objects.get(subject_id=subjects[2])

        subjects.remove(subjects[2])

        for s in subjects:
            models.Subject.objects.get(subject_id=s)

        self.assertEqual(len(models.Subject.objects.get(subject_id=subjects[0]).get_active_mris()), 1)
        self.assertEqual(len(models.Subject.objects.get(subject_id=subjects[0]).get_deleted_mris()), 0)

        self.assertEqual(len(models.Subject.objects.get(subject_id=subjects[1]).get_active_mris()), 1)
        self.assertEqual(len(models.Subject.objects.get(subject_id=subjects[1]).get_deleted_mris()), 1)

        self.assertEqual(len(models.Subject.objects.get(subject_id=subjects[2]).get_active_mris()), 2)
        self.assertEqual(len(models.Subject.objects.get(subject_id=subjects[2]).get_deleted_mris()), 0)

        self.logout()

    def test_click_on_everything(self):
        subject_id = '19800908ihgb'
        test_mri = Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg', 'test_nifti.nii')
        self.index()
        self.login('b1019547')
        self.add_mri_subject_db(subject_id)
        self.upload_mri(subject_id, test_mri)
        self.wait_until(EC.element_to_be_clickable((By.NAME, 'download')))

        # click in everything on the index view
        self.index()
        self.wait_click_and_wait((By.LINK_TEXT, subject_id), False)
        self.assertIsNotNone(self.find_element_by_text(f'Subject: {subject_id}'))
        self.index()
        self.wait_click_and_wait((By.NAME, 'upload'), False)
        self.assertIsNotNone(self.find_element_by_text('New MRI'))
        self.index()
        self.wait_until(EC.element_to_be_clickable((By.NAME, 'delete_button')))
        self.assertFalse(self.find_element_by_text('Confirm Delete').is_displayed())
        self.selenium.find_element_by_name('delete_button').click()
        self.wait_until(EC.element_to_be_clickable((By.ID, 'okButton')))
        self.assertTrue(self.find_element_by_text('Confirm Delete').is_displayed())
        self.index()
        self.wait_click_and_wait((By.NAME, 'edit'))
        self.assertIsNotNone(self.find_element_by_text(f'Subject: {subject_id}'))
        self.index()
        self.wait_click_and_wait((By.ID, 'add_subject'))
        self.assertIsNotNone(self.find_element_by_text('New Subject'))
        self.index()
        self.wait_click_and_wait((By.ID, 'undelete_subjects'))
        self.assertIsNotNone(self.find_element_by_text('Deleted Subjects'))
        self.index()

        # click on everything in the subject detail view...
        self.show_view('mridb.views.display.SubjectDetail', kwargs={'subject_id': subject_id})
        self.wait_click_and_wait((By.ID, 'upload'))
        self.assertIsNotNone(self.find_element_by_text('New MRI'))
        self.show_view('mridb.views.display.SubjectDetail', kwargs={'subject_id': subject_id})
        self.wait_click_and_wait((By.ID, 'undelete'))
        self.assertIsNotNone(self.find_element_by_text('Undelete'))

        self.logout()

    def test_search(self):
        subjects = ['19800908ihgb', '19800908ihga']
        self.index()
        self.login('b1019547')

        for sub in subjects:
            self.add_mri_subject_db(sub)

        self.index()
        navbar_toggle_button = self.selenium.find_element_by_id('navbar-button')
        if navbar_toggle_button.is_displayed():
            navbar_toggle_button.click()

        searchbar = self.selenium.find_element_by_id('id_subject_id')
        searchbar.send_keys('ihgb')
        searchbar.send_keys(Keys.ENTER)

        self.assertIsNotNone(self.find_element_by_text(subjects[0]))
        with self.assertRaises(NoSuchElementException):
            self.find_element_by_text(subjects[1])

    def test_edit_delete_user_permission(self):
        test_mri = Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg', 'test_nifti.nii')
        sub1 = '19800908ihgb'
        sub2 = '19800908ihga'

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Investigator/Technical Staff'])

        self.add_mri_subject_db(sub1)
        self.upload_mri(sub1, test_mri)

        self.logout()
        self.login('thht')
        self.add_mri_subject_db(sub2)
        self.upload_mri(sub2, test_mri)

        self.show_view('mridb.views.display.Subjects')

        sub1_instance = models.Subject.objects.get(subject_id=sub1)
        sub2_instance = models.Subject.objects.get(subject_id=sub2)

        self.wait_for_table()

        sub1_edit = [
            x
            for x in self.find_elements_by_text('Edit')
            if x.tag_name == 'a' and x.get_attribute('data-id') == str(sub1_instance.id)
        ][0]
        sub2_edit = [
            x
            for x in self.find_elements_by_text('Edit')
            if x.tag_name == 'a' and x.get_attribute('data-id') == str(sub2_instance.id)
        ][0]

        sub1_delete = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == sub1
        ][0]
        sub2_delete = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == sub2
        ][0]

        self.assertNotEqual(sub1_edit.get_attribute('class').find('disabled'), -1)
        self.assertEqual(sub2_edit.get_attribute('class').find('disabled'), -1)
        self.assertEqual(sub1_delete.get_attribute('disabled'), 'true')
        self.assertIsNone(sub2_delete.get_attribute('disabled'))

        self.show_view('mridb.views.edit.SubjectEdit', kwargs={'subject_id': sub1})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
        )

        self.show_view('mridb.views.edit.SubjectEdit', kwargs={'subject_id': sub2})
        self.assertIsNotNone(self.find_element_by_text(sub2))

        self.show_view('mridb.views.display.SubjectDetail', kwargs={'subject_id': sub1})
        self.wait_for_table()
        mri_delete = [x for x in self.find_elements_by_text('Delete') if x.tag_name == 'button'][0]
        self.assertEqual(mri_delete.get_attribute('disabled'), 'true')

        self.show_view('mridb.views.display.SubjectDetail', kwargs={'subject_id': sub2})
        self.wait_for_table()
        mri_delete = [x for x in self.find_elements_by_text('Delete') if x.tag_name == 'button'][0]
        self.assertIsNone(mri_delete.get_attribute('disabled'))

        self.show_view('mridb.views.edit.MRIDelete', kwargs={'mri_id': sub1_instance.mri_set.get().id})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
        )

        self.delete_mri(sub2)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_text('Successfully deleted MRI.'))

        self.show_view('mridb.views.edit.SubjectDelete', kwargs={'subject_id': sub1})
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
        )

        self.delete_subject(sub2)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_text('Successfully deleted subject 19800908ihga.'))

    def test_edit_delete_labmanager_permission(self):
        test_mri = Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg', 'test_nifti.nii')
        sub1 = '19800908ihgb'
        sub2 = '19800908ihga'

        self.index()
        self.login('b1019547')
        self.add_user_db('thht', ['Lab Manager'])

        self.add_mri_subject_db(sub1)
        self.upload_mri(sub1, test_mri)

        self.index()
        self.wait_for_table()
        self.logout()
        self.login('thht')
        self.add_mri_subject_db(sub2)
        self.upload_mri(sub2, test_mri)

        self.show_view('mridb.views.display.Subjects')

        sub1_instance = models.Subject.objects.get(subject_id=sub1)
        sub2_instance = models.Subject.objects.get(subject_id=sub2)

        self.wait_for_table()

        sub1_edit = [
            x
            for x in self.find_elements_by_text('Edit')
            if x.tag_name == 'a' and x.get_attribute('data-id') == str(sub1_instance.id)
        ][0]
        sub2_edit = [
            x
            for x in self.find_elements_by_text('Edit')
            if x.tag_name == 'a' and x.get_attribute('data-id') == str(sub2_instance.id)
        ][0]

        sub1_delete = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == sub1
        ][0]
        sub2_delete = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == sub2
        ][0]

        self.assertEqual(sub1_edit.get_attribute('class').find('disabled'), -1)
        self.assertEqual(sub2_edit.get_attribute('class').find('disabled'), -1)
        self.assertIsNone(sub1_delete.get_attribute('disabled'))
        self.assertIsNone(sub2_delete.get_attribute('disabled'))

        self.show_view('mridb.views.edit.SubjectEdit', kwargs={'subject_id': sub1})
        self.assertIsNotNone(self.find_element_by_text(sub1))

        self.show_view('mridb.views.edit.SubjectEdit', kwargs={'subject_id': sub2})
        self.assertIsNotNone(self.find_element_by_text(sub2))

        self.show_view('mridb.views.display.SubjectDetail', kwargs={'subject_id': sub1})
        self.wait_for_table()
        mri_delete = [x for x in self.find_elements_by_text('Delete') if x.tag_name == 'button'][0]
        self.assertIsNone(mri_delete.get_attribute('disabled'))

        self.show_view('mridb.views.display.SubjectDetail', kwargs={'subject_id': sub2})
        self.wait_for_table()
        mri_delete = [x for x in self.find_elements_by_text('Delete') if x.tag_name == 'button'][0]
        self.assertIsNone(mri_delete.get_attribute('disabled'))

        self.delete_mri(sub1)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_text('Successfully deleted MRI.'))

        self.delete_mri(sub2)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_text('Successfully deleted MRI.'))

        self.delete_subject(sub1)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_text('Successfully deleted subject 19800908ihgb.'))

        self.delete_subject(sub2)
        self.wait_for_table()
        self.assertIsNotNone(self.find_element_by_text('Successfully deleted subject 19800908ihga.'))

    def test_change_and_delete_user(self):
        user_model = auth.get_user_model()
        sub1 = '19800908ihgb'
        test_mri = Path(settings.BASE_DIR, 'test_datasets', 'nifti_sbg', 'test_nifti.nii')

        self.index()
        self.add_user_db('thht', ['Investigator/Technical Staff'])

        self.login('thht')
        self.add_mri_subject_db(sub1)
        self.upload_mri(sub1, test_mri)

        subject = models.Subject.objects.get(subject_id=sub1)
        mri = subject.get_active_mris()[0]
        self.assertIsNotNone(mri.mri_file.file_name)

        self.index()
        self.wait_for_table()
        self.logout()
        self.login('b1019547')
        self.edit_user('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])

        thht = user_model.objects.get(username='thht')
        self.assertTrue(thht.groups.filter(name='Access SubjectDB').exists())

        subject = models.Subject.objects.get(subject_id=sub1)
        mri = subject.get_active_mris()[0]
        self.assertIsNotNone(mri.mri_file.file_name)

        self.delete_user('thht')
        thht = user_model.objects.get(username='thht')
        self.assertFalse(thht.is_active)

        subject = models.Subject.objects.get(subject_id=sub1)
        mri = subject.get_active_mris()[0]
        self.assertIsNotNone(mri.mri_file.file_name)

    def test_db_helpers(self):
        sub1 = '19800908ihgb'

        self.index()
        self.login('b1019547')

        self.add_mri_subject_db(sub1)
        self.show_view('mridb.views.display.Subjects')
        self.assertIsNotNone(self.find_element_by_text(sub1))

        self.delete_subject_db(sub1)
        self.show_view('mridb.views.display.Subjects')
        with self.assertRaises(NoSuchElementException):
            self.find_element_by_text(sub1)

    def _check_upload_download_mri(self, test_mri, compare_mri=None):
        if isinstance(test_mri, list):
            test_mri = [str(x) for x in test_mri]
        elif isinstance(test_mri, Path):
            test_mri = str(test_mri)

        if not compare_mri:
            compare_mri = test_mri

        if isinstance(compare_mri, list):
            compare_mri = [str(x) for x in compare_mri]
        elif isinstance(compare_mri, Path):
            compare_mri = str(compare_mri)

        subject_id = '19800908ihgb'

        self.index()
        self.login('b1019547')
        self.add_mri_subject_db(subject_id)
        mri_filename = self.upload_mri(subject_id, test_mri)
        self.wait_until(EC.element_to_be_clickable((By.NAME, 'download')), timeout=240)

        if Path(self.unittest_dir, mri_filename).exists():
            Path(self.unittest_dir, mri_filename).unlink()

        time.sleep(1)
        self.selenium.find_element_by_name('download').click()

        file_handle = polling.poll(
            lambda: Path(self.unittest_dir, mri_filename).open('rb'),  # noqa SIM115
            ignore_exceptions=(IOError,),
            timeout=20,
            step=0.5,
        )

        file_zip = zipfile.ZipFile(file_handle)
        self.assertIsNone(file_zip.testzip())
        if isinstance(compare_mri, six.string_types):
            compare_mri = (compare_mri,)

        compare_mri_fnames = [os.path.split(this_file)[1] for this_file in compare_mri]
        compare_mri_path = os.path.split(compare_mri[0])[0]

        self.assertListEqual(sorted(file_zip.namelist()), sorted(compare_mri_fnames))

        for cur_file in file_zip.namelist():
            with (
                file_zip.open(cur_file) as cur_from_zip,
                Path(compare_mri_path, cur_file).open('rb') as cur_from_compare,
            ):
                self.assertEqual(cur_from_zip.read(), cur_from_compare.read())

        file_handle.close()

        subject = models.Subject.objects.get(subject_id=subject_id)
        mri = subject.get_active_mris()[0]
        self.assertIsNotNone(mri.mri_file.file_name)
        subject.delete()

        self.logout()
