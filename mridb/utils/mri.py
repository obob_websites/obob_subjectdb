# Copyright (c) 2017-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import tarfile
from pathlib import Path

import nibabel
import numpy as np
import pydicom
from dicom2nifti.convert_dicom import dicom_array_to_nifti

ALL_SERIES = (
    'MPRAGE_p2_1mmiso',
    'MPRAGE_1iso_2p',
    'OPTWANG2014_MPRAGE_1iso_2p',
    'MPRAGE_p2',
    'HCPLIFE_T1w_MPR1_vx08',
    'T1w_MPR_HCPLIFE',
)


def _process_tarfile(dicom_tar):
    all_dicoms = []
    has_norm = False

    for cur_item in dicom_tar:
        if cur_item.isfile():
            cur_ds = pydicom.dcmread(dicom_tar.extractfile(cur_item))
            if cur_ds.SeriesDescription in ALL_SERIES:
                all_dicoms.append(cur_ds)
                if 'NORM' in cur_ds.ImageType:
                    has_norm = True

    if has_norm:
        all_dicoms = [ds for ds in all_dicoms if 'NORM' in ds.ImageType]

    return all_dicoms


def convert_cdk_mri(in_file, out_folder):
    flip_axis = 0
    file_ext = Path(in_file).suffix.lower()[1:]

    with tarfile.open(in_file, f'r:{file_ext}') as dicom_tar:
        all_dicoms = _process_tarfile(dicom_tar)

    dicom_array_to_nifti(all_dicoms, Path(out_folder, 'mri_tmp.nii'), reorient_nifti=True)
    nifti = nibabel.load(Path(out_folder, 'mri_tmp.nii'))
    nifti_data = np.flip(np.asanyarray(nifti.dataobj), flip_axis)
    nifti_affine = nifti.affine.copy()
    nifti_affine[flip_axis, :] = -nifti_affine[flip_axis, :]

    nifti_new = nibabel.Nifti1Image(nifti_data, nifti_affine, nifti.header)
    nifti_new.to_filename(Path(out_folder, 'mri.nii'))

    Path(out_folder, 'mri_tmp.nii').unlink()
