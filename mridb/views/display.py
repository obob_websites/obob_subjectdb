# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from pathlib import Path

import django.db.models
from django.http import FileResponse, Http404
from django.views.generic import ListView, View
from django_auto_url import kwargs
from django_auto_url.mixins import AutoUrlMixin
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin

from mridb import models
from mridb.views import tables
from mridb.views.filter import SubjectFilter
from mridb.views.mixins import DefaultMixin
from user_manager.views.mixins import LoginRequiredMixin


class Subjects(DefaultMixin, SingleTableMixin, FilterView):
    model = models.Subject
    table_class = tables.SubjectList
    is_index = True
    main_header_html = 'All Subjects'
    filterset_class = SubjectFilter
    template_name = 'mridb/subject_list.html'

    def get_queryset(self):
        try:
            subject_id = self.request.GET['search']
        except KeyError:
            subject_id = None

        if subject_id is not None:
            object_list = self.model.objects.filter(subject_id__contains=subject_id, deleted=False)
        else:
            object_list = self.model.objects.filter(deleted=False)

        return object_list.prefetch_related(
            django.db.models.Prefetch(
                'mri_set', queryset=models.MRI.objects.filter(deleted=False), to_attr='active_mris'
            )
        )


class SubjectTrash(DefaultMixin, SingleTableMixin, FilterView):
    model = models.Subject
    permission_required = 'mridb.delete_subject'
    table_class = tables.SubjectTrash
    main_header_html = 'Deleted Subjects'
    filterset_class = SubjectFilter
    template_name = 'base_table.html'

    def get_queryset(self):
        return self.model.objects.filter(deleted=True).prefetch_related(
            django.db.models.Prefetch('mri_set', queryset=models.MRI.objects.all(), to_attr='active_mris')
        )


class SubjectDetail(DefaultMixin, SingleTableMixin, ListView):
    model = models.MRI
    table_class = tables.SubjectDetailList
    url_kwargs = [kwargs.string('subject_id')]
    template_name = 'mridb/subject_detail.html'

    def get_queryset(self):
        tmp_qs = models.Subject.objects.get(subject_id=self.kwargs['subject_id'])

        if tmp_qs.deleted:
            raise Http404

        return tmp_qs.get_active_mris().select_related('mri_type').select_related('file_type')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subject_id'] = self.kwargs['subject_id']
        return context

    def get_main_header(self):
        return 'Subject: {}'.format(self.kwargs['subject_id'])


class SubjectDetailTrash(SubjectDetail):
    permission_required = 'mridb.delete_mri'
    table_class = tables.SubjectDetailTrashList
    template_name = 'base_table.html'

    def get_queryset(self):
        return (
            models.Subject.objects.get(subject_id=self.kwargs['subject_id'])
            .get_deleted_mris()
            .select_related('mri_type')
            .select_related('file_type')
        )


# noinspection PyMethodMayBeStatic
class DownloadMri(LoginRequiredMixin, View, AutoUrlMixin):
    http_method_names = ['post']
    url_kwargs = [kwargs.int('mri_id')]

    def post(self, request, mri_id):
        mri_instance = models.MRI.objects.get(pk=mri_id)
        response = FileResponse(Path(mri_instance.mri_file.full_file_path).open('rb'))  # noqa SIM115
        response['Content-Disposition'] = f'attachment; filename={mri_instance.create_filename()}'

        return response
