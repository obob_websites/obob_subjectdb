# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import django_tables2
from django_auto_url.urls import reverse_classname_lazy

from mridb import models
from th_bootstrap.django_tables2.columns import Column, DateTimeColumn, DownloadColumn, UndeleteColumn

from .columns import DeleteColumn, EditColumn, MriCountColumn, MriUploadColumn, SubjectColumn


class SubjectList(django_tables2.Table):
    class Meta:
        model = models.Subject
        fields = ['subject_id', 'datetime_added']

    subject_id = SubjectColumn()
    datetime_added = DateTimeColumn()
    n_mris = MriCountColumn()
    upload = MriUploadColumn()
    delete = DeleteColumn(
        'subject_id',
        'confirm_delete',
        reverse_classname_lazy('mridb.views.edit.SubjectDelete', return_url_name=True),
        must_be_owner=True,
    )
    edit = EditColumn(
        reverse_classname_lazy('mridb.views.edit.SubjectEdit', return_url_name=True),
        'subject_id',
        needs_permission='mridb.change_subject',
        must_be_owner=True,
    )


class SubjectTrash(django_tables2.Table):
    class Meta:
        fields = ['subject_id', 'datetime_added', 'datetime_deleted']

    subject_id = Column(orderable=True)
    datetime_added = DateTimeColumn(orderable=True)
    datetime_deleted = DateTimeColumn(orderable=True)
    n_mris = MriCountColumn()
    undelete = UndeleteColumn(
        'subject_id', reverse_classname_lazy('mridb.views.edit.SubjectUndelete', return_url_name=True)
    )


class SubjectDetailList(django_tables2.Table):
    class Meta:
        model = models.MRI
        fields = ['acquisition_date', 'mri_type', 'file_type', 'notes']

    acquisition_date = DateTimeColumn(orderable=True)
    mri_type = Column(orderable=False)
    file_type = Column(orderable=False)
    notes = Column(orderable=False)
    delete = DeleteColumn(
        'id',
        'confirm_delete',
        reverse_classname_lazy('mridb.views.edit.MRIDelete', return_url_name=True),
        needs_permission='mridb.delete_mri',
        must_be_owner=True,
    )
    download = DownloadColumn('id', 'mridb:DownloadMri', 'btn-sm')


class SubjectDetailTrashList(django_tables2.Table):
    class Meta:
        fields = ['acquisition_date', 'datetime_deleted', 'mri_type', 'file_type', 'notes']

    acquisition_date = DateTimeColumn(orderable=True)
    datetime_deleted = DateTimeColumn(orderable=True)
    mri_type = Column(orderable=False)
    file_type = Column(orderable=False)
    notes = Column(orderable=False)
    undelete = UndeleteColumn('id', reverse_classname_lazy('mridb.views.edit.MRIUndelete', return_url_name=True))
