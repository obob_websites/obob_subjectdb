import os

CRONJOBS = [
    ('0 0 * * *', 'mridb.management.cronjobs.empty_trash'),
    ('0 0 * * *', 'subjectdb.management.cronjobs.empty_trash'),
    ('0 * * * *', 'mridb.management.cronjobs.clean_mri_temp'),
    ('0 0 * * *', 'subjectdb.management.cronjobs.gather_resting_state_data'),
]

CRONTAB_COMMAND_PREFIX = f'OBOB_SUBJECTDB_PERSISTENT_DIR={PERSISTENT_DIR}'

EMPTY_TRASHCAN_DAYS = os.getenv('OBOB_SUBJECTDB_EMPTY_TRASHCAN_DAYS', 30)
CLEAN_MRI_TEMP_SECONDS = os.getenv('OBOB_SUBJECTDB_CLEAN_MRI_TEMP_SECONDS', 60 * 60)

CRONTAB_LOCK_JOBS = True
