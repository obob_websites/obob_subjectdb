# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os.path
import shutil
import sys
from pathlib import Path
from uuid import uuid4

import dotenv
from django.contrib.messages import constants as messages

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

sys.path.append(os.path.join(BASE_DIR, 'dummy'))

# folder for persistent data like the databas (if sqlite is used),
# the mris and the static files
PERSISTENT_DIR = os.getenv('OBOB_SUBJECTDB_PERSISTENT_DIR', os.path.join(BASE_DIR, 'persistent_files', 'data'))

# folder for data from unittests
UNITTEST_DIR = os.path.join(BASE_DIR, 'unittest_files')

# create persistent folder if it does not exist
if not os.path.exists(PERSISTENT_DIR):
    os.makedirs(PERSISTENT_DIR)

# load environment variables...
if os.path.exists(os.path.join(PERSISTENT_DIR, 'settings.env')):
    dotenv.load_dotenv(os.path.join(PERSISTENT_DIR, 'settings.env'))
else:
    shutil.copy(
        os.path.join(BASE_DIR, 'obob_subjectdb', 'settings', 'templates', 'settings.env'),
        os.path.join(PERSISTENT_DIR, 'settings.env'),
    )

RAW_MEG_DIR = os.getenv('OBOB_SUBJECTDB_RAW_MEG_DIR', Path(BASE_DIR, 'test_datasets', 'sinuhe'))

RESTING_STATE_SUFFIXES = ['resting', 'rest', 'rest_open', 'resting_open']

if 'test' in sys.argv:
    PERSISTENT_DIR = os.getenv('OBOB_SUBJECTDB_PERSISTENT_DIR', os.path.join(UNITTEST_DIR, str(uuid4())))

# SECURITY WARNING: keep the secret key used in production secret!
if os.path.exists(os.path.join(os.getenv('OBOB_SUBJECTDB_KEY_DIR', ''), 'django_secret_key')):
    with open(os.path.join(os.getenv('OBOB_SUBJECTDB_KEY_DIR'), 'django_secret_key')) as f:
        SECRET_KEY = f.read().strip()
else:
    SECRET_KEY = 'NOTSAFE!!!'

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', os.getenv('HOSTNAME'), os.getenv('OBOB_SUBJECTDB_HOSTNAME', '')]

INTERNAL_IPS = ['127.0.0.1', 'localhost']

ROOT_URLCONF = 'obob_subjectdb.urls'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'th_django.middleware.AppList',
    'django_currentuser.middleware.ThreadLocalUserMiddleware',
]

if DEBUG and USE_DJANGO_DEBUG_TOOLBAR:  # noqa
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'obob_subjectdb.wsgi.application'

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'dynamic_preferences',
    'mridb',
    'subjectdb',
    'bootstrap3',
    'th_bootstrap',
    'th_django',
    'misc',
    'user_manager',
    'django_crontab',
    'django_tables2',
    'polymorphic',
    'jquery',
    'djangoformsetjs',
    'django_auto_url',
    'django_filters',
    'expose_to_template',
    'webpack_loader',
    'computedfields',
]

if DEBUG and USE_DJANGO_DEBUG_TOOLBAR:  # noqa
    INSTALLED_APPS.append('debug_toolbar')

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
]

MESSAGE_TAGS = {messages.ERROR: 'danger'}

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

BOOTSTRAP3 = {
    'form_renderers': {
        'default': 'th_bootstrap.forms.form_renderer.FormRenderer',
    },
}

DJANGO_TABLES2_TEMPLATE = 'django_tables2/bootstrap.html'

JS_PACKAGES_JSON_FILE = Path(PERSISTENT_DIR, 'js_packages.json')

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
