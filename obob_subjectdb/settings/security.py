# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os

if not DEBUG:
    CSRF_COOKIE_SECURE = True
    SESSION_COOKIE_SECURE = True
    SECURE_HSTS_SECONDS = 31536000
    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_BROWSER_XSS_FILTER = True
    X_FRAME_OPTIONS = 'DENY'
    SESSION_COOKIE_AGE = 60 * 60
else:
    SESSION_COOKIE_AGE = 3000000

CSRF_TRUSTED_ORIGINS = [f'http://{os.getenv("HOSTNAME")}', f'https://{os.getenv("HOSTNAME")}']

OBOB_SUBJECTDB_HOSTNAME = os.getenv('OBOB_SUBJECTDB_HOSTNAME')
if OBOB_SUBJECTDB_HOSTNAME:
    CSRF_TRUSTED_ORIGINS.append(f'https://{OBOB_SUBJECTDB_HOSTNAME}')
