/*
 * Copyright (c) 2016-2025, Thomas Hartmann
 *
 * This file is part of the OBOB Subject Database Project,
 * see: https://gitlab.com/obob/obob_subjectdb/
 *
 *  obob_subjectdb is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  obob_subjectdb is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
 */

import 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css';
import 'bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css';
import './lib/bootstrap_custom.scss';
import './lib/obob_subjectdb.css';
import 'bootstrap-fileinput/css/fileinput.min.css';
import '@claviska/jquery-dropdown/jquery.dropdown.min.css';
import 'select2/dist/css/select2.min.css';
import 'select2-bootstrap-theme/dist/select2-bootstrap.min.css';
import '../../../../th_bootstrap/static/ajaxbootstrap_fileinput.css';
import 'bootstrap-submenu/dist/css/bootstrap-submenu.css';