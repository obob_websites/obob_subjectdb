# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.core.management import call_command

import misc.views
import th_django.test
import user_manager.tests.helpers


class MiscTests(th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin):
    def test_misc_view(self):
        call_command('get_js_packages')
        self.index()
        self.show_view(misc.views.About)
        self.assertIsNotNone(self.find_element_by_text('Python Libraries'))
        self.show_view(misc.views.Help)
        self.assertIsNotNone(self.find_element_by_text('If you cannot figure out how to do something'))
