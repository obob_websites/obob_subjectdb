#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import pytest
from django.conf import settings
from django.core.management import call_command


def pytest_addoption(parser):
    parser.addoption('--django-debug', action='store_true')


@pytest.fixture(scope='session', autouse=True)
def init_test_session(tmpdir_factory, django_db_blocker, request):
    """General init fixture for all pytests."""
    settings.DEBUG = request.config.getoption('--django-debug')

    call_command('collectstatic', interactive=False)

    yield
