/*
* Copyright (c) 2016-2025, Thomas Hartmann
*
* This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
*
*    obob_subjectdb is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    obob_subjectdb is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Created by th on 15.04.16.
 */
require.config({
    "baseUrl": "/static",

    "nodeRequire": "require",

    "shim": {
        "bootstrap": {"deps": ["jquery"]},
        "bootstrap-validator": {"deps": ["bootstrap"]},
        "bootstrap-table": {"deps": ["jquery", "bootstrap"]},
        "jquery-dropdown": {"deps": ["jquery", "bootstrap"]},
        "select2": {"deps": ["jquery"]},
        "js/jquery.formset": {"deps": ["jquery"]},
    },

    "paths": {
        "jquery": "jquery/dist/jquery",
        "bootstrap": "bootstrap-sass/assets/javascripts/bootstrap",
        "bootstrap-datepicker": "bootstrap-datepicker/dist/js/bootstrap-datepicker",
        "bootstrap-datetime-picker": "bootstrap-datetime-picker/js/bootstrap-datetimepicker",
        "bootstrap-fileinput": "bootstrap-fileinput/js/fileinput",
        "js-cookie": "js-cookie/src/js.cookie",
        "bootstrap-validator": "bootstrap-validator/dist/validator",
        "sprintf-js": "sprintf-js/src/sprintf",
        "jquery-dropdown": "@claviska/jquery-dropdown/jquery.dropdown",
        "url-params": "url-params-helper/lib/url-params",
        "deepcopy": "deepcopy/build/deepcopy",
        "select2": "select2/dist/js/select2.full",
        "bootstrap-submenu": "bootstrap-submenu/dist/js/bootstrap-submenu"
    },
});