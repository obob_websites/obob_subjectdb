# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.urls import reverse_lazy
from django_auto_url.urls import reverse_classname_lazy

from th_django.middleware.applist import AppNamespaceInfo, AppNamespaceInfoMixin

from .management import create_groups


class SubjectdbConfig(AppConfig, AppNamespaceInfoMixin):
    name = 'subjectdb'

    namespace_info = [
        AppNamespaceInfo(
            name='Subjects',
            namespace='subjects',
            top_url=reverse_lazy('subjectdb:subjects:Subjects'),
            needs_permission='subjectdb.view_subjectdb',
        ),
        AppNamespaceInfo(
            name='Experiments',
            namespace='experiments',
            top_url=reverse_lazy('subjectdb:experiments:Experiments'),
            needs_permission='subjectdb.view_subjectdb',
        ),
        AppNamespaceInfo(
            name='Resting State',
            namespace='resting_state',
            top_url=reverse_classname_lazy('subjectdb.views.resting.RestingState'),
            needs_permission='subjectdb.view_subjectdb',
        ),
    ]

    def ready(self):
        post_migrate.connect(create_groups, sender=self, dispatch_uid='subjectdb.apps.create_groups')
