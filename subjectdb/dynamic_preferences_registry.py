#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.types import LongStringPreference, StringPreference

recruitmentemail = Section('recruitmentemail')
restingstate = Section('restingstate')


@global_preferences_registry.register
class RecruitmentEmailFooter(LongStringPreference):
    section = recruitmentemail
    name = 'recruitment_email_footer'
    default = ''
    required = False


@global_preferences_registry.register
class RestingStateFilenameSubstitutionMatch(StringPreference):
    section = restingstate
    name = 'restingstate_filename_sub_match'
    default = ''
    required = False
    verbose_name = 'Substitution for Resting State Filenames. Match Pattern'


@global_preferences_registry.register
class RestingStateFilenameSubstitutionReplace(StringPreference):
    section = restingstate
    name = 'restingstate_filename_sub_replace'
    default = ''
    required = False
    verbose_name = 'Substitution for Resting State Filenames. Replacement'
