#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import datetime
from pathlib import Path

import factory
import mne
import numpy as np
from django.utils import timezone


class RawTestFif(mne.io.RawArray):
    def __init__(
        self, *args, subject=None, project_name=None, meas_date=None, save_folder=None, f_suffix=None, **kwargs
    ):
        super().__init__(*args, **kwargs)

        self._subject = subject
        self._project_name = project_name
        self._save_folder = save_folder
        self._f_suffix = f_suffix

        if meas_date is not None:
            self.set_meas_date(meas_date.astimezone(datetime.timezone.utc))

    def save(self, *args, **kwargs):
        if self._subject is None:
            raise RuntimeError('Need a subject')
        if self._project_name is None:
            raise RuntimeError('Need a project name')
        if self._save_folder is None:
            raise RuntimeError('Need a save folder')
        if self.info['meas_date'] is None:
            raise RuntimeError('Need a measurement date!')

        f_name = Path(
            self._save_folder,
            self._project_name,
            'subject_subject',
            self.info['meas_date'].strftime('%y%m%d'),
            f'{self._subject}{"_" + self._f_suffix if self._f_suffix else ""}.fif',  # noqa
        )

        f_name.parent.mkdir(exist_ok=True, parents=True)

        self._filenames = (f_name,)
        super().save(f_name, *args, **kwargs)


class RawFif(factory.Factory):
    class Meta:
        model = RawTestFif

    class Params:
        n_channels = 16
        sfreq = 256
        n_secs = 2
        subject_object = None
        dewar_position = 68

    data = factory.LazyAttribute(lambda o: np.random.randn(o.n_channels, o.n_secs * o.sfreq))

    save_folder = None
    meas_date = factory.Faker('past_datetime', start_date=-1600, tzinfo=timezone.get_current_timezone())
    project_name = None

    @factory.lazy_attribute
    def subject(self):
        if self.subject_object is not None:
            return self.subject_object.subject_id

        return None

    @factory.lazy_attribute
    def info(self):
        i = mne.create_info(ch_names=self.n_channels, sfreq=self.sfreq, ch_types='mag')
        with i._unlock():
            i['gantry_angle'] = self.dewar_position

        return i
