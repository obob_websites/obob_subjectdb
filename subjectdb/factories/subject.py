#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from collections import OrderedDict

from factory import Faker, RelatedFactory, lazy_attribute, post_generation
from factory.django import DjangoModelFactory

import th_django.modelfields.subject_id
from subjectdb import models


class SubjectBaseFactory(DjangoModelFactory):
    class Meta:
        model = models.SubjectBase

    class Params:
        mother_first_name = Faker('first_name_female')
        mother_maiden_name = Faker('last_name')
        age = Faker('random_int', min=18, max=80)

    gender = Faker('random_element', elements=OrderedDict({'M': 0.45, 'F': 0.45, 'O': 0.1}))
    handedness = Faker('random_element', elements=OrderedDict({'Right': 0.8, 'Left': 0.15, 'Ambidexter': 0.05}))

    @lazy_attribute
    def birthday(self):
        return Faker._get_faker().date_of_birth(minimum_age=self.age, maximum_age=self.age)

    @lazy_attribute
    def subject_id(self):
        return th_django.modelfields.subject_id.SubjectID.generate_subject_id(
            birthday=self.birthday, mother_first_name=self.mother_first_name, mother_maiden_name=self.mother_maiden_name
        )

    @post_generation
    def special_groups(self, create, extracted, **kwargs):
        if not create or extracted is None:
            return

        for g in extracted:
            self.special_groups.add(g)


class SubjectPersonalInformationFactory(DjangoModelFactory):
    class Meta:
        model = models.SubjectPersonalInformation

    first_name = Faker('first_name')
    last_name = Faker('last_name')
    email = Faker('email')
    mobile_phone = Faker('phone_number')
    has_glasses = False
    can_do_MEG = True  # noqa N815
    can_do_MRI = True  # noqa N815


class SubjectWithPersonalInformationFactory(SubjectBaseFactory):
    consent_personal_info = True
    available_for_experiments = True

    subjectpersonalinformation = RelatedFactory(
        SubjectPersonalInformationFactory, factory_related_name='parent_subject'
    )


class SpecialSubjectGroupFactory(DjangoModelFactory):
    class Meta:
        model = models.SpecialSubjectGroups

    group_name = Faker('word')
