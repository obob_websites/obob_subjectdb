# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime
import re
import warnings
from pathlib import Path

import django.utils.timezone
import mne
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from tqdm import tqdm

import th_django.modelfields.subject_id
from subjectdb import models


def empty_trash():
    deleted_subjects = models.SubjectBase.objects.filter(
        deleted=True,
        datetime_deleted__lt=(django.utils.timezone.now() - datetime.timedelta(days=int(settings.EMPTY_TRASHCAN_DAYS))),
    )
    deleted_subjects.delete()

    deleted_experiments = models.Experiment.objects.filter(
        deleted=True,
        datetime_deleted__lt=(django.utils.timezone.now() - datetime.timedelta(days=int(settings.EMPTY_TRASHCAN_DAYS))),
    )
    deleted_experiments.delete()

    deleted_sessions = models.Session.objects.filter(
        deleted=True,
        datetime_deleted__lt=(django.utils.timezone.now() - datetime.timedelta(days=int(settings.EMPTY_TRASHCAN_DAYS))),
    )
    deleted_sessions.delete()


def gather_resting_state_data():
    warnings.filterwarnings('ignore', r'.*does not conform to MNE .*')

    print('gathering data')
    all_fif_files = Path(settings.RAW_MEG_DIR).glob('**/*.fif')

    re_pattern = r'.*_(' + '|'.join(settings.RESTING_STATE_SUFFIXES) + r')\.fif$'  # noqa

    print('processing....')
    for cur_fif in tqdm(list(all_fif_files)):
        fname = cur_fif.name

        if not re.match(re_pattern, fname, re.IGNORECASE):
            continue

        if models.RestingStateSession.objects.filter(filename=cur_fif).exists():
            continue

        subject_id = fname[:12].lower()

        if '_sss' in fname[12:].lower():
            continue

        fif = mne.io.read_raw_fif(cur_fif, allow_maxshield=True)
        m_date = fif.info['meas_date']

        if not re.match(th_django.modelfields.subject_id.SubjectID.id_regex, subject_id):
            warnings.warn(f'File {fname} does not have a valid subject id')
            continue

        try:
            birthday = datetime.datetime.strptime(fname[:8], '%Y%m%d').date()
        except ValueError:
            print('file name format seems to be invalid....')
            continue

        subject = models.SubjectBase.objects.get_or_create(subject_id=subject_id, birthday=birthday)[0]

        try:
            session = subject.session_set.filter(datetime_measured__date=m_date).order_by('datetime_added').last()
        except ObjectDoesNotExist:
            session = None

        obj = models.RestingStateSession(
            filename=cur_fif,
            subject=subject,
            session=session,
            datetime_measured=m_date,
            s_freq=fif.info['sfreq'],
            dewar_position=fif.info['gantry_angle'],
        )

        obj.save()
