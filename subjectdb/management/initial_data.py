# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import copy

import django.contrib.auth.management
from django.apps import apps

standard_groups = {
    'Access SubjectDB': {'perms': ['view_subjectdb'], 'verbose_name': 'Access SubjectDB', 'section': 'subjectdb'},
    'Investigator/Technical Staff': {
        'perms': [
            'add_session',
            'change_session',
            'delete_session',
            'add_experiment',
            'change_experiment',
            'delete_experiment',
            'add_subjectpersonalinformation',
            'change_subjectpersonalinformation',
            'delete_subjectpersonalinformation',
            'add_subjectbase',
            'change_subjectbase',
            'delete_subjectbase',
        ],
        'verbose_name': 'Investigator/Technical Staff',
        'section': 'general',
    },
}

standard_groups['Lab Manager'] = copy.deepcopy(standard_groups['Investigator/Technical Staff'])
standard_groups['Lab Manager']['verbose_name'] = 'Lab Manager'
standard_groups['Lab Manager']['perms'].extend(
    ['view_edit_all_subjectdb', 'view_subjectdb', 'change_subject_handedness_gender']
)


def create_groups(app_config, **kwargs):
    from django.contrib.contenttypes.models import ContentType

    django.contrib.auth.management.create_permissions(apps.get_app_config('subjectdb'))
    Permission = apps.get_model('auth', 'Permission').objects  # noqa N806
    Group = apps.get_model('auth', 'Group').objects  # noqa N806
    django_session_content_type = ContentType.objects.get(app_label='sessions')

    for current_group, attrs in standard_groups.items():
        temp_group = Group.get_or_create(name=current_group)[0]
        temp_group.extendedgroup.section = attrs['section']
        temp_group.extendedgroup.verbose_name = attrs['verbose_name']
        temp_group.extendedgroup.save()
        for perm in attrs['perms']:
            temp_group.permissions.add(Permission.exclude(content_type=django_session_content_type).get(codename=perm))
