# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-07 17:45
from __future__ import unicode_literals

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('subjectdb', '0011_auto_20161104_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subjectpersonalinformation',
            name='subject',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True,
                                       related_name='parent_subject_pk', to='subjectdb.SubjectBase'),
        ),
    ]
