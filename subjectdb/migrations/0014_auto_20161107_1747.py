# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-07 17:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('subjectdb', '0013_auto_20161107_1746'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subjectpersonalinformation',
            old_name='subject',
            new_name='parent_subject',
        ),
    ]
