# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-02 15:27
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('subjectdb', '0019_subjectbase_available_for_experiments'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='experiment',
            name='investigators',
        ),
        migrations.RemoveField(
            model_name='experiment',
            name='owner',
        ),
        migrations.AddField(
            model_name='experiment',
            name='additional_investigators',
            field=models.ManyToManyField(related_name='subjectdb_experiment_investigators', to=settings.AUTH_USER_MODEL,
                                         verbose_name='Additional Investigators'),
        ),
    ]
