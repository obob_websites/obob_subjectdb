# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import itertools

import django.db.models.signals
import more_itertools
import pyexcel
from django.conf import settings
from django.db import models

import mridb
from th_django.models.mixins import DeleteToTrash, EditableDateTimeAddedBy

from .ExtraData import ExtraDataTemplate
from .SubjectBase import SubjectBase


class Experiment(EditableDateTimeAddedBy, DeleteToTrash):
    acronym = models.CharField(max_length=30, unique=True, verbose_name='Acronym')
    short_description = models.TextField(max_length=300, verbose_name='Short Description')
    additional_investigators = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='%(app_label)s_%(class)s_investigators',
        verbose_name='Additional Investigators',
        blank=True,
    )
    anc_url = models.URLField(max_length=200, blank=True, verbose_name='ANC URL')
    sinuhe_project = models.CharField(max_length=30, blank=True, verbose_name='Sinuhe Project')
    paradigm_url = models.URLField(max_length=200, blank=True, verbose_name='Paradigm URL')

    uses_eeg = models.BooleanField(default=False, verbose_name='Uses EEG')
    uses_meg = models.BooleanField(default=False, verbose_name='Uses MEG')
    uses_fmri = models.BooleanField(default=False, verbose_name='Uses fMRI')
    uses_eyetracker = models.BooleanField(default=False, verbose_name='Uses Eyetracker')

    extra_data = models.ManyToManyField(ExtraDataTemplate, verbose_name='Extra Data', blank=True)

    def number_of_sessions(self, include_pilots=True, include_excluded=True):
        all_sessions = self.session_set.filter(deleted=False)
        if not include_pilots:
            all_sessions = all_sessions.filter(pilot_session=False)

        if not include_excluded:
            all_sessions = all_sessions.filter(excluded_from_experiment=False)

        return all_sessions.count()

    def mean_age(self, include_pilots=True, include_excluded=True):
        age_sum = 0.0
        all_sessions = self.session_set.filter(deleted=False)
        if not include_pilots:
            all_sessions = all_sessions.filter(pilot_session=False)

        if not include_excluded:
            all_sessions = all_sessions.filter(excluded_from_experiment=False)

        for cur_session in all_sessions:
            age_sum += cur_session.subject.age(cur_session.datetime_measured.date())

        return age_sum / self.number_of_sessions(include_pilots, include_excluded)

    def number_of_females(self, include_pilots=True, include_excluded=True):
        all_sessions = self.session_set.filter(deleted=False)
        if not include_pilots:
            all_sessions = all_sessions.filter(pilot_session=False)

        if not include_excluded:
            all_sessions = all_sessions.filter(excluded_from_experiment=False)

        return SubjectBase.objects.filter(session__in=all_sessions).filter(gender='F').count()

    def number_of_right_handed(self, include_pilots=True, include_excluded=True):
        all_sessions = self.session_set.filter(deleted=False)
        if not include_pilots:
            all_sessions = all_sessions.filter(pilot_session=False)

        if not include_excluded:
            all_sessions = all_sessions.filter(excluded_from_experiment=False)

        return SubjectBase.objects.filter(session__in=all_sessions).filter(handedness='Right').count()

    def export_to_excel(self, include_pilots=True):
        header_sheet_all_sessions = self._get_excel_header_sheet(include_pilots, True)
        header_sheet_valid_sessions = self._get_excel_header_sheet(include_pilots, False)

        all_sessions = self.session_set.filter(deleted=False)
        if not include_pilots:
            all_sessions = all_sessions.filter(pilot_session=False)

        sheet_data = []
        for session in all_sessions:
            sheet_data.append(session.get_excel_data(include_pilots))

        all_headers = list(
            more_itertools.unique_everseen(itertools.chain.from_iterable([y.keys() for y in sheet_data]))
        )

        data_sheet = pyexcel.get_sheet(records=sheet_data, custom_headers=all_headers)

        new_sheet = pyexcel.Sheet()
        new_sheet.row += [['Experiment Acronym', self.acronym]]
        new_sheet.row += [['Description', self.short_description]]
        new_sheet.row += [[' ']]
        new_sheet.row += [['All Sessions']]
        new_sheet.row += header_sheet_all_sessions.to_array()
        new_sheet.row += [[' ']]
        new_sheet.row += [['Only valid Sessions']]
        new_sheet.row += header_sheet_valid_sessions.to_array()
        new_sheet.row += [[' ']]
        new_sheet.row += data_sheet.to_array()

        return new_sheet

    def get_all_mris(self):
        all_subject_ids = self.session_set.all().filter(deleted=False).values_list('subject', flat=True)
        all_subject_ids = SubjectBase.objects.filter(id__in=all_subject_ids).values_list('subject_id', flat=True)

        all_mris = mridb.models.MRI.objects.filter(
            subject__subject_id__in=all_subject_ids, deleted=False, mri_type__mri_type='T1'
        )

        return all_mris

    def get_nr_of_subjects(self):
        all_subject_ids = self.session_set.all().filter(deleted=False).values_list('subject', flat=True)
        all_subject_ids = SubjectBase.objects.filter(id__in=all_subject_ids).values_list('subject_id', flat=True)

        return all_subject_ids.count()

    def _get_excel_header_sheet(self, include_pilots, include_excluded):
        header_data = [
            ['Number of Subjects', self.number_of_sessions(include_pilots, include_excluded)],
            ['Number of Females', self.number_of_females(include_pilots, include_excluded)],
            ['Number of right handed Subjects', self.number_of_right_handed(include_pilots, include_excluded)],
            ['Mean Age', self.mean_age(include_pilots, include_excluded)],
        ]

        header_sheet = pyexcel.get_sheet(array=header_data)

        return header_sheet

    @property
    def has_extradata(self):
        return (
            self.extra_data.count() > 0
            or ExtraDataTemplate.objects.filter(is_session_extradata=True, is_default_extradata=True).exists()
        )

    def subject_in_experiment(self, subject):
        return subject.pk in self.session_set.exclude(deleted=True).values_list('subject', flat=True)  # noqa

    def __str__(self):
        return str(self.acronym)


def update_session_special_groups(sender, action, instance, **kwargs):
    if action == 'post_add':
        for cur_session in instance.session_set.all():
            cur_session.update_extradata()


django.db.models.signals.m2m_changed.connect(update_session_special_groups, sender=Experiment.extra_data.through)
