# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import importlib
from collections import OrderedDict

import django.db.models.signals
import django.db.transaction
import django.forms.models
from dateutil.relativedelta import relativedelta
from django.db import models
from polymorphic.models import PolymorphicModel

import subjectdb.extradata_checker
import subjectdb.models.Session
from subjectdb.modelfields.ExtraDataModelFields import ExtradataMultiChoiceField, ExtradataSingleChoiceField
from th_bootstrap.models.fields import AjaxBootstrapFileInput
from th_django.models.mixins import DateTimeAddedBy, DeleteToTrash


class ExtraDataTemplate(DeleteToTrash, DateTimeAddedBy):
    name = models.CharField(max_length=100, blank=False, unique=True)
    description = models.TextField(blank=False)
    is_session_extradata = models.BooleanField(blank=False, default=True)
    is_default_extradata = models.BooleanField(blank=False, default=False)

    def __str__(self):
        return self.name

    @property
    def is_subject_extradata(self):
        return not self.is_session_extradata

    def create_concrete_extradata(self, target, present_extradata=None):
        session = None
        subject = None

        if present_extradata and self.name == present_extradata.name:
            pe_labels = present_extradata.dataitem_set.all().values_list('label', flat=True)
            my_labels = self.extradatatemplateitem_set.all().values_list('label', flat=True)

            missing_items = [x for x in set(my_labels) if x not in set(pe_labels)]
            deleted_items = [x for x in set(pe_labels) if x not in set(my_labels)]
            common_items = [x for x in set(pe_labels) if x in set(my_labels)]

            for cur_item_label in missing_items:
                cur_item_object = self.extradatatemplateitem_set.get(label=cur_item_label)
                new_item = get_extradataitem_from_string(cur_item_object.type)(label=cur_item_label)
                present_extradata.dataitem_set.add(new_item, bulk=False)

            for cur_item_label in deleted_items:
                cur_object = present_extradata.dataitem_set.get(label=cur_item_label)
                if cur_object.value in (None, ''):
                    cur_object.delete()

            for cur_item_label in common_items:
                cur_present_object = present_extradata.dataitem_set.get(label=cur_item_label)
                cur_template_object = self.extradatatemplateitem_set.get(label=cur_item_label)

                cur_present_object.is_required = cur_template_object.is_required
                cur_present_object.save()

            return present_extradata

        if self.is_session_extradata:
            session = target
        else:
            subject = target

        concrete_data = ConcreteExtraData(
            name=self.name, description=self.description, session=session, subject=subject
        )
        concrete_data.save()
        for item in self.extradatatemplateitem_set.all():
            new_item = get_extradataitem_from_string(item.type)(label=item.label, is_required=item.is_required)
            concrete_data.dataitem_set.add(new_item, bulk=False)
            new_item.process_field_options(item.field_options)
            new_item.save()

        return concrete_data

    @property
    def has_items(self):
        return self.extradatatemplateitem_set.all().exists()

    @django.db.transaction.atomic
    def save(self, **kwargs):
        rval = super().save(**kwargs)
        self.update_all_extradatas()

        return rval

    @django.db.transaction.atomic
    def update_all_extradatas(self):
        if self.is_session_extradata:
            for cur_session in subjectdb.models.Session.objects.all():
                cur_session.update_extradata()
        else:
            for cur_subject in subjectdb.models.SubjectBase.objects.all():
                cur_subject.update_extradata()


class ExtraDataTemplateItem(models.Model):
    label = models.CharField(max_length=100, blank=False)
    type = models.CharField(max_length=100, blank=False)
    extra_data = models.ForeignKey(ExtraDataTemplate, on_delete=models.CASCADE)
    is_required = models.BooleanField(default=True)
    field_options = models.TextField(max_length=10000, blank=True)

    @property
    def description(self):
        return get_extradataitem_from_string(self.type).description()

    def save(self, **kwargs):
        if self.pk is None:
            return super().save(**kwargs)
        else:
            kwargs['update_fields'] = ['is_required']
            return super().save(**kwargs)


class ConcreteExtraData(models.Model):
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=False)
    session = models.ForeignKey('subjectdb.Session', on_delete=models.SET_NULL, blank=True, null=True)
    subject = models.ForeignKey('subjectdb.SubjectBase', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def extradata_complete(self):
        rval = True
        for cur_dataitem in self.dataitem_set.all():
            rval = rval & (not cur_dataitem.is_empty)

        return rval

    def get_excel_data(self, session):
        excel_data = OrderedDict()

        for cur_item in self.dataitem_set.all():
            if cur_item.has_excel_representation:
                excel_data.update(cur_item.get_excel_representation(self.name, session))

        return excel_data


class DataItem(PolymorphicModel):
    label = models.CharField(max_length=100, blank=False)
    extra_data = models.ForeignKey(ConcreteExtraData, on_delete=models.SET_NULL, null=True)
    is_required = models.BooleanField(default=True)

    data_field_name = None
    has_excel_representation = True

    @property
    def value(self):
        return getattr(self, self.data_field_name)

    @value.setter
    def value(self, val):
        setattr(self, self.data_field_name, val)

    @property
    def is_empty(self):
        if not self.is_required:
            return False

        return self.value is None

    @classmethod
    def description(cls):
        raise NotImplementedError('You must override the description property in your class')

    def get_formfield(self):
        if self.data_field_name is None:
            raise NotImplementedError('You must provide the data_field_name property in the derived class')

        formfield = django.forms.models.fields_for_model(self.__class__)[self.data_field_name]
        formfield.widget.attrs['class'] = f'extradata_{self.__class__.__name__}'

        return formfield

    def get_excel_representation(self, prefix, session):
        return {f'{prefix}_{self.label}': self.value}

    def process_field_options(self, field_options):
        pass

    def _get_concrete_field_by_name(self, name):
        for cur_field in self.__class__._meta.concrete_fields:
            if cur_field.name == name:
                return cur_field

        for cur_field in self.__class__._meta.many_to_many:
            if cur_field.name == name:
                return cur_field


class StringItem(DataItem):
    string_value = models.CharField(max_length=100, blank=False)

    data_field_name = 'string_value'

    @classmethod
    def description(cls):
        return 'A String Value'

    @property
    def is_empty(self):
        if not self.is_required:
            return False

        return self.value == ''


class IntegerItem(DataItem):
    int_value = models.IntegerField(null=True, blank=False)
    data_field_name = 'int_value'

    @classmethod
    def description(cls):
        return 'An Integer Value'


class BooleanItem(DataItem):
    bool_value = models.BooleanField(blank=False, default=False)
    data_field_name = 'bool_value'

    @classmethod
    def description(cls):
        return 'A Boolean Value'


class DateItem(DataItem):
    date_value = models.DateField(null=True, blank=False)
    data_field_name = 'date_value'

    @classmethod
    def description(cls):
        return 'A Date'

    def get_excel_representation(self, prefix, session):
        my_prefix = f'{prefix}_{self.label}'

        if self.value:
            years_before_session = relativedelta(session.datetime_measured.date(), self.value).years
        else:
            years_before_session = None

        return {f'{my_prefix}_date': self.value, f'{my_prefix}_years_before_session': years_before_session}


class SingleChoiceDataItem(DataItem):
    data_field_name = 'current_choice'
    current_choice = ExtradataSingleChoiceField(
        'subjectdb.SingleChoiceItems', on_delete=models.CASCADE, blank=True, null=True
    )

    @classmethod
    def description(cls):
        return 'A Single Choice'

    def process_field_options(self, field_options):
        for cur_choice in field_options.split(','):
            self.singlechoiceitems_set.add(SingleChoiceItems(choice_string=cur_choice.strip(), owner=self), bulk=False)

    def get_formfield(self):
        formfield = self._get_concrete_field_by_name('current_choice').formfield(owner=self)
        formfield.widget.attrs['class'] = f'extradata_{self.__class__.__name__}'

        return formfield

    @property
    def value(self):
        return super().value

    @value.setter
    def value(self, val):
        if val:
            self.current_choice = SingleChoiceItems.objects.get(owner=self, choice_string=val)
        else:
            self.current_choice = None

    def get_excel_representation(self, prefix, session):
        return {f'{prefix}_{self.label}': str(self.value)}


class MultiChoiceDataItem(DataItem):
    data_field_name = 'current_choices'
    current_choices = ExtradataMultiChoiceField('subjectdb.MultiChoiceItems', blank=True)

    @classmethod
    def description(cls):
        return 'A Multiple Choice Field'

    def process_field_options(self, field_options):
        for cur_choice in field_options.split(','):
            self.multichoiceitems_set.add(MultiChoiceItems(choice_string=cur_choice.strip(), owner=self), bulk=False)

    def get_formfield(self):
        formfield = self._get_concrete_field_by_name('current_choices').formfield(owner=self)
        formfield.widget.attrs['class'] = f'extradata_{self.__class__.__name__}'

        return formfield

    @property
    def value(self):
        return super().value.all()

    @value.setter
    def value(self, val):
        if val:
            self.current_choices.set(val)
        else:
            self.current_choices.clear()

    def __str__(self):
        pass

    def get_excel_representation(self, prefix, session):
        return {f'{prefix}_{self.label}': ', '.join([str(x) for x in self.value])}

    @property
    def is_empty(self):
        if not self.is_required:
            return False

        return not self.value.exists()


class PDFItem(DataItem):
    pdf_value = AjaxBootstrapFileInput(
        file_storage='pdf_files',
        allow_download=True,
        auto_upload=True,
        show_preview=False,
        file_extensions='pdf',
        single_file_check_function=subjectdb.extradata_checker.validate_pdf,
        show_messages=True,
        messages={'upload_started': 'Uploading PDF...', 'post_upload': 'PDF uploaded successfully'},
        show_current_file_name=True,
        blank=True,
    )

    data_field_name = 'pdf_value'
    has_excel_representation = False

    @classmethod
    def description(cls):
        return 'A PDF File'

    @property
    def is_empty(self):
        if not self.is_required:
            return False

        return self.value.file_name == ''


class SingleChoiceItems(models.Model):
    choice_string = models.CharField(max_length=200, blank=False)
    owner = models.ForeignKey(SingleChoiceDataItem, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.choice_string)


class MultiChoiceItems(models.Model):
    choice_string = models.CharField(max_length=200, blank=False)
    owner = models.ForeignKey(MultiChoiceDataItem, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.choice_string)


def get_datatype_choices():
    choices = []
    for cur_class in DataItem.__subclasses__():
        tmp = cur_class()
        choices.append((cur_class.__name__, tmp.description))

    return choices


def get_extradataitem_from_string(class_name):
    return getattr(importlib.import_module(__name__), class_name)


def update_extradata_items(sender, action, instance, **kwargs):
    if action == 'post_add':
        for cur_session in instance.session_set.all():
            cur_session.update_extradata()
