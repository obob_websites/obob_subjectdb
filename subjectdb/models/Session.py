# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from collections import OrderedDict

from dateutil.relativedelta import relativedelta
from django.db import models

import subjectdb.models.Experiment
import subjectdb.models.ExtraData
from th_django.models.mixins import DateTimeAddedBy, DeleteToTrash

from .SubjectBase import SubjectBase


class Session(DeleteToTrash, DateTimeAddedBy):
    subject = models.ForeignKey(SubjectBase, on_delete=models.CASCADE)
    experiment = models.ForeignKey('Experiment', on_delete=models.CASCADE)
    pilot_session = models.BooleanField(default=False, verbose_name='Pilot Session')
    datetime_measured = models.DateTimeField(verbose_name='Date and Time of Measurement')

    notes = models.TextField(blank=True, verbose_name='Notes')
    excluded_from_experiment = models.BooleanField(default=False, verbose_name='Session excluded')
    excluded_reason = models.TextField(blank=True, verbose_name='Reason for exclusion')

    def save(self, **kwargs):
        self.subject.deleted = False
        self.subject.datetime_deleted = None
        self.subject.save()
        rval = super().save(**kwargs)

        self.update_extradata()

        return rval

    def update_extradata(self):
        all_extradata = []
        for extra_data in self.experiment.extra_data.all():
            if extra_data.has_items and extra_data not in all_extradata:
                all_extradata.append(extra_data)

        ExtraDataTemplate = subjectdb.models.ExtraData.ExtraDataTemplate  # noqa N806
        for extra_data in ExtraDataTemplate.objects.filter(is_default_extradata=True, is_session_extradata=True):
            if extra_data.has_items and extra_data not in all_extradata:
                all_extradata.append(extra_data)

        present_concrete_extradata = self.concreteextradata_set.all().values_list('name', flat=True)

        for cur_extradata in all_extradata:
            if cur_extradata.name in present_concrete_extradata:
                current_concrete_extradata = self.concreteextradata_set.all().get(name=cur_extradata.name)
                self.concreteextradata_set.add(
                    cur_extradata.create_concrete_extradata(self, current_concrete_extradata)
                )
            else:
                self.concreteextradata_set.add(cur_extradata.create_concrete_extradata(self))

        # identify deleted extradata
        present_extradata = set(present_concrete_extradata.values_list('name', flat=True))
        new_extradata = {str(item.name) for item in all_extradata}
        deleted_extradata = present_extradata - new_extradata

        # iterate over all deleted extradatas and delete empty items
        for cur_extradata in self.concreteextradata_set.filter(name__in=deleted_extradata):
            all_empty = True
            for cur_item in cur_extradata.dataitem_set.all():
                if cur_item.is_empty:
                    cur_item.delete()
                else:
                    all_empty = False

            if all_empty:
                cur_extradata.delete()

    def subject_id(self):
        return self.subject.subject_id

    def subject_age(self):
        return relativedelta(self.datetime_measured.date(), self.subject.birthday).years

    def get_excel_data(self, include_pilots):
        excel_data = OrderedDict(
            {
                'Date and Time of Measurement': self.datetime_measured.isoformat(),
                'Subject ID': self.subject.subject_id,
                'Age': self.subject.age(self.datetime_measured.date()),
                'Gender': self.subject.gender,
                'Handedness': self.subject.handedness,
                'Session Notes': self.notes,
                'Subject Notes': self.subject.notes,
                'Excluded': self.excluded_from_experiment,
                'Reason for Exclusion': self.excluded_reason,
            }
        )

        if include_pilots:
            excel_data['Pilot Session'] = self.pilot_session

        for cur_subject_extradata in self.subject.concreteextradata_set.all():
            excel_data.update(cur_subject_extradata.get_excel_data(self))

        for cur_session_extradata in self.concreteextradata_set.all():
            excel_data.update(cur_session_extradata.get_excel_data(self))

        return excel_data
