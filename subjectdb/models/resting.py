#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from computedfields.models import ComputedFieldsModel, computed
from django.db import models

from .Session import Session
from .SubjectBase import SubjectBase


class RestingStateSession(ComputedFieldsModel):
    datetime_added = models.DateTimeField(auto_now_add=True, verbose_name='Added')
    filename = models.CharField(max_length=500, unique=True)
    subject = models.ForeignKey(SubjectBase, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, null=True, on_delete=models.CASCADE, blank=True)
    datetime_measured = models.DateTimeField(verbose_name='Date and Time of Measurement')
    s_freq = models.FloatField(default=0)
    dewar_position = models.FloatField(default=0)

    @computed(models.IntegerField(), depends=[('subject', ['birthday']), ('self', ['datetime_measured'])])
    def age_at_measurement(self):
        return self.subject.age(self.datetime_measured.date())
