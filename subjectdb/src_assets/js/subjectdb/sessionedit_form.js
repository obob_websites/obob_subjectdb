/*
 * Copyright (c) 2016-2025, Thomas Hartmann
 *
 * This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
 *
 *    obob_subjectdb is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    obob_subjectdb is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
 */
import 'jquery';
import init_extradata from "./lib/init_extradata";
import 'bootstrap-validator';

init_extradata();
var form = $('#form');

var excluded_switch = $('#id_excluded_from_experiment');
var excluded_reason = $('#id_excluded_reason');
excluded_reason.attr('data-validate', 'true');
excluded_reason.prop('disabled', !excluded_switch.prop('checked'));
excluded_reason.prop('required', excluded_switch.prop('checked'));

form.validator('update');
form.validator('validate');

excluded_switch.change(function () {
    excluded_reason.prop('disabled', !this.checked);
    excluded_reason.prop('required', this.checked);
    form.validator('update');
    form.validator('validate');
});
