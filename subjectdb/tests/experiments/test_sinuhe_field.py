from copy import deepcopy
from pathlib import Path

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa: N812
from selenium.webdriver.support.ui import Select

import subjectdb.views.experiments
import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.tests import UITestMixin, base_infos
from subjectdb.tests.fixtures import TestData
from subjectdb.tests.helpers import ExtraAssertionMixin


class SinuheFieldTests(
    th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin, ExtraAssertionMixin
):
    def test_datafixture(self):
        self.index()
        self.login_fast('b1019547')
        self.index()

        with TestData.use() as data:
            for cur_project in data.projects:
                assert Path(data.tmp_path, cur_project.name).exists()

    def test_sinuhe_field(self):
        self.index()
        self.add_user_db('thht', ['Investigator/Technical Staff'])
        self.login_fast('b1019547')

        with TestData.use() as data, self.settings(RAW_MEG_DIR=str(data.tmp_path)):
            for has_sinuhe_project in [True, False]:
                self.show_view(subjectdb.views.experiments.ExperimentCreate)
                self.wait_until(EC.visibility_of_element_located((By.ID, 'id_acronym')))
                sinuhe_select = Select(self.selenium.find_element(By.ID, 'id_sinuhe_project'))
                for cur_project in data.projects:
                    assert cur_project.name in [o.text for o in sinuhe_select.options]

                exp_data = deepcopy(base_infos.experiment)
                if has_sinuhe_project:
                    exp_data['sinuhe_project'] = data.projects[0].name
                self.fill_experiment(**exp_data)

                self.selenium.find_element_by_id('form').submit()
                self.assert_experiment(exp_data)
                object = models.Experiment.objects.get(acronym=exp_data['acronym'])
                object.delete()
