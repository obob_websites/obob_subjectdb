# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import django.test

import user_manager.tests.helpers
from subjectdb import models
from subjectdb.tests import base_infos
from subjectdb.tests.helpers import UITestMixin


class ExtraDataDBTests(django.test.TestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin):
    def test_create_extradataitem(self):
        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        extra_data_template = self.add_session_extradata_template_db(**base_infos.extradata_with_items)

        experiment = self.add_experiment_db(**base_infos.experiment)
        experiment.extra_data.add(extra_data_template)
        self.add_session_db(experiment.acronym, **base_infos.session)

        current_session = models.Session.objects.all()[0]
        current_extradata = current_session.concreteextradata_set.get(name=base_infos.extradata_with_items['name'])

        int_item = current_extradata.dataitem_set.get(label='A test Integertype')
        int_item.value = 10
        int_item.save()

        str_item = current_extradata.dataitem_set.get(label='A test Stringtype')
        str_item.value = 'TestString'
        str_item.save()

        self.assertEqual(models.Session.objects.count(), 1)

        new_session = models.Session.objects.all()[0]
        new_extradata = new_session.concreteextradata_set.get(name=base_infos.extradata_with_items['name'])
        self.assertEqual(new_extradata.dataitem_set.get(label='A test Integertype').value, 10)
        self.assertEqual(new_extradata.dataitem_set.get(label='A test Stringtype').value, 'TestString')
