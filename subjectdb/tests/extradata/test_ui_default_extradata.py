# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import contextlib
import copy
import datetime
from pathlib import Path

from django.conf import settings
from django.db.models import QuerySet
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa N812

import subjectdb.views.extradata
import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.tests import base_infos
from subjectdb.tests.helpers import ExtraAssertionMixin, UITestMixin


class ExtradataDefaultUITests(
    th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin, ExtraAssertionMixin
):
    def test_add_edit_session_extradata(self):
        self.index()
        self.login('b1019547')

        new_extradata = self.add_session_extradata_template(**base_infos.extradata_default)

        self._assert_extradatatemplate(new_extradata, base_infos.extradata_default)

        new_extradata_info = copy.deepcopy(base_infos.extradata_with_items_default)
        new_extradata_info['name'] = 'Another Extradata'
        new_extradata_info['description'] = 'Another Test Description'

        new_extradata_with_items = self.add_session_extradata_template(**new_extradata_info)

        self._assert_extradatatemplate(new_extradata_with_items, new_extradata_info)

        label_to_delete = 'A test Stringtype'

        self.del_items_from_session_extradata(new_extradata_with_items, [label_to_delete])

        del new_extradata_info['dataitems'][label_to_delete]

        new_extradata_with_items.refresh_from_db()

        self._assert_extradatatemplate(new_extradata_with_items, new_extradata_info)

    def test_add_edit_subject_extradata(self):
        self.index()
        self.login('b1019547')

        new_extradata = self.add_subject_extradata_template(**base_infos.extradata_default)

        self._assert_extradatatemplate(new_extradata, base_infos.extradata_default)

        new_extradata_info = copy.deepcopy(base_infos.extradata_with_items_default)
        new_extradata_info['name'] = 'Another Extradata'
        new_extradata_info['description'] = 'Another Test Description'

        new_extradata_with_items = self.add_subject_extradata_template(**new_extradata_info)

        self._assert_extradatatemplate(new_extradata_with_items, new_extradata_info)

        label_to_delete = 'A test Stringtype'
        self.del_items_from_subject_extradata(new_extradata_with_items, [label_to_delete])

        del new_extradata_info['dataitems'][label_to_delete]

        new_extradata_with_items.refresh_from_db()

        self._assert_extradatatemplate(new_extradata_with_items, new_extradata_info)

    def test_add_extradata_to_experiment(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        self.add_session_extradata_template_db(**base_infos.extradata_with_items_default)

        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)
        self.fill_session_extradata(models.Session.objects.all()[0].id, base_infos.extradata_values)

        this_session = models.Session.objects.all()[0]
        self.assert_extradata(this_session.concreteextradata_set.all()[0], base_infos.extradata_values)

    def test_add_extradata_later_to_experiment(self):
        self.index()
        self.login('b1019547')

        other_extradata_with_items = {
            'name': 'Other TestExtraData',
            'description': 'Other TestDescription',
            'is_default_data': True,
            'dataitems': {
                'A second test Stringtype': 'StringItem',
                'A second Singlechoice': 'SingleChoiceDataItem',
                'A second Multichoice': 'MultiChoiceDataItem',
            },
            'options': {
                'A second Singlechoice': 'Yes, No, Vielleicht',
                'A second Multichoice': 'Hello World, Goodbye, The last one',
            },
        }

        other_extradata_values = {
            'A second test Stringtype': 'A second String',
            'A second Singlechoice': 'No',
            'A second Multichoice': 'Hello World, Goodbye',
        }

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        self.add_session_extradata_template(**base_infos.extradata_with_items_default)

        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)
        self.fill_session_extradata(models.Session.objects.all()[0].id, base_infos.extradata_values)

        self.add_session_extradata_template(**other_extradata_with_items)
        self.fill_session_extradata(models.Session.objects.all()[0].id, other_extradata_values)

        this_session = models.Session.objects.all()[0]

        self.assert_extradata(this_session.concreteextradata_set.all()[0], base_infos.extradata_values)
        self.assert_extradata(this_session.concreteextradata_set.all()[1], other_extradata_values)

    def test_delete_used_unused_extradataitem_from_experiment(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        extra_data_template = self.add_session_extradata_template_db(**base_infos.extradata_with_items_default)

        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)
        first_session = models.Session.objects.order_by('-id')[0]
        self.fill_session_extradata(first_session.id, base_infos.extradata_values)

        self.add_session(this_experiment['acronym'], **base_infos.session)
        second_session = models.Session.objects.order_by('-id')[0]

        item_to_delete = 'A test Datetype'

        self.del_items_from_session_extradata(extra_data_template, [item_to_delete])

        first_session.refresh_from_db()
        second_session.refresh_from_db()

        self.assert_extradata(first_session.concreteextradata_set.all()[0], base_infos.extradata_values)

        second_session_extradata = second_session.concreteextradata_set.all()[0]
        for key, value in base_infos.extradata_values.items():
            if key != item_to_delete:
                cmp_value = second_session_extradata.dataitem_set.get(label=key).value
                if isinstance(cmp_value, QuerySet) and not cmp_value.exists():
                    cmp_value = None

                self.assertIn(cmp_value, (None, ''))
            else:
                with self.assertRaises(models.DataItem.DoesNotExist):
                    second_session_extradata.dataitem_set.get(label=key)

    def test_add_extradataitem_later_to_experiment(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        extra_data_template = self.add_session_extradata_template_db(**base_infos.extradata_with_items_default)

        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)
        self.fill_session_extradata(models.Session.objects.all()[0].id, base_infos.extradata_values)

        this_session = models.Session.objects.all()[0]
        extradata = this_session.concreteextradata_set.all()[0]

        self.assert_extradata(extradata, base_infos.extradata_values)

        new_extradata_item = {'A second test Stringtype': 'StringItem'}

        self.add_item_to_session_extradata(extra_data_template, new_extradata_item)

        new_extradata_values = copy.deepcopy(base_infos.extradata_values)

        new_extradata_values.update(
            {
                'A second test Stringtype': 'A second String',
            }
        )

        self.fill_session_extradata(models.Session.objects.all()[0].id, new_extradata_values)

        extradata.refresh_from_db()

        self.assert_extradata(extradata, new_extradata_values)

    def test_delete_sessionextradata(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', ['Access SubjectDB', 'Investigator/Technical Staff'])
        self.add_user_db('thht2', ['Investigator/Technical Staff'])

        extra_data_template = self.add_session_extradata_template_db(**base_infos.extradata_with_items_default)

        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)
        first_session = models.Session.objects.order_by('-id')[0]
        self.fill_session_extradata(first_session.id, base_infos.extradata_values)

        self.add_session(this_experiment['acronym'], **base_infos.session)
        empty_session = models.Session.objects.order_by('-id')[0]

        partial_extradata = copy.deepcopy(base_infos.extradata_values)
        del partial_extradata['A test Multichoice']
        self.add_session(this_experiment['acronym'], **base_infos.session)
        partial_session = models.Session.objects.order_by('-id')[0]
        self.fill_session_extradata(partial_session.id, partial_extradata)

        self.del_sessionextradata(extra_data_template.name, True)

        self.assert_extradata(first_session.concreteextradata_set.all()[0], base_infos.extradata_values)
        self.assertFalse(empty_session.concreteextradata_set.all().exists())
        self.assert_extradata(partial_session.concreteextradata_set.all()[0], partial_extradata)

    def test_add_extradataitem_later_to_subject(self):
        self.index()
        self.login('b1019547')

        extra_data_template = self.add_subject_extradata_template_db(**base_infos.extradata_with_items_default)
        new_group = self.add_specialsubjectgroup('test_group')

        self.add_subject(special_subject_groups=[new_group], **base_infos.subject)
        this_subject = models.SubjectBase.objects.all()[0]

        self.fill_subject_extradata(this_subject.subject_id, base_infos.extradata_values)

        this_subject.refresh_from_db()
        extradata = this_subject.concreteextradata_set.all()[0]
        self.assert_extradata(extradata, base_infos.extradata_values)

        new_extradata_item = {'A second test Stringtype': 'StringItem'}

        self.add_item_to_subject_extradata(extra_data_template, new_extradata_item)

        new_extradata_values = copy.deepcopy(base_infos.extradata_values)

        new_extradata_values.update(
            {
                'A second test Stringtype': 'A second String',
            }
        )

        self.fill_subject_extradata(this_subject.subject_id, new_extradata_values)

        extradata.refresh_from_db()
        self.assert_extradata(extradata, new_extradata_values)

    def test_delete_used_unused_extradataitem_from_subject(self):
        self.index()
        self.login('b1019547')

        extra_data_template = self.add_subject_extradata_template_db(**base_infos.extradata_with_items_default)
        new_group = self.add_specialsubjectgroup('test_group')

        self.add_subject(special_subject_groups=[new_group], **base_infos.subject)
        first_subject = models.SubjectBase.objects.order_by('-id')[0]

        self.fill_subject_extradata(first_subject.subject_id, base_infos.extradata_values)

        second_subject_info = copy.deepcopy(base_infos.subject)
        second_subject_info['mother_first_name'] = 'Oma'

        self.add_subject(special_subject_groups=[new_group], **second_subject_info)
        second_subject = models.SubjectBase.objects.order_by('-id')[0]

        item_to_delete = 'A test Datetype'

        self.del_items_from_subject_extradata(extra_data_template, [item_to_delete])

        first_subject.refresh_from_db()
        second_subject.refresh_from_db()

        self.assert_extradata(first_subject.concreteextradata_set.all()[0], base_infos.extradata_values)

        second_subject_extradata = second_subject.concreteextradata_set.all()[0]
        for key, value in base_infos.extradata_values.items():
            if key != item_to_delete:
                cmp_value = second_subject_extradata.dataitem_set.get(label=key).value
                if isinstance(cmp_value, QuerySet) and not cmp_value.exists():
                    cmp_value = None

                self.assertIn(cmp_value, (None, ''))
            else:
                with self.assertRaises(models.DataItem.DoesNotExist):
                    second_subject_extradata.dataitem_set.get(label=key)

    def test_delete_subjectextradata_with_extradata_from_subject(self):
        self.index()
        self.login('b1019547')

        self.add_subject_extradata_template_db(**base_infos.extradata_with_items_default)
        new_group = self.add_specialsubjectgroup('test_group')

        self.add_subject(special_subject_groups=[new_group], **base_infos.subject)
        first_subject = models.SubjectBase.objects.order_by('-id')[0]

        self.fill_subject_extradata(first_subject.subject_id, base_infos.extradata_values)

        second_subject_info = copy.deepcopy(base_infos.subject)
        second_subject_info['mother_first_name'] = 'Oma'

        self.add_subject(special_subject_groups=[new_group], **second_subject_info)
        second_subject = models.SubjectBase.objects.order_by('-id')[0]

        third_subject_info = copy.deepcopy(base_infos.subject)
        third_subject_info['mother_first_name'] = 'Tante'
        self.add_subject(special_subject_groups=[new_group], **third_subject_info)
        third_subject = models.SubjectBase.objects.order_by('-id')[0]

        partial_extradata_values = copy.deepcopy(base_infos.extradata_values)
        del partial_extradata_values['A test Multichoice']

        self.fill_subject_extradata(third_subject.subject_id, partial_extradata_values)

        self.del_subjectextradata(base_infos.extradata_with_items_default['name'], True)

        first_subject.refresh_from_db()
        second_subject.refresh_from_db()
        third_subject.refresh_from_db()

        self.assert_extradata(first_subject.concreteextradata_set.all()[0], base_infos.extradata_values)
        self.assert_extradata(third_subject.concreteextradata_set.all()[0], partial_extradata_values)

        self.assertFalse(second_subject.concreteextradata_set.all().exists())

    def test_add_extradata_to_subject(self):
        self.index()
        self.login('b1019547')

        self.add_subject_extradata_template_db(**base_infos.extradata_with_items_default)
        new_group = self.add_specialsubjectgroup('test_group')

        self.add_subject(special_subject_groups=[new_group], **base_infos.subject)
        this_subject = models.SubjectBase.objects.all()[0]

        self.fill_subject_extradata(this_subject.subject_id, base_infos.extradata_values)

        this_subject.refresh_from_db()

        self.assert_extradata(this_subject.concreteextradata_set.all()[0], base_infos.extradata_values)

        models.SubjectBase.objects.all().delete()

    def test_add_extradata_later_to_subject(self):
        self.index()
        self.login('b1019547')

        other_extradata_with_items = {
            'name': 'Other TestExtraData',
            'description': 'Other TestDescription',
            'is_default_data': True,
            'dataitems': {
                'A second test Stringtype': 'StringItem',
                'A second Singlechoice': 'SingleChoiceDataItem',
                'A second Multichoice': 'MultiChoiceDataItem',
            },
            'options': {
                'A second Singlechoice': 'Yes, No, Vielleicht',
                'A second Multichoice': 'Hello World, Goodbye, The last one',
            },
        }

        other_extradata_values = {
            'A second test Stringtype': 'A second String',
            'A second Singlechoice': 'No',
            'A second Multichoice': 'Hello World, Goodbye',
        }

        self.add_subject_extradata_template_db(**base_infos.extradata_with_items_default)
        new_group = self.add_specialsubjectgroup('test_group')

        self.add_subject(special_subject_groups=[new_group], **base_infos.subject)
        this_subject = models.SubjectBase.objects.all()[0]

        self.fill_subject_extradata(this_subject.subject_id, base_infos.extradata_values)

        self.add_subject_extradata_template(**other_extradata_with_items)

        self.fill_subject_extradata(this_subject.subject_id, other_extradata_values)

        this_subject.refresh_from_db()

        self.assert_extradata(this_subject.concreteextradata_set.all()[0], base_infos.extradata_values)
        self.assert_extradata(this_subject.concreteextradata_set.all()[1], other_extradata_values)

    def test_two_of_the_same_kind(self):
        self.index()
        self.login('b1019547')

        this_extradata_template = copy.deepcopy(base_infos.extradata_with_items_default)
        this_extradata_template['dataitems'].update(
            {
                'A second test Stringtype': 'StringItem',
                'A second test Integertype': 'IntegerItem',
                'A second test Datetype': 'DateItem',
            }
        )

        self.add_session_extradata_template(**this_extradata_template)

        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)

        this_extradata_values = copy.deepcopy(base_infos.extradata_values)
        this_extradata_values.update(
            {
                'A second test Stringtype': 'World Hello',
                'A second test Integertype': 24,
                'A second test Datetype': datetime.date(day=13, month=2, year=2071),
            }
        )

        self.fill_session_extradata(models.Session.objects.all()[0].id, this_extradata_values)

        this_session = models.Session.objects.all()[0]

        self.assert_extradata(this_session.concreteextradata_set.all()[0], this_extradata_values)

    def test_pdf_upload(self):
        self.index()
        self.login('b1019547')

        self.add_session_extradata_template(**base_infos.extradata_with_pdf_default)
        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)

        self.fill_session_extradata(models.Session.objects.all()[0].id, base_infos.extradata_pdf_values)

        self.wait_for_table()
        self.show_view('subjectdb.views.session.SessionEdit', {'pk': models.Session.objects.all()[0].id})
        self.wait_until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Download')))
        self.selenium.find_element_by_id('form').submit()
        self.wait_for_table()
        self.show_view('subjectdb.views.session.SessionEdit', {'pk': models.Session.objects.all()[0].id})
        self.wait_until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Download')))

        with contextlib.suppress(FileNotFoundError):
            Path(self.unittest_dir, Path(base_infos.extradata_pdf_values['A PDF Item']).name).unlink()

        self.selenium.find_element_by_partial_link_text('Download').click()

        cur_basename = base_infos.extradata_pdf_values['A PDF Item']
        cur_fname = Path(self.unittest_dir, cur_basename)
        with self._wait_for_downloaded_file(cur_fname) as uploaded_file, Path(cur_basename).open('rb') as original_file:
            self.assertEqual(uploaded_file.read(), original_file.read())

    def test_multi_pdf_upload(self):
        self.index()
        self.login('b1019547')

        new_pdf_extradata = copy.deepcopy(base_infos.extradata_with_pdf_default)
        new_pdf_extradata['dataitems'].update({'A second PDF': 'PDFItem'})

        self.add_session_extradata_template(**new_pdf_extradata)
        this_experiment = copy.deepcopy(base_infos.experiment)

        self.add_experiment(**this_experiment)
        self.add_session(this_experiment['acronym'], **base_infos.session)

        new_pdf_extradata_values = copy.deepcopy(base_infos.extradata_pdf_values)
        new_pdf_extradata_values.update(
            {'A second PDF': str(Path(settings.BASE_DIR, 'test_datasets', 'pdf', 'valid_pdf2.pdf'))}
        )

        self.fill_session_extradata(models.Session.objects.all()[0].id, new_pdf_extradata_values)
        self.wait_for_table()
        self.show_view('subjectdb.views.session.SessionEdit', {'pk': models.Session.objects.all()[0].id})
        self.wait_until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Download')))
        self.selenium.find_element_by_id('form').submit()
        self.wait_for_table()
        self.show_view('subjectdb.views.session.SessionEdit', {'pk': models.Session.objects.all()[0].id})
        self.wait_until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Download')))

        for cur_pdf_item in new_pdf_extradata_values:
            with contextlib.suppress(FileNotFoundError):
                Path(self.unittest_dir, Path(new_pdf_extradata_values[cur_pdf_item]).name).unlink()

        for cur_button in self.selenium.find_elements_by_partial_link_text('Download'):
            cur_button.click()

        for cur_pdf_item in new_pdf_extradata_values:
            cur_value = Path(new_pdf_extradata_values[cur_pdf_item])
            cur_fname = Path(self.unittest_dir, cur_value.name)
            with (
                self._wait_for_downloaded_file(cur_fname) as uploaded_file,
                Path(cur_value).open('rb') as original_file,
            ):
                self.assertEqual(uploaded_file.read(), original_file.read())

    def _assert_extradatatemplate(self, object, info):
        self.assertEqual(object.name, info['name'])
        self.assertEqual(object.description, info['description'])

        if 'dataitems' in info:
            for label, type in info['dataitems'].items():
                cur_item = object.extradatatemplateitem_set.get(label=label)
                self.assertEqual(cur_item.type, type)

    def _assert_permission_failure(self):
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
        )

    def _test_all_views(self, should_fail, prepared_session_extradata, prepared_subject_extradata):
        self.show_view(subjectdb.views.extradata.SessionExtraData)
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('All Optional SessionExtraData'))

        self.show_view(subjectdb.views.extradata.SessionExtraDataCreate, {'is_default_extradata': False})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Create new SessionExtraData'))

        self.show_view(subjectdb.views.extradata.SessionExtraDataEdit, {'pk': prepared_session_extradata.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Edit SessionExtraData'))

        self.show_view(subjectdb.views.extradata.SubjectExtraData)
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('All Optional SubjectExtraData'))

        self.show_view(subjectdb.views.extradata.SubjectExtraDataCreate, {'is_default_extradata': False})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Create new SubjectExtraData'))

        self.show_view(subjectdb.views.extradata.SubjectExtraDataEdit, {'pk': prepared_subject_extradata.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Edit SubjectExtraData'))

        self.show_view(subjectdb.views.extradata.ExtraDataDetails, {'pk': prepared_session_extradata.id})
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('Extra Data'))
