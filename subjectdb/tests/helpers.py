# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import copy
import datetime
import random
import time

from django.contrib import auth
from django.contrib.auth import get_user_model
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import QuerySet
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa: N812
from selenium.webdriver.support.ui import Select

import obob_subjectdb.tests.helpers
import subjectdb.views.experiments
import subjectdb.views.extradata
import subjectdb.views.session
import subjectdb.views.specialgroups
import subjectdb.views.subjectbase
import th_django.modelfields.subject_id
import th_django.test
from subjectdb import models
from subjectdb.tests import base_infos
from th_django.modelfields.subject_id import SubjectID
from th_django.templatetags.th_django import date_format, datetime_format
from th_django.test import EC_upload_ok
from th_django.utils.misc import random_string


class UITestMixin(obob_subjectdb.tests.helpers.UITestMixin):
    def add_subject(
        self,
        mother_first_name,
        mother_maiden_name,
        birthday,
        gender,
        handedness,
        available_for_experiments,
        subject_notes='',
        personal_info_consent=False,
        special_subject_groups=None,
        **kwargs,
    ):
        self.show_view(subjectdb.views.subjectbase.Subjects)
        self.wait_for_table()
        self.click_navbutton()
        self.wait_until(EC.visibility_of_element_located((By.ID, 'add_subject')))
        self.selenium.find_element_by_id('add_subject').click()
        self.wait_until(EC.visibility_of_any_elements_located((By.ID, 'id_mother_first_name')))
        self.selenium.find_element_by_id('id_mother_first_name').send_keys(mother_first_name)
        self.selenium.find_element_by_id('id_mother_maiden_name').send_keys(mother_maiden_name)
        self.selenium.find_element_by_id('id_birthday').send_keys(birthday.strftime(date_format()))
        self.selenium.find_element_by_id('id_gender').send_keys(gender)
        self.selenium.find_element_by_id('id_handedness').send_keys(handedness)

        if subject_notes:
            self.selenium.find_element_by_id('id_notes').send_keys(subject_notes)

        if special_subject_groups:
            special_subject_groups_select = Select(self.selenium.find_element_by_name('special_groups'))
            for cur_group in special_subject_groups:
                special_subject_groups_select.select_by_visible_text(cur_group.group_name)

        available_element = self.selenium.find_element_by_id('id_available_for_experiments')
        if available_element.is_selected() != available_for_experiments:
            available_element.click()

        if personal_info_consent:
            self.selenium.find_element_by_id('id_consent_personal_info').click()
            self.add_personal_info(**kwargs)

        self.selenium.find_element_by_id('form').submit()

    def add_subject_db(
        self,
        mother_first_name,
        mother_maiden_name,
        birthday,
        gender,
        handedness,
        available_for_experiments,
        subject_notes='',
        personal_info_consent=False,
        special_subject_groups=None,
        **kwargs,
    ):
        subject = models.SubjectBase()
        subject.subject_id = SubjectID.generate_subject_id(birthday, mother_first_name, mother_maiden_name)
        subject.birthday = birthday
        subject.gender = gender
        subject.handedness = handedness
        subject.available_for_experiments = available_for_experiments
        if subject_notes:
            subject.notes = subject_notes

        subject.added_by = self.get_current_user()

        subject.full_clean()

        if special_subject_groups:
            subject.special_groups.set(special_subject_groups)

        if personal_info_consent:
            subject.consent_personal_info = True
            p_info = models.SubjectPersonalInformation()
            p_info.first_name = kwargs['first_name']
            p_info.last_name = kwargs['last_name']
            p_info.email = kwargs['email']
            p_info.mobile_phone = kwargs['phone_number']
            p_info.has_glasses = kwargs['wears_glasses']
            p_info.can_do_MEG = kwargs['can_do_meg']
            p_info.can_do_MRI = kwargs['can_do_mri']

            subject.save()
            p_info.parent_subject = subject
            p_info.save()
        else:
            subject.consent_personal_info = False
            subject.save()

        try:
            self.show_view(subjectdb.views.subjectbase.Subjects)
        except ValueError as e:
            if e.args[0] != 'live_server_url has not been set':
                raise

        return subject

    def add_personal_info(self, **kwargs):
        self.wait_until(EC.visibility_of_any_elements_located((By.ID, 'id_first_name')))
        self.selenium.find_element_by_id('id_first_name').clear()
        self.selenium.find_element_by_id('id_first_name').send_keys(kwargs['first_name'])
        self.selenium.find_element_by_id('id_last_name').clear()
        self.selenium.find_element_by_id('id_last_name').send_keys(kwargs['last_name'])
        self.selenium.find_element_by_id('id_email').clear()
        self.selenium.find_element_by_id('id_email').send_keys(kwargs['email'])
        self.selenium.find_element_by_id('id_mobile_phone').clear()
        self.selenium.find_element_by_id('id_mobile_phone').send_keys(kwargs['phone_number'])

        wears_glasses = self.selenium.find_element_by_id('id_has_glasses')
        if wears_glasses.is_selected() != kwargs['wears_glasses']:
            wears_glasses.click()

        can_do_meg = self.selenium.find_element_by_id('id_can_do_MEG')
        if can_do_meg.is_selected() != kwargs['can_do_meg']:
            can_do_meg.click()

        can_do_mri = self.selenium.find_element_by_id('id_can_do_MRI')
        if can_do_mri.is_selected() != kwargs['can_do_mri']:
            can_do_mri.click()

    def delete_subject(self, subject_id, already_in_view=False):
        if not already_in_view:
            self.show_view(subjectdb.views.subjectbase.Subjects)
            self.wait_for_table()

        self.wait_until(EC.element_to_be_clickable((By.NAME, 'delete_button')))

        delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == subject_id
        ][0]
        delete_button.click()

        self.wait_click_and_wait((By.ID, 'okButton'))

    def undelete_subject(self, subject_id):
        self.show_view(subjectdb.views.subjectbase.Subjects)
        self.wait_click_and_wait((By.ID, 'undelete_subjects'))
        self.wait_until(EC.element_to_be_clickable((By.ID, subject_id)))
        self.wait_click_and_wait((By.ID, subject_id))

    def fill_experiment(
        self,
        acronym,
        description,
        uses_eeg=False,
        uses_meg=False,
        uses_fmri=False,
        uses_eyetracker=False,
        additional_investigators=None,
        extradata=None,
        anc_url='',
        sinuhe_project=None,
        paradigm_url='',
    ):
        self.selenium.find_element_by_id('id_acronym').clear()
        self.selenium.find_element_by_id('id_acronym').send_keys(acronym)
        self.selenium.find_element_by_id('id_short_description').clear()
        self.selenium.find_element_by_id('id_short_description').send_keys(description)
        self.selenium.find_element_by_id('id_anc_url').clear()
        self.selenium.find_element_by_id('id_anc_url').send_keys(anc_url)
        self.selenium.find_element_by_id('id_paradigm_url').clear()
        self.selenium.find_element_by_id('id_paradigm_url').send_keys(paradigm_url)

        sinuhe_select = Select(self.selenium.find_element_by_id('id_sinuhe_project'))
        if sinuhe_project and sinuhe_project != '':
            sinuhe_select.select_by_visible_text(sinuhe_project)
        else:
            sinuhe_select.select_by_visible_text('---')

        uses_eeg_element = self.selenium.find_element_by_id('id_uses_eeg')
        if uses_eeg_element.is_selected() != uses_eeg:
            uses_eeg_element.click()

        uses_meg_element = self.selenium.find_element_by_id('id_uses_meg')
        if uses_meg_element.is_selected() != uses_meg:
            uses_meg_element.click()

        uses_fmri_element = self.selenium.find_element_by_id('id_uses_fmri')
        if uses_fmri_element.is_selected() != uses_fmri:
            uses_fmri_element.click()

        uses_eyetracker_element = self.selenium.find_element_by_id('id_uses_eyetracker')
        if uses_eyetracker_element.is_selected() != uses_eyetracker:
            uses_eyetracker_element.click()

        usermodel = auth.get_user_model()
        add_inv_field = Select(self.selenium.find_element_by_id('id_additional_investigators'))
        add_inv_field.deselect_all()

        if additional_investigators:
            for investigator in additional_investigators:
                try:
                    cur_user = usermodel.objects.get(username=investigator)
                    add_inv_field.select_by_visible_text(f'{cur_user.last_name}, {cur_user.first_name}')
                except usermodel.DoesNotExist:
                    pass

        extradata_select = Select(self.selenium.find_element_by_id('id_extra_data'))

        if extradata:
            for cur_extradata in extradata:
                extradata_select.select_by_visible_text(cur_extradata)

    def add_experiment(
        self,
        acronym,
        description,
        uses_eeg=False,
        uses_meg=False,
        uses_fmri=False,
        uses_eyetracker=False,
        additional_investigators=None,
        extradata=None,
        anc_url='',
        sinuhe_project='',
        paradigm_url='',
    ):
        self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'show_all_experiments': 'true'})
        self.wait_for_table()
        self.wait_click_and_wait((By.ID, 'add_experiment'))
        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_acronym')))
        self.fill_experiment(
            acronym,
            description,
            uses_eeg,
            uses_meg,
            uses_fmri,
            uses_eyetracker,
            additional_investigators,
            extradata,
            anc_url,
            sinuhe_project,
            paradigm_url,
        )

        self.selenium.find_element_by_id('form').submit()

        return models.Experiment.objects.get(acronym=acronym)

    def add_experiment_db(
        self,
        acronym,
        description,
        uses_eeg=False,
        uses_meg=False,
        uses_fmri=False,
        uses_eyetracker=False,
        additional_investigators=None,
        sinuhe_project='',
        anc_url='',
        paradigm_url='',
    ):
        new_exp = models.Experiment()
        new_exp.acronym = acronym
        new_exp.short_description = description
        new_exp.uses_eeg = uses_eeg
        new_exp.uses_meg = uses_meg
        new_exp.uses_fmri = uses_fmri
        new_exp.uses_eyetracker = uses_eyetracker
        new_exp.anc_url = anc_url
        new_exp.sinuhe_project = sinuhe_project
        new_exp.paradigm_url = paradigm_url

        new_exp.save()

        if additional_investigators:
            for cur_investigator in additional_investigators:
                new_exp.additional_investigators.add(auth.get_user_model().objects.get(username=cur_investigator))

        new_exp.added_by = self.get_current_user()
        new_exp.save()

        try:
            self.show_view(subjectdb.views.experiments.Experiments)
        except ValueError as e:
            if e.args[0] != 'live_server_url has not been set':
                raise

        return new_exp

    def delete_experiment(self, acronym):
        id = models.Experiment.objects.get(acronym=acronym).id

        self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'show_all_experiments': 'true'})
        self.wait_until(EC.element_to_be_clickable((By.NAME, 'delete_button')))

        delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == str(id)
        ][0]
        delete_button.click()

        self.wait_click_and_wait((By.ID, 'okButton'))

    def undelete_experiment(self, acronym):
        id = models.Experiment.objects.get(acronym=acronym).id

        self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'show_all_experiments': 'true'})
        self.wait_click_and_wait((By.ID, 'undelete_experiment'))
        self.wait_until(EC.element_to_be_clickable((By.ID, str(id))))
        self.wait_click_and_wait((By.ID, str(id)))

    def add_extradata_to_existing_experiment(self, experiment, extra_data):
        self.show_view(subjectdb.views.experiments.ExperimentEdit, {'pk': experiment.id})
        self.wait_until(EC.visibility_of_element_located((By.NAME, 'acronym')))
        extra_data_select = Select(self.selenium.find_element_by_name('extra_data'))
        for cur_extra_data in extra_data:
            extra_data_select.select_by_visible_text(cur_extra_data.name)

        self.selenium.find_element_by_id('form').submit()

    def add_specialsubjectgroup(self, group_name, extra_data=None):
        self.show_view(subjectdb.views.specialgroups.SpecialGroups)
        self.wait_for_table()
        self.wait_until(EC.element_to_be_clickable((By.ID, 'add_specialsubjectgroup')))
        self.selenium.find_element_by_id('add_specialsubjectgroup').click()
        self.wait_until(EC.visibility_of_element_located((By.NAME, 'group_name')))

        self.selenium.find_element_by_name('group_name').send_keys(group_name)

        if extra_data:
            extra_data_select = Select(self.selenium.find_element_by_name('extra_data'))
            for cur_extra_data in extra_data:
                extra_data_select.select_by_visible_text(cur_extra_data.name)

        self.selenium.find_element_by_id('form').submit()

        return models.SpecialSubjectGroups.objects.get(group_name=group_name)

    def add_specialsubjectgroup_db(self, group_name, extra_data=None):
        new_group = models.SpecialSubjectGroups(group_name=group_name)
        new_group.save()
        if extra_data:
            new_group.extra_data.set(extra_data)

        return new_group

    def del_specialsubjectgroup(self, group_name):
        self.show_view(subjectdb.views.specialgroups.SpecialGroups)
        self.wait_for_table()

        special_group_id = models.SpecialSubjectGroups.objects.get(group_name=group_name).pk
        delete_button = self.selenium.find_element_by_xpath(f'//*[@data-subject="{special_group_id:d}"]')
        delete_button.click()
        self.wait_click_and_wait((By.ID, 'okButton'))

    def del_sessionextradata(self, extradata_name, is_default_extradata=False):
        if is_default_extradata:
            self.show_view(subjectdb.views.extradata.DefaultSessionExtraData)
        else:
            self.show_view(subjectdb.views.extradata.SessionExtraData)
        self.wait_for_table()

        extradata_id = models.ExtraDataTemplate.objects.get(name=extradata_name).pk
        delete_button = self.selenium.find_element_by_xpath(f'//*[@data-subject="{extradata_id:d}"]')
        delete_button.click()
        self.wait_click_and_wait((By.ID, 'okButton'))

    def del_subjectextradata(self, extradata_name, is_default_extradata=False):
        if is_default_extradata:
            self.show_view(subjectdb.views.extradata.DefaultSubjectExtraData)
        else:
            self.show_view(subjectdb.views.extradata.SubjectExtraData)
        self.wait_for_table()

        extradata_id = models.ExtraDataTemplate.objects.get(name=extradata_name).pk
        delete_button = self.selenium.find_element_by_xpath(f'//*[@data-subject="{extradata_id:d}"]')
        delete_button.click()
        self.wait_click_and_wait((By.ID, 'okButton'))

    def add_extradata_to_existing_specialsubjectgroup(self, group, extra_data):
        self.show_view(subjectdb.views.specialgroups.SpecialGroupEdit, {'pk': group.id})
        self.wait_until(EC.visibility_of_element_located((By.NAME, 'group_name')))
        extra_data_select = Select(self.selenium.find_element_by_name('extra_data'))
        for cur_extra_data in extra_data:
            extra_data_select.select_by_visible_text(cur_extra_data.name)

        self.selenium.find_element_by_id('form').submit()

    def add_session_db(
        self,
        exp_acronym,
        mother_first_name,
        mother_maiden_name,
        birthday,
        gender,
        handedness,
        personal_info_consent,
        available_for_experiments,
        datetime_measurement,
        pilot_session,
        notes,
        session_excluded,
        session_excluded_reason,
        subject_notes=None,
        personal_info=None,
    ):
        try:
            subject = models.SubjectBase.objects.get(
                subject_id__exact=SubjectID.generate_subject_id(birthday, mother_first_name, mother_maiden_name)
            )
            is_new_subject = False
        except models.SubjectBase.DoesNotExist:
            if personal_info:
                subject = self.add_subject_db(
                    mother_first_name,
                    mother_maiden_name,
                    birthday,
                    gender,
                    handedness,
                    available_for_experiments,
                    subject_notes,
                    personal_info_consent,
                    **personal_info,
                )
            else:
                subject = self.add_subject_db(
                    mother_first_name,
                    mother_maiden_name,
                    birthday,
                    gender,
                    handedness,
                    available_for_experiments,
                    subject_notes,
                    personal_info_consent,
                )
            is_new_subject = False

        experiment = models.Experiment.objects.get(acronym=exp_acronym)

        session = models.Session()
        session.subject = subject
        session.experiment = experiment
        session.datetime_measured = datetime_measurement
        session.notes = notes
        session.pilot_session = pilot_session
        session.excluded_from_experiment = session_excluded
        session.excluded_reason = session_excluded_reason

        session.save()

        return is_new_subject

    def add_session(
        self,
        exp_acronym,
        mother_first_name,
        mother_maiden_name,
        birthday,
        gender,
        handedness,
        personal_info_consent,
        available_for_experiments,
        datetime_measurement,
        pilot_session,
        notes,
        session_excluded,
        session_excluded_reason,
        subject_notes=None,
        personal_info=None,
        special_subject_groups=None,
    ):
        id = models.Experiment.objects.get(acronym=exp_acronym).id
        self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'show_all_experiments': 'true'})
        self.wait_for_table()
        self.wait_until(EC.element_to_be_clickable((By.NAME, 'add_session')))

        add_session_button = [
            x
            for x in self.find_elements_by_text('Add Session')
            if x.tag_name == 'a' and x.get_attribute('data-id') == str(id)
        ][0]
        add_session_button.click()

        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_mother_first_name')))

        is_pilot = self.selenium.find_element_by_id('id_pilot_session')
        if is_pilot.is_selected() != pilot_session:
            is_pilot.click()

        self.selenium.find_element_by_id('id_mother_first_name').send_keys(mother_first_name)
        self.selenium.find_element_by_id('id_mother_maiden_name').send_keys(mother_maiden_name)
        self.selenium.find_element_by_id('id_birthday').send_keys(birthday.strftime(date_format()))

        self.selenium.find_element_by_id('id_datetime_measured').clear()
        self.selenium.find_element_by_id('id_datetime_measured').send_keys(
            datetime_measurement.strftime(datetime_format())
        )
        self.selenium.find_element_by_id('id_notes').send_keys(notes)

        sub_select = Select(self.selenium.find_element_by_id('id_subject'))
        if sub_select.first_selected_option.text == 'New Subject':
            is_new_subject = True
        else:
            is_new_subject = False

        self.selenium.find_element_by_id('form').submit()

        subject_needs_review = True
        try:
            self.find_element_by_text('Please review the subject data')
        except NoSuchElementException:
            subject_needs_review = False

        if subject_needs_review:
            self.wait_until(EC.visibility_of_element_located((By.ID, 'id_gender')))
            self.selenium.find_element_by_id('id_gender').send_keys(gender)
            self.selenium.find_element_by_id('id_handedness').send_keys(handedness)

            available_element = self.selenium.find_element_by_id('id_available_for_experiments')
            if available_element.is_selected() != available_for_experiments:
                available_element.click()

            if personal_info_consent and personal_info:
                self.selenium.find_element_by_id('id_consent_personal_info').click()
                self.add_personal_info(**personal_info)

            if subject_notes:
                self.selenium.find_element_by_id('id_notes').send_keys(subject_notes)

            if special_subject_groups:
                special_subject_groups_select = Select(self.selenium.find_element_by_name('special_groups'))
                for cur_group in special_subject_groups:
                    special_subject_groups_select.select_by_visible_text(cur_group.group_name)

            self.selenium.find_element_by_id('form').submit()

        if is_new_subject:
            self.wait_until(EC.visibility_of_any_elements_located((By.ID, 'id_mother_first_name')))
            self.selenium.find_element_by_id('id_gender').send_keys(gender)
            self.selenium.find_element_by_id('id_handedness').send_keys(handedness)

            available_element = self.selenium.find_element_by_id('id_available_for_experiments')
            if available_element.is_selected() != available_for_experiments:
                available_element.click()

            if personal_info_consent and personal_info:
                self.selenium.find_element_by_id('id_consent_personal_info').click()
                self.add_personal_info(**personal_info)

            if subject_notes:
                self.selenium.find_element_by_id('id_notes').send_keys(subject_notes)

            if special_subject_groups:
                special_subject_groups_select = Select(self.selenium.find_element_by_name('special_groups'))
                for cur_group in special_subject_groups:
                    special_subject_groups_select.select_by_visible_text(cur_group.group_name)

            self.selenium.find_element_by_id('form').submit()

        if session_excluded:
            session_id = subjectdb.models.Experiment.objects.get(acronym=exp_acronym).session_set.order_by('-id')[0].id
            self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session_id})
            self.wait_until(EC.visibility_of_element_located((By.ID, 'id_excluded_from_experiment')))
            self.selenium.find_element_by_id('id_excluded_from_experiment').click()
            self.selenium.find_element_by_id('id_excluded_reason').send_keys(session_excluded_reason)
            self.selenium.find_element_by_id('form').submit()

        return is_new_subject

    def edit_session(self, exp_acronym, session_id, new_session):
        self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'show_all_experiments': 'true'})
        self.wait_for_table()
        self.find_element_by_text(exp_acronym).click()
        self.wait_for_table()
        edit_button = [
            x
            for x in self.find_elements_by_text('Edit')
            if x.tag_name == 'a' and x.get_attribute('data-id') == str(session_id)
        ][0]
        edit_button.click()
        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_subject')))
        is_pilot = self.selenium.find_element_by_id('id_pilot_session')
        if is_pilot.is_selected() != new_session['pilot_session']:
            is_pilot.click()

        self.selenium.find_element_by_id('id_notes').clear()
        self.selenium.find_element_by_id('id_notes').send_keys(new_session['notes'])

        self.selenium.find_element_by_id('form').submit()

    def delete_session(self, exp_acronym, session_id, ignore_disabled=False):
        self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'show_all_experiments': 'true'})
        self.wait_for_table()
        self.find_element_by_text(exp_acronym).click()
        self.wait_for_table()
        delete_button = [
            x
            for x in self.find_elements_by_text('Delete')
            if x.tag_name == 'button' and x.get_attribute('data-subject') == str(session_id)
        ][0]

        if ignore_disabled:
            self.selenium.execute_script("document.querySelector('.deletebutton').disabled = false")

        delete_button.click()
        self.wait_click_and_wait((By.ID, 'okButton'))

    def undelete_session(self, exp_acronym, session_id, ignore_disabled=False):
        self.show_view(subjectdb.views.experiments.Experiments, get_parameters={'show_all_experiments': 'true'})
        self.wait_for_table()
        self.find_element_by_text(exp_acronym).click()
        self.wait_for_table()
        self.selenium.find_element_by_id('session_trash').click()
        self.wait_for_table()
        undelete_button = [
            x
            for x in self.find_elements_by_text('Undelete')
            if x.tag_name == 'button' and x.get_attribute('id') == str(session_id)
        ][0]

        if ignore_disabled:
            self.selenium.execute_script("document.querySelector('.undeletebutton').disabled = false")

        undelete_button.click()

    def fill_session_extradata(self, session_id, extradata):
        self.show_view(subjectdb.views.session.SessionEdit, kwargs={'pk': session_id})
        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_subject')))
        for key, value in extradata.items():
            element = self.find_element_by_partial_id(key)

            if isinstance(value, datetime.date):
                value = value.strftime(date_format())  # noqa PLW2901

            if element.tag_name == 'select':
                for cur_value in value.split(','):
                    Select(element).select_by_visible_text(cur_value.strip())
            else:
                element.clear()
                element.send_keys(value)

            if element.get_property('type') == 'file':
                self.wait_until(EC_upload_ok(element))

        self.selenium.find_element_by_id('form').submit()

    def fill_subject_extradata(self, subject_id, extradata):
        self.show_view(subjectdb.views.subjectbase.SubjectEdit, kwargs={'subject_id': subject_id})
        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_special_groups')))
        for key, value in extradata.items():
            element = self.find_element_by_partial_id(key)

            if isinstance(value, datetime.date):
                value = value.strftime(date_format())  # noqa PLW2901

            if element.tag_name == 'select':
                for cur_value in value.split(','):
                    Select(element).select_by_visible_text(cur_value.strip())
            else:
                element.clear()
                element.send_keys(value)

            if element.get_property('type') == 'file':
                self.wait_until(EC_upload_ok(element))

        self.selenium.find_element_by_id('form').submit()

    def _create_experiments(self, n_exp, base_info=base_infos.experiment, db_direct=False):
        experiments = []
        experiment_acronyms = []
        experiment_baseinfo = []

        for i in range(n_exp):
            tmp_exp = copy.deepcopy(base_info)
            tmp_exp['acronym'] = random_string()
            if db_direct:
                self.add_experiment_db(**tmp_exp)
            else:
                self.add_experiment(**tmp_exp)
            experiments.append(models.Experiment.objects.get(acronym=tmp_exp['acronym']))
            experiment_acronyms.append(tmp_exp['acronym'])
            experiment_baseinfo.append(tmp_exp)

        return (experiments, experiment_acronyms, experiment_baseinfo)

    def _create_subjects(self, n_subjects, with_personal_info=False, db_direct=False):
        subjects = []
        subject_ids = []
        subject_baseinfo = []

        for i in range(n_subjects):
            tmp_subject = copy.deepcopy(base_infos.subject)
            tmp_subject['mother_first_name'] = random_string()
            tmp_subject['mother_last_name'] = random_string()
            tmp_subject['birthday'] = datetime.date(
                day=random.randrange(1, 31), month=9, year=random.randrange(1950, 1990)
            )
            tmp_subject['subject_notes'] = tmp_subject['mother_first_name']
            if with_personal_info:
                tmp_subject.update(base_infos.personal_info)
                tmp_subject['personal_info_consent'] = True

            if db_direct:
                self.add_subject_db(**tmp_subject)
            else:
                self.add_subject(**tmp_subject)

            subjects.append(models.SubjectBase.objects.get(notes=tmp_subject['subject_notes']))
            subject_ids.append(models.SubjectBase.objects.get(notes=tmp_subject['subject_notes']).subject_id)
            subject_baseinfo.append(tmp_subject)

        return (subjects, subject_ids, subject_baseinfo)

    def _create_sessions(self, exp_acronym, n_sessions, db_direct=False, session_info=base_infos.session):
        sessions = []
        session_ids = []

        for i in range(n_sessions):
            tmp_session = self._prepare_session(session_info)
            if db_direct:
                self.add_session_db(exp_acronym, **tmp_session)
            else:
                self.add_session(exp_acronym, **tmp_session)
            sessions.append(models.Session.objects.get(notes=tmp_session['notes']))
            session_ids.append(models.Session.objects.get(notes=tmp_session['notes']).id)

        return (sessions, session_ids)

    def _prepare_session(self, src_session_info):
        tmp_session = copy.deepcopy(src_session_info)
        tmp_session['mother_first_name'] = random_string()
        tmp_session['mother_maiden_name'] = random_string()
        tmp_session['subject_notes'] = tmp_session['mother_first_name']
        tmp_session['notes'] = random_string()
        tmp_session['birthday'] = datetime.date(
            day=random.randrange(1, 28), month=random.randrange(1, 12), year=random.randrange(1960, 2000)
        )

        return tmp_session

    def add_extradata_template_db(
        self, name, description, dataitems=None, options=None, is_session_data=True, is_default_data=False
    ):
        new_extradata = models.ExtraDataTemplate(is_session_extradata=is_session_data)
        new_extradata.name = name
        new_extradata.description = description
        new_extradata.added_by = self.get_current_user()
        new_extradata.is_default_extradata = is_default_data

        new_extradata.save()

        if dataitems:
            for label, type in dataitems.items():
                new_dataitem = models.ExtraDataTemplateItem(label=label, type=type, extra_data=new_extradata)
                if options is not None and label in options:
                    new_dataitem.field_options = options[label]
                new_dataitem.save()

        return new_extradata

    def add_extradata_template(
        self, name, description, dataitems=None, options=None, is_session_data=True, is_default_data=False
    ):
        button_link_text = 'Session Extra Data'
        default_optional_link_text = 'Optional Extra Data'

        if is_default_data:
            default_optional_link_text = 'Default Extra Data'

        if not is_session_data:
            button_link_text = 'Subject Extra Data'

        self.index()
        self.wait_for_table()
        self.selenium.find_element_by_id('admin_dropdown').click()
        self.wait_until(EC.visibility_of_element_located((By.LINK_TEXT, default_optional_link_text)))
        self.selenium.find_element_by_link_text(default_optional_link_text).click()
        self.wait_until(EC.visibility_of_element_located((By.LINK_TEXT, button_link_text)))
        self.selenium.find_element_by_link_text(button_link_text).click()
        self.wait_until(EC.visibility_of_element_located((By.LINK_TEXT, 'Add ExtraData')))
        self.selenium.find_element_by_link_text('Add ExtraData').click()
        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_name')))
        self.selenium.find_element_by_id('id_name').send_keys(name)
        self.selenium.find_element_by_id('id_description').send_keys(description)

        if dataitems:
            counter = 0
            for label, type in dataitems.items():
                self.selenium.find_element_by_id('id_add_item').click()
                self.wait_until(
                    EC.visibility_of_element_located((By.NAME, f'extradatatemplateitem_set-{counter:d}-label'))
                )
                self.selenium.find_element_by_name(f'extradatatemplateitem_set-{counter:d}-label').send_keys(label)
                type_select = Select(self.selenium.find_element_by_name(f'extradatatemplateitem_set-{counter:d}-type'))
                type_select.select_by_value(type)
                if options is not None and label in options:
                    self.selenium.find_element_by_name(
                        f'extradatatemplateitem_set-{counter:d}-field_options'
                    ).send_keys(options[label])
                counter += 1  # noqa SIM113

        self.selenium.find_element_by_id('form').submit()

        self.wait_for_table(timeout=120)

        new_extradata = models.ExtraDataTemplate.objects.get(name=name)
        return new_extradata

    def add_subject_extradata_template_db(self, name, description, dataitems=None, options=None, is_default_data=False):
        return self.add_extradata_template_db(
            name, description, dataitems, options, is_session_data=False, is_default_data=is_default_data
        )

    def add_subject_extradata_template(self, name, description, dataitems=None, options=None, is_default_data=False):
        return self.add_extradata_template(
            name, description, dataitems, options, is_session_data=False, is_default_data=is_default_data
        )

    def add_session_extradata_template_db(self, name, description, dataitems=None, options=None, is_default_data=False):
        return self.add_extradata_template_db(
            name, description, dataitems, options, is_session_data=True, is_default_data=is_default_data
        )

    def add_session_extradata_template(self, name, description, dataitems=None, options=None, is_default_data=False):
        return self.add_extradata_template(
            name, description, dataitems, options, is_session_data=True, is_default_data=is_default_data
        )

    def add_item_to_extradata(self, extra_data, items, options, is_session_data=True):
        edit_view = subjectdb.views.extradata.SessionExtraDataEdit

        if not is_session_data:
            edit_view = subjectdb.views.extradata.SubjectExtraDataEdit

        self.show_view(edit_view, {'pk': extra_data.pk})

        all_existing_extradata_delete_buttons = self.find_elements_by_text('Delete Item')
        counter = len([x for x in all_existing_extradata_delete_buttons if x.tag_name == 'button'])

        for label, type in items.items():
            self.selenium.find_element_by_id('id_add_item').click()
            self.selenium.find_element_by_name(f'extradatatemplateitem_set-{counter:d}-label').send_keys(label)
            type_select = Select(self.selenium.find_element_by_name(f'extradatatemplateitem_set-{counter:d}-type'))
            type_select.select_by_value(type)
            if options is not None and label in options:
                self.selenium.find_element_by_name(f'extradatatemplateitem_set-{counter:d}-field_options').send_keys(
                    options[label]
                )
            counter += 1

        self.selenium.find_element_by_id('form').submit()
        self.wait_for_table(120)

    def add_item_to_session_extradata(self, extra_data, item, options=None):
        return self.add_item_to_extradata(extra_data, item, options, is_session_data=True)

    def add_item_to_subject_extradata(self, extra_data, item, options=None):
        return self.add_item_to_extradata(extra_data, item, options, is_session_data=False)

    def del_items_from_extradata(self, extra_data, items, is_session_data=True):
        edit_view = subjectdb.views.extradata.SessionExtraDataEdit

        if not is_session_data:
            edit_view = subjectdb.views.extradata.SubjectExtraDataEdit

        self.show_view(edit_view, {'pk': extra_data.pk})

        for cur_item in items:
            delete_label_item = self.find_element_by_value(cur_item)
            del_button = self.find_element_by_text(
                'Delete Item', parent=delete_label_item.find_element(By.XPATH, '../..')
            )
            self.wait_until(EC.visibility_of(del_button))
            time.sleep(0.5)
            del_button.click()

        self.selenium.find_element_by_id('form').submit()
        self.wait_for_table()

    def del_items_from_session_extradata(self, extra_data, items):
        return self.del_items_from_extradata(extra_data, items, True)

    def del_items_from_subject_extradata(self, extra_data, items):
        return self.del_items_from_extradata(extra_data, items, False)

    def set_subject_gender(self, subject_id, gender, undo_disable_fields):
        edit_view = subjectdb.views.subjectbase.SubjectEdit
        self.show_view(edit_view, kwargs={'subject_id': subject_id})

        if undo_disable_fields:
            self.run_script("document.querySelector('#id_gender').disabled = false")
            self.run_script("document.querySelector('#id_handedness').disabled = false")

        gender_select = Select(self.selenium.find_element_by_name('gender'))
        gender_select.select_by_visible_text(gender)

        self.selenium.find_element_by_id('form').submit()
        self.wait_for_table()

    def set_subject_handedness(self, subject_id, handedness, undo_disable_fields):
        edit_view = subjectdb.views.subjectbase.SubjectEdit
        self.show_view(edit_view, kwargs={'subject_id': subject_id})

        if undo_disable_fields:
            self.run_script("document.querySelector('#id_gender').disabled = false")
            self.run_script("document.querySelector('#id_handedness').disabled = false")

        gender_select = Select(self.selenium.find_element_by_name('handedness'))
        gender_select.select_by_visible_text(handedness)

        self.selenium.find_element_by_id('form').submit()
        self.wait_for_table()


class ExtraAssertionMixin(StaticLiveServerTestCase):
    def assert_experiment(self, info):
        object = models.Experiment.objects.get(acronym=info['acronym'])
        self.assertEqual(object.short_description, info['description'])
        self.assertEqual(object.uses_eeg, info['uses_eeg'])
        self.assertEqual(object.uses_meg, info['uses_meg'])
        self.assertEqual(object.uses_fmri, info['uses_fmri'])
        self.assertEqual(object.uses_eyetracker, info['uses_eyetracker'])
        self.assertEqual(object.anc_url, info['anc_url'])
        self.assertEqual(object.sinuhe_project, info['sinuhe_project'])
        self.assertEqual(object.paradigm_url, info['paradigm_url'])

        if info['additional_investigators']:
            for add_inv in info['additional_investigators']:
                object.additional_investigators.get(username=add_inv)
        else:
            self.assertEqual(object.additional_investigators.count(), 0)

        return object

    def assert_experiment_detail(self, info):
        exp_fields_dict = {'description': 'short_description'}
        exp_values_dict = {
            'additional_investigators': lambda x: '\n'.join(
                [f'{i.last_name}, {i.first_name}' for i in get_user_model().objects.filter(username__in=x)]
            ),
        }

        self.show_view(subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': info['acronym']})
        self.wait_for_table()
        for key, value in info.items():
            this_key = exp_fields_dict.get(key, key)
            if key in exp_values_dict:
                value = exp_values_dict[key](value)  # noqa PLW2901
            desc_element = self.find_element_by_text(getattr(models.Experiment, this_key).field.verbose_name)
            value_element = desc_element.find_element(
                By.XPATH, "./ancestor::div[@class='row']/div[@class='col-md-9']/p"
            )
            assert value_element.text == str(value)

    def assert_extradata(self, extradata_from_db, extradata_from_baseinfo):
        for key, value in extradata_from_baseinfo.items():
            compare_value = extradata_from_db.dataitem_set.get(label=key).value
            if isinstance(compare_value, QuerySet):
                tmp_list = list()

                for cur_value in compare_value:
                    tmp_list.append(str(cur_value))

                compare_value = ', '.join(tmp_list)
            else:
                compare_value = str(compare_value)

            self.assertEqual(compare_value, str(value))

        self.assertListEqual(
            list(extradata_from_db.dataitem_set.all().values_list('label', flat=True)),
            list(extradata_from_baseinfo.keys()),
        )

    def assert_subject(self, object, base_info, personal_info=None):
        self.assertEqual(object.birthday, base_info['birthday'])
        self.assertEqual(object.gender, base_info['gender'])
        self.assertEqual(object.handedness, base_info['handedness'])
        self.assertEqual(object.available_for_experiments, base_info['available_for_experiments'])
        self.assertEqual(object.consent_personal_info, base_info['personal_info_consent'])
        self.assertEqual(object.subject_id, self._subject_id(base_info))

        if not personal_info and 'first_name' in base_info:
            personal_info = base_info

        if not base_info['personal_info_consent']:
            with self.assertRaises(ObjectDoesNotExist):
                object.subjectpersonalinformation
        else:
            try:
                pers_info = object.subjectpersonalinformation
                if personal_info:
                    self.assertEqual(pers_info.first_name, personal_info['first_name'])
                    self.assertEqual(pers_info.last_name, personal_info['last_name'])
                    self.assertEqual(pers_info.can_do_MEG, personal_info['can_do_meg'])
                    self.assertEqual(pers_info.can_do_MRI, personal_info['can_do_mri'])
                    self.assertEqual(pers_info.has_glasses, personal_info['wears_glasses'])
            except models.SubjectPersonalInformation.DoesNotExist:
                pass

    def _subject_id(self, info):
        return th_django.modelfields.subject_id.SubjectID.generate_subject_id(
            info['birthday'], info['mother_first_name'], info['mother_maiden_name']
        )
