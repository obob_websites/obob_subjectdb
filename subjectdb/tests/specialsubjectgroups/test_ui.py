# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa N812
from selenium.webdriver.support.ui import Select

import subjectdb.management.cronjobs
import subjectdb.views.specialgroups
import subjectdb.views.subjectbase
import th_django.test
import user_manager.tests.helpers
from subjectdb import models
from subjectdb.tests.helpers import UITestMixin


class SessionUiTests(th_django.test.SeleniumTestCase, user_manager.tests.helpers.LoginLogoutMixin, UITestMixin):
    def _assert_permission_failure(self):
        self.assertIsNotNone(
            self.find_element_by_text('You cannot view this page because you do not have the necessary permission.')
        )

    def _test_all_views(self, should_fail):
        self.show_view(subjectdb.views.specialgroups.SpecialGroups)
        if should_fail:
            self._assert_permission_failure()
        else:
            self.wait_for_table()
            self.assertIsNotNone(self.find_element_by_text('All Special Subject Groups'))

        self.show_view(subjectdb.views.specialgroups.SpecialGroupCreate)
        if should_fail:
            self._assert_permission_failure()
        else:
            self.assertIsNotNone(self.find_element_by_text('Create New Special Subject Group'))

    def test_add_session(self):
        self.index()
        self.login('b1019547')
        self.add_specialsubjectgroup('test_group')

        models.SpecialSubjectGroups.objects.get(group_name='test_group')

    def test_add_session_db(self):
        self.add_specialsubjectgroup_db('test_group')

        models.SpecialSubjectGroups.objects.get(group_name='test_group')

    def test_permissions(self):
        self.add_specialsubjectgroup_db('test_group')
        self.add_user_db('thht', ['Access SubjectDB'])

        self.index()
        self.login('b1019547')
        self._test_all_views(False)

        self.logout()
        self.login('thht')
        self._test_all_views(True)

    def test_add_and_delete_specialgroup_from_subject(self):
        test_group = self.add_specialsubjectgroup_db('test_group')
        self.add_user_db('thht', ['Access SubjectDB'])
        self.index()
        self.login('b1019547')

        (subjects, subject_ids, subject_baseinfo) = self._create_subjects(2, True, True)
        self.show_view(subjectdb.views.subjectbase.SubjectEdit, kwargs={'subject_id': subject_ids[0]})
        self.wait_until(EC.visibility_of_element_located((By.ID, 'id_special_groups')))
        specialgroups_field = Select(self.selenium.find_element_by_id('id_special_groups'))
        specialgroups_field.select_by_visible_text('test_group')
        self.selenium.find_element_by_id('form').submit()

        subjects[0].refresh_from_db()
        self.assertEqual(subjects[0].special_groups.first(), test_group)
        test_group.delete()

        new_subject_instance = models.SubjectBase.objects.get(subject_id=subject_ids[0])
        self.assertIsNone(new_subject_instance.special_groups.first())
        subjects[0].refresh_from_db()
        self.assertEqual(subjects[0], new_subject_instance)
