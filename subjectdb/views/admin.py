#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from django.views.generic import FormView
from django_auto_url.urls import reverse_classname
from dynamic_preferences.forms import global_preference_form_builder

from subjectdb.views.mixins import DefaultMixin
from th_django.views.mixins import MessageAfterPostMixin


class AdminSettings(DefaultMixin, MessageAfterPostMixin, FormView):
    main_header_html = 'Settings'

    def get_form_class(self):
        return global_preference_form_builder()

    def has_permission(self):
        return self.request.user.is_superuser

    def get_success_url(self):
        return reverse_classname(AdminSettings)

    def form_valid(self, form):
        form.update_preferences()
        self.message_text = 'Settings updated successfully.'
        return super().form_valid(form)
