#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import datetime
import distutils.util
import random

import django_filters
from dateutil.relativedelta import relativedelta
from django import forms
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mass_mail
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, QueryDict
from django.template import Context, Template
from django.utils.functional import cached_property
from django.views.generic import FormView
from django_auto_url import kwargs
from django_auto_url.urls import reverse_classname
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, Table
from dynamic_preferences.forms import global_preferences_registry

import mridb.models
from expose_to_template.mixins.expose_to_template import ExposeContextItem, ExposeToTemplateViewMixin
from subjectdb import models
from subjectdb.models import RecruitmentEmail
from subjectdb.views.columns import MriAvailableColumn, SubjectColumn
from subjectdb.views.experiments import ExperimentDetail
from subjectdb.views.filters import CustomBooleanFilter
from subjectdb.views.mixins import DefaultMixin
from th_django.views.mixins import MessageAfterPostMixin


class RecruitTable(Table):
    class Meta:
        model = models.SubjectBase
        fields = ['subject', 'datetime_added', 'age', 'gender', 'handedness', 'special_groups']
        orderable = False

    def __init__(self, data, mridb_list, **kwargs):
        super().__init__(data, **kwargs)
        self.mridb_list = mridb_list

    mri = MriAvailableColumn()
    subject = SubjectColumn(field='subject_id', orderable=True)


class RecruitFilter(django_filters.FilterSet):
    class Meta:
        model = models.SubjectBase
        fields = ['handedness', 'special_groups']

    handedness = django_filters.AllValuesMultipleFilter('handedness')
    mri_available = CustomBooleanFilter(method='mri_available_filter', label='MRI Available')
    special_groups = django_filters.ModelMultipleChoiceFilter(
        queryset=models.SpecialSubjectGroups.objects.all(), method='special_groups_filter'
    )

    age = django_filters.NumericRangeFilter(method='age_filter', label='Age')

    is_subject_in = django_filters.ModelMultipleChoiceFilter(
        queryset=lambda r: RecruitFilter.is_subject_in_qs(r),
        label='Has already participated in one of...',
        method='is_subject_in_filter',
    )

    def __init__(self, mridb_list, *args, **kwargs):
        self.mridb_list = mridb_list

        super().__init__(*args, **kwargs)

    @classmethod
    def is_subject_in_qs(cls, request):
        qs = models.Experiment.objects.filter(added_by=request.user)
        qs |= models.Experiment.objects.filter(additional_investigators__username__contains=request.user.username)
        qs = qs.exclude(pk=request.resolver_match.kwargs['experiment_pk'])
        qs = qs.distinct()
        return qs

    def mri_available_filter(self, queryset, name, value):
        if value:
            queryset = queryset.filter(subject_id__in=self.mridb_list)
        else:
            queryset = queryset.exclude(subject_id__in=self.mridb_list)

        return queryset

    def special_groups_filter(self, queryset, name, value):
        if not value:
            queryset = queryset.filter(special_groups__isnull=True)
        else:
            queryset = queryset.filter(special_groups__in=value)
        return queryset

    def age_filter(self, queryset, name, value):
        min_age = value.start
        max_age = value.stop

        if min_age:
            born_later_than = datetime.date.today() + relativedelta(years=-min_age)
            queryset = queryset.filter(birthday__lte=born_later_than)
        if max_age:
            born_before_than = datetime.date.today() + relativedelta(years=-max_age)
            queryset = queryset.filter(birthday__gte=born_before_than)

        return queryset

    def is_subject_in_filter(self, queryset, name, value):
        if len(value) == 0:
            return queryset

        q = None

        for cur_exp in value:
            all_subjects = cur_exp.session_set.all().values_list('subject', flat=True)
            cur_q = Q(id__in=all_subjects)

            if q is None:
                q = cur_q
            else:
                q = q | cur_q

        return queryset.filter(q).distinct()


class ExposeFullNameItem(ExposeContextItem):
    def __init__(self, tag='full_name', description='Full name of participant'):
        super().__init__(tag=tag, description=description, getfun_or_field=self._get_full_name)

    def _get_full_name(self, object, context):
        if context['recipient'] is not None and context['recipient'].subjectpersonalinformation is not None:
            return '{} {}'.format(
                context['recipient'].subjectpersonalinformation.first_name,
                context['recipient'].subjectpersonalinformation.last_name,
            )

        return ''


class ExposeSenderEmail(ExposeContextItem):
    def __init__(self, tag='sender_email', description='Email of sender (your email)'):
        super().__init__(tag=tag, description=description, getfun_or_field=self._get_sender_email)

    def _get_sender_email(self, object, context):
        return object.request.user.email


class ExposeSenderFullName(ExposeContextItem):
    def __init__(self, tag='sender_full_name', description='Full name of sender'):
        super().__init__(tag=tag, description=description, getfun_or_field=self._get_sender_full_name)

    def _get_sender_full_name(self, object, context):
        return ' '.join([object.request.user.first_name, object.request.user.last_name])


class EmailExposedItemsMixin(ExposeToTemplateViewMixin):
    expose_fields = [ExposeFullNameItem(), ExposeSenderEmail(), ExposeSenderFullName()]


class Recruit(DefaultMixin, SingleTableMixin, FilterView):
    permission_required = 'subjectdb.add_session'
    url_kwargs = [kwargs.int('experiment_pk')]
    table_class = RecruitTable
    template_name = 'subjectdb/recruit.html'
    filterset_class = RecruitFilter
    must_be_owner = True

    extra_context = {'extra_js': ['subjectdb/recruit.js']}

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['object'] = self.get_object()

        if models.RecruitmentEmail.objects.filter(
            experiment=self.get_object(), added_by=self.request.user, status='draft'
        ).exists():
            ctx['has_draft_mail'] = True

        return ctx

    @cached_property
    def previous_mail(self):
        return (
            models.RecruitmentEmail.objects.filter(
                added_by=self.request.user, experiment=self.get_object(), status='sent'
            )
            .order_by('-pk')
            .first()
        )

    def get_object(self):
        return models.Experiment.objects.get(pk=self.kwargs['experiment_pk'])

    def get_queryset(self):
        this_experiment_pk = self.kwargs['experiment_pk']
        q = models.SubjectBase.objects.filter(available_for_experiments=True, consent_personal_info=True)

        q = q.exclude(session__experiment__pk=this_experiment_pk)

        already_written_subjects = models.RecruitmentEmail.objects.filter(
            experiment=self.get_object(), status__in=['sent', 'sending']
        ).values_list('recipients__id', flat=True)

        q = q.exclude(pk__in=already_written_subjects)

        return q

    def get_main_header(self):
        exp_name = models.Experiment.objects.get(pk=self.kwargs['experiment_pk']).acronym

        return f'Recruit participants for: {exp_name}'

    def get_random_subject_ids(self):
        all_pks = self.object_list.values_list('pk', flat=True)
        n = min(len(all_pks), settings.MAX_RECRUIT_SUBJECTS_PER_EMAIL)
        random_pks = random.sample(list(all_pks), n)
        return random_pks

    def get(self, request, *args, **kwargs):
        self.mridb_list = mridb.models.Subject.objects.filter(deleted=False).all().values_list('subject_id', flat=True)

        rval = super().get(request, *args, **kwargs)

        request.session['email_recipients'] = self.get_random_subject_ids()
        request.session['current_filterset'] = list(self.get_filterset_kwargs(self.filterset_class)['data'].lists())

        return rval

    def get_filterset_kwargs(self, filterset_class):
        kwargs = super().get_filterset_kwargs(filterset_class)
        kwargs['mridb_list'] = self.mridb_list

        if kwargs['data'] is None:
            kwargs['data'] = QueryDict('')
            if self.previous_mail is not None:
                qd = QueryDict('', mutable=True)
                for cur_item in self.previous_mail.filter_settings:
                    qd.setlist(cur_item[0], cur_item[1])
                kwargs['data'] = qd

        return kwargs

    def get_table_kwargs(self):
        kwargs = super().get_table_kwargs()
        kwargs['mridb_list'] = self.mridb_list

        return kwargs


class ComposeEmailForm(forms.ModelForm):
    class Meta:
        model = RecruitmentEmail
        fields = ['subject', 'content']


class ComposeEmail(DefaultMixin, EmailExposedItemsMixin, FormView):
    form_class = ComposeEmailForm
    url_kwargs = [kwargs.int('experiment_pk'), kwargs.bool('reuse_draft')]
    permission_required = 'subjectdb.add_session'
    template_name = 'subjectdb/compose_email.html'

    extra_context = {'extra_js': ['subjectdb/compose_email.js']}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._save_draft = True

    def has_permission(self):
        has = super().has_permission()

        is_exp_owner = (
            self.experiment.added_by == self.request.user
            or self.request.user in self.experiment.additional_investigators.all()
            or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
        )

        return has and is_exp_owner

    def get_main_header(self):
        return f'Compose Email for Experiment {self.experiment.acronym}'

    @cached_property
    def experiment(self):
        return models.Experiment.objects.get(pk=self.kwargs['experiment_pk'])

    @cached_property
    def draft_mail(self):
        try:
            mail = models.RecruitmentEmail.objects.filter(
                added_by=self.request.user, experiment=self.experiment, status__in=['draft', 'sending']
            ).last()
            if mail is not None and mail.status == 'sending' and self._save_draft:
                mail.status = 'draft'
                mail.save()
        except ObjectDoesNotExist:
            mail = None

        return mail

    @cached_property
    def previous_mail(self):
        return (
            models.RecruitmentEmail.objects.filter(
                added_by=self.request.user, experiment=self.experiment, status='sent'
            )
            .order_by('-pk')
            .first()
        )

    def get_or_create_draft_mail(self):
        mail = self.draft_mail
        if mail is None:
            mail = models.RecruitmentEmail(
                experiment=self.experiment, status='draft', filter_settings=self.request.session['current_filterset']
            )
            mail.save()
            mail.recipients.set(self.request.session['email_recipients'])

            mail.save()

        return mail

    def get(self, request, reuse_draft, *args, **kwargs):
        reuse_draft = bool(distutils.util.strtobool(reuse_draft))
        if not reuse_draft and self.draft_mail:
            models.RecruitmentEmail.objects.filter(
                added_by=self.request.user, experiment=self.experiment, status__in=['draft', 'sending']
            ).delete()
            del self.draft_mail

        if 'email_recipients' not in self.request.session:
            raise RuntimeError('email_recipients should be in the session')

        if 'current_filterset' not in self.request.session:
            raise RuntimeError('current_filterset should be in the session')

        rval = super().get(request, *args, **kwargs)
        return rval

    def post(self, request, *args, **kwargs):
        if 'draft_update' in request.POST and request.POST['draft_update'] == 'true' and self._save_draft:
            return self.update_draft(request, *args, **kwargs)

        self._save_draft = False
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        mail_object = self.get_or_create_draft_mail()
        mail_object.status = 'sending'
        mail_object.save()
        mail_object.content = form.cleaned_data['content']
        mail_object.subject = form.cleaned_data['subject']
        mail_object.save()

        return HttpResponseRedirect(reverse_classname(CheckEmail, kwargs={'mail_pk': mail_object.pk}))

    def update_draft(self, request, *args, **kwargs):
        if not self._save_draft:
            return

        mail = self.get_or_create_draft_mail()

        if mail.status == 'draft':
            mail.subject = self.request.POST['subject']
            mail.content = self.request.POST['content']

            mail.save()

        return JsonResponse({})

    def get_initial(self):
        initial = super().get_initial()
        if self.draft_mail is not None:
            initial['subject'] = self.draft_mail.subject
            initial['content'] = self.draft_mail.content
        elif self.previous_mail is not None:
            initial['subject'] = self.previous_mail.subject
            initial['content'] = self.previous_mail.content

        return initial


class CheckEmail(DefaultMixin, EmailExposedItemsMixin, MessageAfterPostMixin, FormView):
    url_kwargs = [kwargs.int('mail_pk')]
    permission_required = 'subjectdb.add_session'
    form_class = ComposeEmailForm
    template_name = 'subjectdb/check_email.html'

    def get_main_header(self):
        return f'Check Email for Experiment {self.experiment.acronym}'

    @cached_property
    def mail(self):
        return models.RecruitmentEmail.objects.get(pk=self.kwargs['mail_pk'])

    @property
    def experiment(self):
        return self.mail.experiment

    def get_initial(self):
        g_preferences = global_preferences_registry.manager()
        initial = super().get_initial()
        email_template = Template(
            '\n\n'.join([self.mail.content, g_preferences['recruitmentemail__recruitment_email_footer']])
        )
        exposed_context = self.get_expose_context({'recipient': self.mail.recipients.first()})

        initial['subject'] = self.mail.subject
        initial['content'] = email_template.render(Context(exposed_context))

        return initial

    def get_form(self, form_class=None):
        form = super().get_form()
        form.fields['subject'].disabled = True
        form.fields['content'].disabled = True

        return form

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['edit_email_url'] = reverse_classname(
            ComposeEmail, kwargs={'experiment_pk': self.experiment.pk, 'reuse_draft': True}
        )

        return ctx

    def get_success_url(self):
        return reverse_classname(ExperimentDetail, kwargs={'acronym': self.experiment.acronym})

    def form_valid(self, form):
        g_preferences = global_preferences_registry.manager()
        sender = self.request.user.email
        email_template = Template(
            '\n\n'.join([self.mail.content, g_preferences['recruitmentemail__recruitment_email_footer']])
        )

        content_list = []
        for recipient in self.mail.recipients.all():
            exposed_context = self.get_expose_context({'recipient': recipient})

            content_list.append(
                (
                    self.mail.subject,
                    email_template.render(Context(exposed_context)),
                    sender,
                    (recipient.subjectpersonalinformation.email,),
                )
            )

        content_list.append((self.mail.subject, self.mail.content, sender, (sender,)))

        send_mass_mail(content_list, fail_silently=False)

        self.mail.status = 'sent'

        self.mail.save()

        models.RecruitmentEmail.objects.filter(
            added_by=self.request.user, experiment=self.experiment, status__in=['draft', 'sending']
        ).delete()

        del self.request.session['current_filterset']
        del self.request.session['email_recipients']

        self.message_text = (
            f'Email sent successfully to {len(content_list) - 1} participants of {self.experiment.acronym}.'
        )

        return super().form_valid(form)

    def has_permission(self):
        has = super().has_permission()

        is_exp_owner = (
            self.experiment.added_by == self.request.user
            or self.request.user in self.experiment.additional_investigators.all()
            or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
        )

        return has and is_exp_owner
