# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import django_filters
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.forms import NullBooleanSelect
from django_filters.fields import ModelChoiceField

import mridb
from subjectdb import models


class CustomBoolSelect(NullBooleanSelect):
    def __init__(self, attrs=None):
        super().__init__(attrs)
        self.choices = (('unknown', 'All'), ('true', 'Yes'), ('false', 'No'))


class CustomBooleanFilter(django_filters.BooleanFilter):
    def __init__(self, *args, **kwargs):
        kwargs['widget'] = CustomBoolSelect()
        super().__init__(*args, **kwargs)


class SubjectFilter(django_filters.FilterSet):
    class Meta:
        model = models.SubjectBase
        fields = ['subject_id']

    subject_id = django_filters.CharFilter(field_name='subject_id', lookup_expr='icontains', label='Subject ID')


class SessionFilter(django_filters.FilterSet):
    class Meta:
        model = models.Session
        fields = ['subject_id']

    subject_id = django_filters.CharFilter(
        field_name='subject__subject_id', lookup_expr='icontains', label='Subject ID'
    )


class RestingStateFilter(django_filters.FilterSet):
    class Meta:
        model = models.RestingStateSession
        fields = []

    information_complete = CustomBooleanFilter(method='info_complete_filter', label='Information Complete')
    mri_available = CustomBooleanFilter(method='mri_available_filter', label='MRI Available')
    age_at_measurement = django_filters.RangeFilter()
    acquired_during = django_filters.ModelMultipleChoiceFilter(
        queryset=lambda r: models.Experiment.objects.distinct(),
        label='Acquired during...',
        method='acquired_during_filter',
    )
    s_freq = django_filters.AllValuesMultipleFilter(label='Sampling Frequency')
    dewar_position = django_filters.AllValuesMultipleFilter(label='Dewar Angle')

    def __init__(self, *args, **kwargs):
        self.mridb_list = mridb.models.Subject.objects.filter(deleted=False).all().values_list('subject_id', flat=True)

        super().__init__(*args, **kwargs)

    def info_complete_filter(self, queryset, name, value):
        if value:
            return queryset.filter(~Q(subject__gender__exact='') & ~Q(subject__handedness__exact=''))
        else:
            return queryset.exclude(~Q(subject__gender__exact='') & ~Q(subject__handedness__exact=''))

    def mri_available_filter(self, queryset, name, value):
        if value:
            queryset = queryset.filter(subject__subject_id__in=self.mridb_list)
        else:
            queryset = queryset.exclude(subject__subject_id__in=self.mridb_list)

        return queryset

    def acquired_during_filter(self, queryset, name, value):
        if len(value) == 0:
            return queryset

        q = None

        for cur_exp in value:
            all_sessions = cur_exp.session_set.all()

            cur_q = Q(session__in=all_sessions)

            if q is None:
                q = cur_q
            else:
                q |= cur_q

        return queryset.filter(q).distinct()


class UserModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return f'{obj.first_name} {obj.last_name}'


class UserModelChoiceFilter(django_filters.ModelChoiceFilter):
    field_class = UserModelChoiceField

    def get_queryset(self, request):
        qs = (
            get_user_model()
            .objects.filter(id__in=self.parent.queryset.values_list('added_by'))
            .order_by('first_name', 'last_name')
        )

        return qs


class ExperimentFilter(django_filters.FilterSet):
    class Meta:
        model = models.Experiment
        fields = ['acronym', 'owner']

    owner = UserModelChoiceFilter(
        field_name='added_by',
        label='Owner',
    )

    methods = django_filters.MultipleChoiceFilter(
        choices=(
            ('eeg', 'EEG'),
            ('meg', 'MEG'),
            ('fmri', 'fMRI'),
            ('eyetracker', 'Eyetracker'),
        ),
        label='Methods',
        method='filter_methods',
    )

    def filter_methods(self, queryset, name, value):
        if 'eeg' in value:
            queryset = queryset.filter(uses_eeg=True)
        if 'meg' in value:
            queryset = queryset.filter(uses_meg=True)
        if 'fmri' in value:
            queryset = queryset.filter(uses_fmri=True)
        if 'eyetracker' in value:
            queryset = queryset.filter(uses_eyetracker=True)

        return queryset
