# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import django.contrib.messages
from django import forms
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.http import Http404, HttpResponseRedirect
from django.utils import timezone
from django.views.generic import DetailView, ListView
from django_auto_url import kwargs
from django_auto_url.urls import reverse_classname, reverse_local_classname
from django_tables2 import SingleTableMixin, Table
from extra_views import UpdateWithInlinesView

import subjectdb.views.experiments
import subjectdb.views.subjectbase
from subjectdb import models
from subjectdb.views.columns import ExperimentColumn, SubjectColumn, UndeleteSessionColumn
from subjectdb.views.extradata import ConcreteExtradataFormset
from subjectdb.views.mixins import DefaultMixin
from th_bootstrap.django_tables2.columns import BooleanColumn, Column, DateTimeColumn
from th_bootstrap.views.mixins import DetailFieldsMixin
from th_django.forms.widgets import ModelAsStringWidget
from th_django.views.edit import CreateWithForeignKeyView, DeleteToTrashcanView, UndeleteFromTrashcanView
from th_django.views.mixins import MessageAfterPostMixin


class SubjectsSessionsTable(Table):
    class Meta:
        fields = ['datetime_measured', 'experiment', 'pilot_session', 'notes']

    datetime_measured = DateTimeColumn()
    experiment = ExperimentColumn(field='experiment__acronym')
    pilot_session = BooleanColumn(orderable=False)
    notes = Column(orderable=False)


class SessionTrashTable(Table):
    class Meta:
        fields = ['datetime_measured', 'subject', 'pilot_session', 'notes']

    datetime_measured = DateTimeColumn()
    subject = SubjectColumn(field='subject__subject_id')
    pilot_session = BooleanColumn(orderable=False)
    notes = Column(orderable=False)
    undelete = UndeleteSessionColumn(
        'id',
        reverse_local_classname('SessionUndelete', return_url_name=True),
        must_be_owner=True,
        needs_permission='subjectdb.delete_session',
    )


class SessionEditForm(ModelForm):
    class Meta:
        model = models.Session
        fields = [
            'subject',
            'experiment',
            'pilot_session',
            'datetime_measured',
            'notes',
            'excluded_from_experiment',
            'excluded_reason',
        ]

    subject = forms.CharField(disabled=True, widget=ModelAsStringWidget(model=models.SubjectBase))
    experiment = forms.CharField(disabled=True, widget=ModelAsStringWidget(model=models.Experiment))
    datetime_measured = forms.DateTimeField(disabled=True, label='Date and Time of Measurement')
    notes = forms.CharField(label='Session Notes', required=False, widget=forms.Textarea)

    def clean(self):
        self.cleaned_data['subject'] = self.instance.subject
        self.cleaned_data['experiment'] = self.instance.experiment

        if not self.cleaned_data['excluded_from_experiment']:
            self.cleaned_data['excluded_reason'] = ''
        elif not self.cleaned_data['excluded_reason']:
            raise ValidationError('A reason for the exclusion must be given.')

        return super().clean()


class SessionAddForm(ModelForm):
    class Meta:
        model = models.Session
        fields = [
            'mother_first_name',
            'mother_maiden_name',
            'birthday',
            'subject',
            'pilot_session',
            'datetime_measured',
            'notes',
        ]

    mother_first_name = forms.CharField(label='Mothers first name', required=False)
    mother_maiden_name = forms.CharField(label='Mothers maiden name', required=False)
    birthday = forms.DateField(label='Birthday of the Participant', required=False)
    subject = forms.ModelChoiceField(
        queryset=models.SubjectBase.objects.filter(deleted=False), empty_label='New Subject', required=False
    )
    notes = forms.CharField(label='Session Notes', required=False, widget=forms.Textarea)

    def needs_new_subject(self):
        if not self.is_valid():
            return False

        return self.cleaned_data['subject'] is None

    def needs_more_info(self):
        if not self.is_valid() or self.cleaned_data['subject'] is None:
            return False

        return not self.cleaned_data['subject'].has_all_info


class SessionAdd(DefaultMixin, CreateWithForeignKeyView):
    model = models.Session
    foreign_model = models.Experiment
    foreign_model_field = 'acronym'
    parameter_url_kwarg = 'experiment_acronym'
    form_field = 'experiment'
    form_class = SessionAddForm
    permission_required = 'subjectdb.add_session'
    must_be_owner = True
    url_kwargs = [kwargs.string('experiment_acronym')]
    main_header_html = 'New Session'

    extra_context = {'extra_js': ['subjectdb/sessionadd_form.js']}

    def get_form(self, form_class=None):
        form = super().get_form()
        form.fields['datetime_measured'].initial = timezone.now()
        return form

    def get_success_url(self):
        if self.object.concreteextradata_set.all().exists():
            return reverse_classname(SessionEdit, kwargs={'pk': self.object.pk})
        else:
            return reverse_classname(
                subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': self.object.experiment.acronym}
            )

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if 'new_session_data' in request.session:
            del request.session['new_session_data']
        if 'new_subject_data' in request.session:
            del request.session['new_subject_data']

        if form.needs_new_subject() or form.needs_more_info():
            new_session_data = {
                'datetime_measured': form.data['datetime_measured'],
                'notes': form.data['notes'],
                'pilot_session': form.data.get('pilot_session', False),
                'experiment_acronym': kwargs['experiment_acronym'],
            }

            request.session['new_session_data'] = new_session_data

            if form.needs_new_subject():
                subject_data = {
                    'birthday': form.data['birthday'],
                    'mother_first_name': form.data['mother_first_name'],
                    'mother_maiden_name': form.data['mother_maiden_name'],
                }

                request.session['new_subject_data'] = subject_data

                url = reverse_classname(
                    subjectdb.views.subjectbase.SubjectCreate, kwargs={'use_data_from_session': True}
                )
            else:
                django.contrib.messages.add_message(
                    request,
                    django.contrib.messages.WARNING,
                    'Please review the subject data, especially gender and handedness',
                )

                url = reverse_classname(
                    subjectdb.views.subjectbase.SubjectEdit,
                    kwargs={'use_data_from_session': True, 'subject_id': form.cleaned_data['subject'].subject_id},
                )

            return HttpResponseRedirect(url)

        return super().post(request, *args, **kwargs)

    def is_owner(self):
        return (
            self.foreign_object.added_by == self.request.user
            or self.request.user in self.foreign_object.additional_investigators.all()
            or self.request.user.is_superuser
            or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
        )


class SessionEdit(DefaultMixin, UpdateWithInlinesView):
    model = models.Session
    must_be_owner = True
    permission_required = 'subjectdb.change_session'
    form_class = SessionEditForm
    inlines = [ConcreteExtradataFormset]
    template_name = 'subjectdb/sessionedit_form.html'

    extra_context = {'extra_js': ['subjectdb/sessionedit_form.js']}

    def get_success_url(self):
        return reverse_classname(
            subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': self.object.experiment.acronym}
        )

    def is_owner(self):
        object = self.get_object().experiment
        if object:
            return (
                object.added_by == self.request.user
                or self.request.user in object.additional_investigators.all()
                or self.request.user.is_superuser
                or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
            )
        else:
            return True

    def get_main_header(self):
        return f'Edit Session of Experiment {self.object.experiment}'


class SubjectSessions(DefaultMixin, SingleTableMixin, ListView):
    model = models.Session
    permission_required = 'subjectdb.view_subjectdb'
    table_class = SubjectsSessionsTable
    url_kwargs = [kwargs.string('subject_id')]

    def get_queryset(self):
        return (
            models.SubjectBase.objects.get(subject_id=self.kwargs['subject_id']).session_set.all().exclude(deleted=True)
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subject_id'] = self.kwargs['subject_id']

        return context

    def get_main_header(self):
        return 'Sessions for {}'.format(self.kwargs['subject_id'])


class SessionDelete(DefaultMixin, MessageAfterPostMixin, DeleteToTrashcanView):
    model = models.Session
    http_method_names = ['post', 'delete']
    permission_required = 'subjectdb.delete_session'
    must_be_owner = True
    message_priority = messages.SUCCESS

    def get_message_text(self):
        return 'Successfully deleted the session.'

    def get_success_url(self):
        return reverse_classname(
            subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': self.object.experiment.acronym}
        )

    def is_owner(self):
        object = self.get_object()
        if object:
            return (
                self.request.user in (object.added_by, object.experiment.added_by)
                or self.request.user.is_superuser
                or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
            )
        else:
            return True


class SessionUndelete(DefaultMixin, MessageAfterPostMixin, UndeleteFromTrashcanView):
    model = models.Session
    http_method_names = ['post']
    permission_required = 'subjectdb.delete_session'
    must_be_owner = True
    message_priority = messages.SUCCESS
    url_kwargs = [kwargs.int('pk')]

    def get_message_text(self):
        return f'Successfully undeleted experiment {self.object.experiment.acronym}.'

    def is_owner(self):
        object = self.get_object()
        if object:
            return (
                self.request.user in (object.added_by, object.experiment.added_by)
                or self.request.user.is_superuser
                or self.request.user.has_perm('subjectdb.view_edit_all_subjectdb')
            )
        else:
            return True

    def get_success_url(self):
        return reverse_classname(
            subjectdb.views.experiments.ExperimentDetail, kwargs={'acronym': self.object.experiment.acronym}
        )


class SessionTrash(DefaultMixin, DetailView, SingleTableMixin, DetailFieldsMixin):
    model = models.Experiment
    permission_required = 'subjectdb.delete_session'
    must_be_owner = True
    show_fields = ['acronym', 'short_description', 'uses_eeg', 'uses_meg', 'uses_fmri', 'additional_investigators']
    slug_field = 'acronym'
    slug_url_kwarg = 'experiment_acronym'
    table_class = SessionTrashTable
    url_kwargs = [kwargs.string('experiment_acronym')]

    def get_additional_investigators_string(self):
        values = []
        for investigator in self.object.additional_investigators.all():
            values.append(f'{investigator.last_name}, {investigator.first_name}')

        return '\n'.join(values)

    def get_table_data(self):
        return self.object.session_set.all().filter(deleted=True)

    def get_queryset(self):
        if models.Experiment.objects.get(acronym=self.kwargs['experiment_acronym']).deleted:
            raise Http404

        return super().get_queryset()

    def get_main_header(self):
        return f'Deleted Sessions of Experiment {self.object.acronym}'
