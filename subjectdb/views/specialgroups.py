# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import django.db.models
from django.contrib import messages
from django.views.generic import DeleteView, DetailView, ListView
from django_auto_url.urls import reverse_classname_lazy, reverse_local_classname
from django_tables2 import SingleTableMixin, Table

import subjectdb.views.subjectbase
import th_django.views.edit
from subjectdb import models
from subjectdb.views.columns import SpecialGroupColumn
from subjectdb.views.mixins import DefaultMixin
from th_bootstrap.django_tables2.columns import DeleteColumn, EditColumn
from th_bootstrap.views.mixins import DetailFieldsMixin
from th_django.views.mixins import MessageAfterPostMixin


class SpecialGroupsTable(Table):
    class Meta:
        model = models.SpecialSubjectGroups
        fields = ['group_name']

    group_name = SpecialGroupColumn()
    edit = EditColumn(reverse_local_classname('SpecialGroupEdit', return_url_name=True), 'pk')
    delete = DeleteColumn('id', 'confirm_delete', reverse_local_classname('SpecialGroupDelete', return_url_name=True))


class SpecialGroups(DefaultMixin, SingleTableMixin, ListView):
    model = models.SpecialSubjectGroups
    permission_required = 'subjectdb.add_specialsubjectgroups'
    table_class = SpecialGroupsTable
    is_index = True
    main_header_html = 'All Special Subject Groups'


class SpecialGroupsDetail(DefaultMixin, DetailView, SingleTableMixin, DetailFieldsMixin):
    model = models.SpecialSubjectGroups
    permission_required = 'subjectdb.add_specialsubjectgroups'
    show_fields = ['extra_data']
    table_class = subjectdb.views.subjectbase.SubjectListTable

    def get_main_header(self):
        return f'Special Subject Group: {self.object.group_name}'

    def get_extra_data_string(self):
        values = []
        for cur_extra_data in self.object.extra_data.all():
            values.append(cur_extra_data.name)

        return '\n'.join(values)

    def get_table_data(self):
        objects = self.object.subjectdb_subjectbase_special_groups.all()
        return objects.prefetch_related(
            django.db.models.Prefetch(
                'session_set', queryset=models.Session.objects.exclude(deleted=True), to_attr='valid_sessions'
            )
        )


class SpecialGroupEdit(DefaultMixin, th_django.views.edit.CreateUpdateView):
    model = models.SpecialSubjectGroups
    permission_required = 'subjectdb.add_specialsubjectgroups'
    fields = ['group_name', 'extra_data']
    success_url = reverse_classname_lazy(SpecialGroups)
    main_header_html = 'Create New Special Subject Group'

    extra_context = {'extra_js': ['subjectdb/subjectgroup_form.js']}

    def get_form(self, form_class=None):
        form = super().get_form(form_class)

        form.fields['extra_data'].queryset = form.fields['extra_data'].queryset.filter(
            is_session_extradata=False, is_default_extradata=False
        )

        return form

    def get_main_header(self):
        return f'Edit Subject Group {self.object.group_name}'


class SpecialGroupCreate(SpecialGroupEdit):
    url_kwargs = []
    url_ignore_pk = True

    def get_main_header(self):
        return 'Create New Special Subject Group'


class SpecialGroupDelete(DefaultMixin, MessageAfterPostMixin, DeleteView):
    model = models.SpecialSubjectGroups
    permission_required = 'subjectdb.delete_specialsubjectgroups'
    message_priority = messages.SUCCESS
    success_url = reverse_classname_lazy(SpecialGroups)

    def get_message_text(self):
        return 'Successfully deleted the group'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        all_subjects_ids = list(self.object.subjectdb_subjectbase_special_groups.all().values_list('id', flat=True))

        rval = super().post(request, *args, **kwargs)

        all_subjects = models.SubjectBase.objects.filter(pk__in=all_subjects_ids)

        for cur_subject in all_subjects:
            cur_subject.update_extradata()

        return rval
