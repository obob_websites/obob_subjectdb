# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import contextlib

import django.db.models
from django import forms
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.forms import ModelForm
from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic import DetailView
from django_auto_url import kwargs
from django_auto_url.urls import reverse_classname, reverse_classname_lazy, reverse_local_classname
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, Table
from extra_views import UpdateWithInlinesView

import mridb.models
from subjectdb import models
from subjectdb.validators import birthday_validator
from subjectdb.views.columns import (
    DeleteSubjectColumn,
    ExtradataCompleteColumn,
    MriLinkColumn,
    SubjectColumn,
    SubjectToSessionColumn,
)
from subjectdb.views.extradata import ConcreteExtradataFormset
from subjectdb.views.filters import SubjectFilter
from subjectdb.views.mixins import DefaultMixin
from th_bootstrap.django_tables2.columns import BooleanColumn, Column, DateTimeColumn, EditColumn, UndeleteColumn
from th_bootstrap.views.mixins import DetailFieldsForeignObjectMixin
from th_django.modelfields.subject_id import SubjectID
from th_django.utils.date import parse_date
from th_django.views.edit import CreateUpdateWithInlineTrashcanView, DeleteToTrashcanView, UndeleteFromTrashcanView
from th_django.views.mixins import MessageAfterPostMixin


class NewSubjectModelForm(ModelForm):
    class Meta:
        model = models.SubjectBase
        fields = ['mother_first_name']

    mother_first_name = forms.CharField(label='Mothers first name')
    mother_maiden_name = forms.CharField(label='Mothers maiden name')
    birthday = forms.DateField(label='Birthday of the Participant', validators=[birthday_validator])
    gender = forms.ChoiceField(label='Gender', choices=(('M', 'Male'), ('F', 'Female'), ('O', 'Other')))
    handedness = forms.ChoiceField(
        label='Handedness', choices=(('Right', 'Right'), ('Left', 'Left'), ('Ambidexter', 'Ambidexter'))
    )
    notes = forms.CharField(label='Subject Notes', required=False, widget=forms.Textarea)
    special_groups = forms.ModelMultipleChoiceField(
        label='Special Groups',
        queryset=models.SpecialSubjectGroups.objects.all(),  # noqa
        required=False,
    )
    available_for_experiments = forms.BooleanField(
        label='Subject available for Experiments', initial=True, required=False
    )
    consent_personal_info = forms.BooleanField(
        label='Consent for Personal Information provided', initial=False, required=False
    )

    def save(self, **kwargs):
        subject_id = SubjectID.generate_subject_id(
            birthday=self.cleaned_data['birthday'],
            mother_first_name=self.cleaned_data['mother_first_name'],
            mother_maiden_name=self.cleaned_data['mother_maiden_name'],
        )

        previous_object = self.Meta.model.objects.filter(subject_id=subject_id)

        if previous_object.exists():
            self.instance = previous_object.first()
            self.instance.deleted = False
            self.instance.datetime_deleted = None

        self.instance.birthday = self.cleaned_data['birthday']
        self.instance.notes = self.cleaned_data['notes']
        self.instance.gender = self.cleaned_data['gender']
        self.instance.subject_id = subject_id
        self.instance.consent_personal_info = self.cleaned_data['consent_personal_info']
        self.instance.available_for_experiments = self.cleaned_data['available_for_experiments']
        self.instance.handedness = self.cleaned_data['handedness']

        new_instance = super().save(**kwargs)
        new_instance.special_groups.set(self.cleaned_data['special_groups'])
        new_instance.save()

        return new_instance


class EditSubjectModelForm(ModelForm):
    class Meta:
        model = models.SubjectBase
        fields = [
            'subject_id',
            'birthday',
            'gender',
            'handedness',
            'notes',
            'special_groups',
            'available_for_experiments',
            'consent_personal_info',
        ]

    subject_id = forms.CharField(disabled=True)
    birthday = forms.DateField(disabled=True)
    gender = forms.ChoiceField(choices=(('M', 'Male'), ('F', 'Female'), ('O', 'Other')))
    handedness = forms.ChoiceField(
        label='Handedness', choices=(('Right', 'Right'), ('Left', 'Left'), ('Ambidexter', 'Ambidexter'))
    )
    available_for_experiments = forms.BooleanField(label='Subject available for Experiments', required=False)


class EditOnlySubjectExtradataModelForm(ModelForm):
    class Meta:
        model = models.SubjectBase
        fields = []


class SubjectPersonalInfoModelForm(ModelForm):
    class Meta:
        model = models.SubjectPersonalInformation
        fields = ['first_name', 'last_name', 'email', 'mobile_phone', 'has_glasses', 'can_do_MEG', 'can_do_MRI']


class SubjectListTable(Table):
    class Meta:
        model = models.SubjectBase
        fields = ['subject_id', 'datetime_added', 'birthday', 'gender', 'handedness']

    subject_id = SubjectColumn()
    datetime_added = DateTimeColumn()
    birthday = DateTimeColumn(orderable=False)
    gender = Column(orderable=False)
    handedness = Column(orderable=False)
    mri = MriLinkColumn()
    available_for_experiments = BooleanColumn(verbose_name='Subject available', orderable=False)
    consent_personal_info = BooleanColumn(verbose_name='Personal Information', orderable=False)
    data_complete = ExtradataCompleteColumn(orderable=False)
    sessions = SubjectToSessionColumn()
    edit = EditColumn(
        reverse_local_classname('SubjectEdit', return_url_name=True),
        'subject_id',
        needs_permission='subjectdb.change_subjectbase',
    )
    delete = DeleteSubjectColumn(
        'subject_id', 'confirm_delete', reverse_local_classname('SubjectDelete', return_url_name=True)
    )

    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)
        self.mridb_list = mridb.models.Subject.objects.filter(deleted=False).all().values_list('subject_id', flat=True)

    def before_render(self, request):
        if not models.ConcreteExtraData.objects.filter(subject__isnull=False).exists():
            self.columns.hide('data_complete')


class SubjectTrashTable(Table):
    class Meta(SubjectListTable.Meta):
        fields = ['subject_id', 'datetime_added', 'datetime_deleted']

    subject_id = Column()
    datetime_added = DateTimeColumn()
    datetime_deleted = DateTimeColumn()
    undelete = UndeleteColumn('subject_id', reverse_local_classname('SubjectUndelete', return_url_name=True))


class Subjects(DefaultMixin, SingleTableMixin, FilterView):
    model = models.SubjectBase
    permission_required = 'subjectdb.view_subjectdb'
    table_class = SubjectListTable
    is_index = True
    main_header_html = 'All Subjects'
    filterset_class = SubjectFilter
    template_name = 'subjectdb/subjectbase_list.html'

    def get_queryset(self):
        return (
            self.model.objects.filter(deleted=False, has_all_info=True)
            .prefetch_related('session_set')
            .prefetch_related('concreteextradata_set')
            .prefetch_related(
                django.db.models.Prefetch(
                    'session_set', queryset=models.Session.objects.exclude(deleted=True), to_attr='valid_sessions'
                )
            )
        )


class SubjectTrash(DefaultMixin, SingleTableMixin, FilterView):
    model = models.SubjectBase
    permission_required = 'subjectdb.delete_subjectbase'
    template_name = 'base_table.html'
    table_class = SubjectTrashTable
    main_header_html = 'Deleted Subjects'
    filterset_class = SubjectFilter

    def get_queryset(self):
        return self.model.objects.filter(deleted=True)


class SubjectDetail(DefaultMixin, DetailView, DetailFieldsForeignObjectMixin):
    model = models.SubjectBase
    permission_required = 'subjectdb.view_subjectdb'
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    show_fields = ['birthday', 'gender', 'handedness', 'available_for_experiments', 'special_groups', 'notes']
    foreign_field = 'subjectpersonalinformation'
    show_foreign_fields = [
        'first_name',
        'last_name',
        'email',
        'mobile_phone',
        'has_glasses',
        'can_do_MEG',
        'can_do_MRI',
    ]

    url_kwargs = [kwargs.string('subject_id')]

    def get_queryset(self):
        if models.SubjectBase.objects.get(subject_id=self.kwargs['subject_id']).deleted:
            raise Http404

        return models.SubjectBase.objects.filter(subject_id=self.kwargs['subject_id'])

    def get_main_header(self):
        return f'Subject: {self.object.subject_id}'

    def get_special_groups_string(self):
        values = []
        for cur_special_group in self.object.special_groups.all():
            values.append(cur_special_group.group_name)

        return '\n'.join(values)


class SubjectCreate(DefaultMixin, CreateUpdateWithInlineTrashcanView):
    model = models.SubjectBase
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    form_class = NewSubjectModelForm
    extra_modelform = SubjectPersonalInfoModelForm
    create_if_nonexistent = True
    main_header_html = 'New Subject'

    url_kwargs = [kwargs.string('use_data_from_session', 'False')]

    extra_context = {'js_bundle': 'subjectdb/subject_form.js'}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._new_session_pk = None

    def get_success_url(self):
        next_url = None

        if self.object.has_extradata:
            next_url = reverse_classname(EditOnlySubjectExtradata, kwargs={'subject_id': self.object.subject_id})

            with contextlib.suppress(KeyError):
                del self.request.session['next_view']

            if self._new_session_pk:
                self.request.session['next_view'] = {
                    'class_name': 'subjectdb.views.session.SessionEdit',
                    'kwargs': {'pk': self._new_session_pk},
                }

        elif self._new_session_pk:
            next_url = reverse_classname('subjectdb.views.session.SessionEdit', kwargs={'pk': self._new_session_pk})

        else:
            next_url = reverse_classname(Subjects)

        return next_url

    def get_permission_required(self):
        if self.slug_field in self.kwargs:
            return ('subjectdb.change_subjectbase',)
        else:
            return ('subjectdb.add_subjectbase',)

    def get_extra_form(self):
        if self.request.method in ('POST', 'PUT'):
            form = self.extra_modelform(data=self.request.POST)
            if hasattr(self, 'object') and hasattr(self.object, 'id'):
                form.instance.parent_subject_id = self.object.id
                with contextlib.suppress(ObjectDoesNotExist):
                    form.instance.id = self.object.subjectpersonalinformation.id  # noqa

            return form
        elif self.object and hasattr(self.object, 'subjectpersonalinformation'):
            return self.extra_modelform(instance=self.object.subjectpersonalinformation)
        else:
            return self.extra_modelform()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['extra_form'] = self.get_extra_form()

        return context

    def get_form_kwargs(self):
        self.object = self.get_object()
        kwargs = super().get_form_kwargs()
        if not self.object.subject_id:
            with contextlib.suppress(AttributeError, KeyError):
                kwargs['data']['subject_id'] = SubjectID.generate_subject_id(
                    parse_date(kwargs['data']['birthday']),
                    kwargs['data']['mother_first_name'],
                    kwargs['data']['mother_maiden_name'],
                )

            try:
                self.object = self.model.objects.get(subject_id__exact=kwargs['data']['subject_id'])
                kwargs['instance'] = self.object
            except (ObjectDoesNotExist, AttributeError, KeyError):
                pass

        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        if 'new_subject_data' in self.request.session:
            form.initial = self.request.session['new_subject_data']

        return form

    def get(self, request, *args, **kwargs):
        if 'use_data_from_session' not in kwargs or kwargs['use_data_from_session'] != 'True':
            self._clear_session()

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        extra_form = self.get_extra_form()
        if hasattr(self, 'object'):
            self.object.deleted = False
            self.object.datetime_deleted = None

        if form.is_valid():
            self.form_valid(form)
            if form.cleaned_data['consent_personal_info']:
                if extra_form.is_valid():
                    extra_form.instance.parent_subject_id = form.instance.id
                    extra_form.save()
                else:
                    return self.form_invalid(form)

            if 'new_session_data' in self.request.session:
                new_session_data = self.request.session['new_session_data']

                new_session = models.Session()
                new_session.datetime_measured = new_session_data['datetime_measured']
                new_session.notes = new_session_data['notes']
                new_session.pilot_session = new_session_data['pilot_session'] == 'on'
                new_session.experiment = models.Experiment.objects.get(acronym=new_session_data['experiment_acronym'])
                new_session.subject = form.instance
                new_session.save()

                self._new_session_pk = new_session.pk

            return super().post(request, *args, **kwargs)

        else:
            return self.form_invalid(form)

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def _clear_session(self):
        if 'new_session_data' in self.request.session:
            del self.request.session['new_session_data']

        if 'new_subject_data' in self.request.session:
            del self.request.session['new_subject_data']


class SubjectEdit(SubjectCreate):
    form_class = EditSubjectModelForm
    url_kwargs = [kwargs.string('subject_id'), kwargs.string('use_data_from_session', 'False')]
    inlines = [ConcreteExtradataFormset]

    def get_main_header(self):
        return f'Edit Subject {self.object.subject_id}'

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        may_edit_hand_gender = self.request.user.has_perm('subjectdb.change_subject_handedness_gender')
        form.fields['gender'].disabled = not may_edit_hand_gender
        form.fields['handedness'].disabled = not may_edit_hand_gender

        return form

    def get_message(self):
        pass


class EditOnlySubjectExtradata(DefaultMixin, UpdateWithInlinesView):
    model = models.SubjectBase
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    form_class = EditOnlySubjectExtradataModelForm
    inlines = [ConcreteExtradataFormset]
    url_kwargs = [kwargs.string('subject_id')]
    template_name = 'subjectdb/editonlyextradata.html'

    extra_context = {'extra_js': ['subjectdb/subject_only_extradata_form.js']}

    def get_main_header(self):
        return f'Please fill in the additional data for subject {self.object.subject_id}'

    def get_success_url(self):
        try:
            rval = reverse_classname(
                self.request.session['next_view']['class_name'], kwargs=self.request.session['next_view']['kwargs']
            )
            del self.request.session['next_view']
            return rval
        except KeyError:
            return reverse_classname(Subjects)


class SubjectDelete(DefaultMixin, MessageAfterPostMixin, DeleteToTrashcanView):
    model = models.SubjectBase
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    http_method_names = ['post', 'delete']
    success_url = reverse_classname_lazy(Subjects)
    permission_required = 'subjectdb.delete_subjectbase'
    message_priority = messages.SUCCESS
    url_kwargs = [kwargs.string('subject_id')]

    def get_message_text(self):
        return f'Successfully deleted subject {self.get_object().subject_id}.'

    def form_valid(self, form):
        self.object = self.get_object()
        if len(self.object.session_set.all().exclude(deleted=True)) > 0:
            raise ValueError('Cannot delete a subject that is still attached to a session')

        return super().form_valid(form)


class SubjectUndelete(DefaultMixin, MessageAfterPostMixin, UndeleteFromTrashcanView):
    model = models.SubjectBase
    slug_field = 'subject_id'
    slug_url_kwarg = 'subject_id'
    http_method_names = ['post']
    permission_required = 'subjectdb.delete_subjectbase'
    success_url = reverse_lazy('subjectdb:subjects:Subjects')
    message_priority = messages.SUCCESS
    url_kwargs = [kwargs.string('subject_id')]

    def get_message_text(self):
        return f'Successfully undeleted subject {self.get_object().subject_id}.'
