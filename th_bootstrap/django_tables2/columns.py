from django.template import Context
from django.urls import reverse_lazy
from django.utils.html import format_html
from django_tables2 import TemplateColumn, columns
from django_tables2.columns import library
from django_tables2.utils import A


class BootstrapColumnMixin(columns.Column):
    def __init__(self, sortable=False, searchable=False, valign='middle', needs_permission=None, **kwargs):
        super().__init__(**kwargs)

        self.needs_permission = needs_permission

        data_attrs = {
            'sortable': sortable,
            'searchable': searchable,
            'valign': valign,
        }

        if self.verbose_name:
            data_attrs['field'] = self.verbose_name

        if 'th' not in self.attrs:
            self.attrs['th'] = {}

        for data, value in data_attrs.items():
            if isinstance(value, bool):
                val = str(value).lower()
            else:
                val = str(value)

            self.attrs['th'].update({f'data-{data}': val})


class LinkColumnWithID(columns.LinkColumn):
    def render(self, value, record):
        self.attrs['a']['data-id'] = record.id
        return super().render(value, record)


class MustBeOwnerMixin:
    def __init__(self, must_be_owner=False, **kwargs):
        self._must_be_owner = must_be_owner
        super().__init__(**kwargs)

    def is_owner(self, context, record):
        user = context.request.user
        if self._must_be_owner:
            return user == record.added_by or user.is_superuser or user.has_perm('subjectdb.view_edit_all_subjectdb')

        return True


@library.register
class Column(BootstrapColumnMixin, columns.Column):
    pass


@library.register
class BooleanColumn(BootstrapColumnMixin, columns.BooleanColumn):
    pass


@library.register
class DateTimeColumn(Column):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'th' not in self.attrs:
            self.attrs['th'] = {}

        self.attrs['th']['class'] = 'data-has-iso-datetime'

    def render(self, value):
        if 'td' not in self.attrs:
            self.attrs['td'] = dict()

        self.attrs['td']['data-datetime-iso'] = format_html('{}', value.isoformat())
        return value


@library.register
class EditColumn(BootstrapColumnMixin, MustBeOwnerMixin, LinkColumnWithID):
    def __init__(self, viewname, field, must_be_owner=False, **kwargs):
        default_args = {
            'viewname': viewname,
            'args': [A(field)],
            'accessor': A(field),
            'text': 'Edit',
            'verbose_name': 'Edit',
            'attrs': {
                'a': {
                    'class': 'btn btn-default btn-sm',
                    'name': 'edit',
                }
            },
            'orderable': False,
        }

        default_args.update(kwargs)

        super().__init__(must_be_owner=must_be_owner, **default_args)

    def render(self, record, table, value, **kwargs):
        context = getattr(table, 'context', Context())

        self.attrs['a']['class'] = self.attrs['a']['class'].replace('disabled', '')

        if not self.is_owner(context, record):
            self.attrs['a']['class'] = self.attrs['a']['class'] + ' disabled'

        return super().render(value, record)
        return super().render(value, record)


@library.register
class DeleteColumn(BootstrapColumnMixin, MustBeOwnerMixin, columns.TemplateColumn):
    def __init__(self, field, modal_id, delete_url, button_size='btn-sm', must_be_owner=False, **kwargs):
        kwargs['accessor'] = A(field)
        kwargs['template_name'] = 'delete_button.html'
        kwargs['orderable'] = False

        self._delete_url = delete_url
        self._modal_id = modal_id
        self._button_size = button_size
        self._field = field

        super().__init__(verbose_name='Delete', must_be_owner=must_be_owner, **kwargs)

    def render(self, record, table, value, **kwargs):
        context = getattr(table, 'context', Context())
        context.update(
            {
                'subject': getattr(record, self._field),
                'delete_url': reverse_lazy(self._delete_url, args=[getattr(record, self._field)]),
                'modal_id': self._modal_id,
                'button_size': self._button_size,
                'disabled': False,
            }
        )

        if not self.is_owner(context, record):
            context.update(
                {
                    'disabled': True,
                }
            )

        return super().render(record, table, value, **kwargs)


@library.register
class DownloadColumn(BootstrapColumnMixin, columns.TemplateColumn):
    def __init__(self, field, download_url, button_size='', **kwargs):
        kwargs['accessor'] = A(field)
        kwargs['template_name'] = 'bootstrap_fileinput_downloadbutton.html'
        kwargs['orderable'] = False

        self._field = field
        self._download_url = download_url
        self._button_size = button_size

        super().__init__(verbose_name='Download', **kwargs)

    def render(self, record, table, value, **kwargs):
        context = getattr(table, 'context', Context())
        context.update(
            {
                'url': reverse_lazy(self._download_url, args=[getattr(record, self._field)]),
                'button_size': self._button_size,
            }
        )

        return super().render(record, table, value, **kwargs)


@library.register
class Many2ManyColumn(BootstrapColumnMixin, columns.TemplateColumn):
    def __init__(self, **kwargs):
        kwargs['template_code'] = '{% for obj in value.all %} <p>{{ obj }}</p> {% endfor %}'
        kwargs['orderable'] = False

        super().__init__(**kwargs)


@library.register
class UndeleteColumn(BootstrapColumnMixin, MustBeOwnerMixin, TemplateColumn):
    def __init__(self, field, view, must_be_owner=False, **kwargs):
        self.field = field
        self.view = view

        kwargs['orderable'] = False

        kwargs.update(
            {
                'accessor': A(field),
                'template_code': '<form method="post" action={{ url }}> '
                '{% csrf_token %} <button {% if disabled %} '
                'disabled {% endif %} type="submit" '
                'class="btn btn-default btn-sm undeletebutton" '
                'name="undelete" id="{{ subject_id }}"> '
                'Undelete </button> </form>',
                'orderable': False,
            }
        )

        super().__init__(verbose_name='Undelete', must_be_owner=must_be_owner, **kwargs)

    def render(self, record, table, value, **kwargs):
        context = getattr(table, 'context', Context())
        context.update(
            {
                'url': reverse_lazy(self.view, args=[getattr(record, self.field)]),
                'subject_id': getattr(record, self.field),
                'disabled': False,
            }
        )

        if not self.is_owner(context, record):
            context.update(
                {
                    'disabled': True,
                }
            )

        return super().render(record, table, value, **kwargs)
