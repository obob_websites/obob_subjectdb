# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from collections import OrderedDict

from django.views.generic.detail import SingleObjectMixin


class DetailFieldsMixin(SingleObjectMixin):
    show_fields = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        detail_fields = OrderedDict()
        if self.object:
            for field in self.show_fields:
                if hasattr(self, f'get_{field}_string'):
                    value = getattr(self, f'get_{field}_string')()
                else:
                    try:
                        value = getattr(self.object, f'get_{field}_display')()
                    except AttributeError:
                        value = getattr(self.object, field)

                if hasattr(self, f'get_{field}_verbose_name'):
                    verbose_name = getattr(self, f'get_{field}_verbose_name')
                else:
                    verbose_name = self.model._meta.get_field(field).verbose_name  # noqa

                detail_fields[verbose_name] = value

        context['detail_fields'] = detail_fields
        return context


class DetailFieldsForeignObjectMixin(DetailFieldsMixin):
    foreign_field = None
    show_foreign_fields = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        detail_fields = OrderedDict()
        if self.object and hasattr(self.object, self.foreign_field):
            foreign_object = getattr(self.object, self.foreign_field)
            if foreign_object:
                foreign_model = foreign_object._meta.model
                for field in self.show_foreign_fields:
                    try:
                        value = getattr(foreign_object, f'get_{field}_display')()
                    except AttributeError:
                        value = getattr(foreign_object, field)

                    detail_fields[foreign_model._meta.get_field(field).verbose_name] = value

        context['detail_fields'].update(detail_fields)
        return context
