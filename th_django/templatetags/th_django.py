# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from django import template
from django.utils import formats

register = template.Library()

input_formats = formats.get_format_lazy('DATE_INPUT_FORMATS')
input_formats_datetime = formats.get_format_lazy('DATETIME_INPUT_FORMATS')


@register.simple_tag
def date_format(for_javascript=False):
    if not for_javascript:
        return input_formats[0]

    dateformat = input_formats[0]
    dateformat = dateformat.replace('%Y', 'yyyy').replace('%m', 'mm').replace('%d', 'dd')

    return dateformat


@register.simple_tag
def datetime_format(for_javascript=False):
    if not for_javascript:
        return input_formats_datetime[0]

    dateformat = input_formats_datetime[0]
    dateformat = (
        dateformat.replace('%Y', 'yyyy')
        .replace('%m', 'mm')
        .replace('%d', 'dd')
        .replace('%H', 'hh')
        .replace('%M', 'ii')
        .replace('%S', 'ss')
    )

    return dateformat
