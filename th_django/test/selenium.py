# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import abc
import contextlib
import shutil
import time
from pathlib import Path
from uuid import uuid4

from django.conf import settings
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import override_settings
from django_auto_url.urls import reverse_classname
from selenium.common.exceptions import (
    ElementNotVisibleException,
    NoSuchElementException,
    StaleElementReferenceException,
)
from selenium.webdriver import ChromeOptions
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC  # noqa N812
from selenium.webdriver.support.ui import WebDriverWait
from xvfbwrapper import Xvfb

old_submit = WebElement.submit


def new_submit(self):
    old_submit(self)
    time.sleep(0.1)


WebElement.submit = new_submit


class HasLiveServerUrl(abc.ABC):
    @property
    def live_server_url(self):
        if not hasattr(self, '_live_server_url') or not self._live_server_url:
            raise ValueError('live_server_url has not been set')
        return self._live_server_url

    @live_server_url.setter
    def live_server_url(self, val):
        self._live_server_url = val


class SeleniumMixin(HasLiveServerUrl):
    @property
    def selenium(self) -> WebDriver:
        if not hasattr(self, '_selenium') or not self._selenium:
            raise ValueError('Selenium has not been set')
        return self._selenium

    @selenium.setter
    def selenium(self, val):
        self._selenium = val

    def wait_until_visible(self, by, selector, timeout=10):
        start = time.time()
        stop = start + timeout

        while time.time() <= stop:
            try:
                el = self.selenium.find_element(by=by, value=selector)
                if el.is_displayed():
                    return

                raise Exception
            except Exception:
                time.sleep(0.1)

        raise ElementNotVisibleException

    def wait_until(self, until, timeout=40):
        waiting = True
        while waiting:
            try:
                WebDriverWait(self.selenium, timeout).until(until)
            except StaleElementReferenceException:
                waiting = True
            else:
                waiting = False

    def wait_until_not(self, until, timeout=20):
        WebDriverWait(self.selenium, timeout).until_not(until)

    def wait_until_notstale(self, callable):
        rval = None
        while not rval:
            with contextlib.suppress(StaleElementReferenceException):
                rval = callable()

        return rval

    def index(self):
        self.selenium.get(self.live_server_url)

    def show_view(self, view, kwargs=None, get_parameters=None):
        if isinstance(view, str) and ':' in view:
            raise ValueError('show_view with django url namespace views is not supported anymore')
        url = f'{self.live_server_url}{reverse_classname(view, kwargs=kwargs)}'
        if get_parameters:
            url = url + '?'
            for param, value in get_parameters.items():
                url = url + f'{param}={value}&'

        self.selenium.get(url)

    def find_element_by_text(self, text, parent=None):
        if parent:
            root = parent
        else:
            root = self.selenium

        return root.find_element(By.XPATH, f".//*[contains(text(), '{text}')]")

    def find_elements_by_text(self, text, parent=None):
        if parent:
            root = parent
        else:
            root = self.selenium

        return root.find_elements(By.XPATH, f".//*[contains(text(), '{text}')]")

    def find_element_by_href(self, href, parent=None):
        if parent:
            root = parent
        else:
            root = self.selenium

        all_links = root.find_elements(By.TAG_NAME, 'a')
        for cur_link in all_links:
            if cur_link.get_attribute('href') == href:
                return cur_link

        return None

    def find_element_by_partial_href(self, href, parent=None):
        if parent:
            root = parent
        else:
            root = self.selenium

        all_links = root.find_elements(By.TAG_NAME, 'a')
        for cur_link in all_links:
            if href in cur_link.get_attribute('href'):
                return cur_link

        return None

    def find_element_by_value(self, value, parent=None):
        if parent:
            root = parent
        else:
            root = self.selenium

        return root.find_element(By.XPATH, f".//*[@value = '{value}']")

    def find_element_by_partial_id(self, id, parent=None):
        id = id.replace(' ', '_')

        if parent:
            root = parent
        else:
            root = self.selenium

        return root.find_element(By.XPATH, f".//*[contains(@id, '{id}')]")

    def by_partial_id(self, id):
        id = id.replace(' ', '_')

        return (By.XPATH, f"//*[contains(@id, '{id}')]")

    def wait_click_and_wait(self, locator):
        old_page = self.selenium.find_element_by_tag_name('html')
        self.wait_until(EC.element_to_be_clickable(locator))
        element = self.selenium.find_element(*locator)
        element.click()
        self.wait_until(EC.staleness_of(element))
        self.wait_until(EC.staleness_of(old_page))

    def run_script(self, script):
        self.selenium.execute_script(script)

    def click_with_js(self, selector):
        script = f"document.querySelector('{selector}').click()"
        self.selenium.execute_script(script)

    def assert_element_not_found(self, element, by=By.XPATH):
        gone = False
        self.selenium.implicitly_wait(0)
        try:
            self.selenium.find_element(by=by, value=element)
        except NoSuchElementException:
            gone = True

        self.selenium.implicitly_wait(2)

        assert gone


@override_settings(CSRF_COOKIE_SECURE=False)
@override_settings(SESSION_COOKIE_SECURE=False)
class SeleniumTestCase(StaticLiveServerTestCase, SeleniumMixin):
    extra_options = {}
    ci_artifacts_folder = 'ci_artifacts'

    @classmethod
    def setUpClass(cls):
        xvfb_width = 3000
        xvfb_height = 3000
        if settings.USE_XVFB:
            cls.vdisplay = Xvfb(width=xvfb_width, height=xvfb_height)
            cls.vdisplay.start()
        else:
            cls.vdisplay = None

        cls.unittest_dir = Path(settings.UNITTEST_DIR, str(uuid4()))
        super().setUpClass()
        chrome_options = ChromeOptions()
        chrome_options.add_argument(f'user-data-dir={cls.unittest_dir / "chrome"}')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.binary_location = '/usr/bin/google-chrome'
        prefs = {}
        prefs['download.default_directory'] = str(cls.unittest_dir)
        chrome_options.add_experimental_option('prefs', prefs)

        for option in cls.extra_options:
            chrome_options.add_experimental_option(option, cls.extra_options[option])

        cls.selenium = WebDriver(options=chrome_options)

        cls.selenium.implicitly_wait(2)
        if settings.USE_XVFB:
            cls.selenium.set_window_size(xvfb_width, xvfb_height)
        else:
            cls.selenium.set_window_size(1400, 1000)

        cls.selenium.switch_to.window(cls.selenium.window_handles[-1])
        cls.selenium.find_element_by_id = lambda x: cls.selenium.find_element(By.ID, x)
        cls.selenium.find_element_by_name = lambda x: cls.selenium.find_element(  # noqa
            By.NAME, x
        )

        cls.selenium.find_element_by_xpath = lambda x: cls.selenium.find_element(  # noqa
            By.XPATH, x
        )

        cls.selenium.find_element_by_tag_name = lambda x: cls.selenium.find_element(  # noqa
            By.TAG_NAME, x
        )

        cls.selenium.find_element_by_class_name = lambda x: cls.selenium.find_element(  # noqa
            By.CLASS_NAME, x
        )

        cls.selenium.find_element_by_link_text = lambda x: cls.selenium.find_element(By.LINK_TEXT, x)

        cls.selenium.find_element_by_partial_link_text = lambda x: cls.selenium.find_element(By.PARTIAL_LINK_TEXT, x)

        cls.selenium.find_elements_by_id = lambda x: cls.selenium.find_elements(  # noqa
            By.ID, x
        )
        cls.selenium.find_elements_by_name = lambda x: cls.selenium.find_elements(  # noqa
            By.NAME, x
        )

        cls.selenium.find_elements_by_xpath = lambda x: cls.selenium.find_elements(  # noqa
            By.XPATH, x
        )

        cls.selenium.find_elements_by_tag_name = lambda x: cls.selenium.find_elements(  # noqa
            By.TAG_NAME, x
        )

        cls.selenium.find_elements_by_class_name = lambda x: cls.selenium.find_elements(  # noqa
            By.CLASS_NAME, x
        )

        cls.selenium.find_elements_by_partial_link_text = lambda x: cls.selenium.find_elements(By.PARTIAL_LINK_TEXT, x)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        if cls.vdisplay:
            cls.vdisplay.stop()
        if Path(cls.unittest_dir).exists():
            shutil.rmtree(cls.unittest_dir)

        super().tearDownClass()


class element_has_css_class:  # noqa N801
    """An expectation for checking that an element has a particular css class.

    locator - used to find the element
    returns the WebElement once it has the particular css class
    """

    def __init__(self, locator, css_class):
        self.locator = locator
        self.css_class = css_class

    def __call__(self, driver):
        element = driver.find_element(*self.locator)  # Finding the referenced element
        if self.css_class in element.get_attribute('class'):
            return element
        else:
            return False


class EC_upload_ok:  # noqa N801
    def __init__(self, element):
        self.element = element

    def __call__(self, webdriver):
        return (
            self.element.find_element(By.XPATH, '../../../../../..')
            .find_element(By.CLASS_NAME, 'alert')
            .get_attribute('class')
            == 'alert alert-success'
        )
