# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.contrib import messages


class TrashcanMixin:
    deleted_field = 'deleted'
    deleted_datetime_field = 'datetime_deleted'


class MessageAfterPostMixin:
    message_text = None
    message_priority = messages.INFO

    def get_message_text(self):
        return self.message_text

    def get_message_priority(self):
        return self.message_priority

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if self.get_message_text():
            messages.add_message(request, self.get_message_priority(), self.get_message_text())

        return response
