# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import contextlib

import django.contrib.auth.management
from django.apps import apps

standard_groups = {
    'User Administrator': {
        'perms': ['view_user', 'add_user', 'change_user', 'delete_user'],
        'verbose_name': 'Manage Users',
    }
}

old_groups = [
    'View Users',
    'Manage Users',
]


def create_groups(app_config, **kwargs):
    django.contrib.auth.management.create_permissions(apps.get_app_config('user_manager'), verbosity=1)
    permission = apps.get_model(app_config.label, 'Permission').objects
    group = apps.get_model(app_config.label, 'Group').objects

    delete_old_permissions(app_config, **kwargs)

    for current_group, attrs in standard_groups.items():
        temp_group = group.get_or_create(name=current_group)[0]
        temp_group.extendedgroup.section = 'user_manager'
        temp_group.extendedgroup.verbose_name = attrs['verbose_name']
        temp_group.extendedgroup.save()
        for perm in attrs['perms']:
            temp_group.permissions.add(permission.get(codename=perm))

    delete_old_groups(app_config, **kwargs)


def delete_old_groups(app_config, **kwargs):
    groups = apps.get_model(app_config.label, 'Group')
    for group in old_groups:
        with contextlib.suppress(groups.DoesNotExist):
            groups.objects.get(name=group).delete()


def delete_old_permissions(app_config, **kwargs):
    permission = apps.get_model(app_config.label, 'Permission').objects
    try:
        old_view_user_perm = permission.get(codename='view_user', content_type__model='user')
        old_view_user_perm.delete()
    except apps.get_model(app_config.label, 'Permission').DoesNotExist:
        pass
