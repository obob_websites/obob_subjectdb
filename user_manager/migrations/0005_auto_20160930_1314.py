# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-30 13:14
from __future__ import unicode_literals

import cuser.fields
import django.db.models.deletion
from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('user_manager', '0004_extendedgroup_verbose_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extendeduser',
            name='created_by',
            field=cuser.fields.CurrentUserField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE,
                                                related_name='user_manager_extendeduser_added_by',
                                                to=settings.AUTH_USER_MODEL),
        ),
        migrations.RenameField(
            model_name='extendeduser',
            old_name='created_by',
            new_name='added_by',
        ),
    ]
