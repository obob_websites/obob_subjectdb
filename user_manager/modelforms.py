# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import django.contrib.auth.models
import django.forms
from django import forms
from django.contrib import auth

import user_manager.authentication
import user_manager.forms.fields


class CreateOrEditUser(django.forms.ModelForm):
    class Meta:
        model = auth.get_user_model()
        fields = [
            'username',
            'is_superuser',
            'groups',
        ]
        widgets = {'groups': django.forms.widgets.CheckboxSelectMultiple}
        field_classes = {'groups': user_manager.forms.fields.ModelMultipleChoiceField}

    expiration_date = forms.DateField(label='Expiration Date', required=False)

    def __init__(self, **kwargs):
        self.old_username = ''

        super().__init__(**kwargs)
        if self.instance.pk:
            self.fields['username'].widget.attrs['readonly'] = True
            self.old_username = self.instance.username
            self.fields['expiration_date'].initial = self.instance.extendeduser.expiration_date

        self.fields['username'].help_text = 'Username of a user already existing in the LDAP database'
        self.fields['groups'].queryset = django.contrib.auth.models.Group.objects.order_by(
            'extendedgroup__priority', 'extendedgroup__section', 'name'
        )

    def clean(self):
        if self.old_username and self.old_username != self.cleaned_data['username']:
            self.add_error('username', 'You changed the username somehow. This should not have happened!')
            return super().clean()

        ldap_instance = user_manager.authentication.LDAPBackend()
        user_model = auth.get_user_model()
        existing_user = user_model.objects.filter(username__exact=self.cleaned_data['username']).exists()
        user_object = ldap_instance.populate_user(self.cleaned_data['username'])
        if not user_object:
            self.add_error('username', 'User does not exist in LDAP database')
        elif not existing_user:
            user_object.delete()
        return super().clean()

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        instance.extendeduser.expiration_date = self.cleaned_data['expiration_date']
        instance.extendeduser.save()

        return instance
