# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime

import django.contrib.auth.models
import django.db.models.signals
import django.dispatch
from django.conf import settings
from django.db import models
from django.db.models import Q

from th_django.models.mixins import DateTimeAddedBy


class ExtendedUser(DateTimeAddedBy):
    class Meta(DateTimeAddedBy.Meta):
        permissions = (('view_user', 'Can view users'),)

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    expiration_date = models.DateField(verbose_name='Expiration Date', null=True, blank=True)

    @staticmethod
    @django.dispatch.receiver(
        django.db.models.signals.post_save,
        sender=settings.AUTH_USER_MODEL,
        dispatch_uid='user_manager.extended_user.post_save',
    )
    def on_user_save(sender, **kwargs):
        e_user = ExtendedUser.objects.get_or_create(user_id=kwargs['instance'].id)[0]
        e_user.save()

    @property
    def is_expired(self):
        if not self.expiration_date:
            return False

        return datetime.date.today() > self.expiration_date and not self.user.is_superuser

    @property
    def can_login(self):
        return (not self.is_expired) and self.user.is_active

    @staticmethod
    def can_login_filter():
        return Q(
            Q(is_active=True)
            & Q(
                Q(extendeduser__expiration_date__isnull=True)
                | Q(extendeduser__expiration_date__gt=datetime.date.today())
            )
        )


class ExtendedGroup(models.Model):
    group = models.OneToOneField(django.contrib.auth.models.Group, on_delete=models.CASCADE)
    section = models.CharField(max_length=20, default='undefined')
    priority = models.PositiveSmallIntegerField(default=100)
    verbose_name = models.CharField(max_length=30, default='undefined')

    @staticmethod
    @django.dispatch.receiver(
        django.db.models.signals.post_save,
        sender=django.contrib.auth.models.Group,
        dispatch_uid='user_manager.extended_group.post_save',
    )
    def on_group_save(sender, **kwargs):
        e_group = ExtendedGroup.objects.get_or_create(group_id=kwargs['instance'].id)[0]
        e_group.save()
