/*
 * Copyright (c) 2016-2025, Thomas Hartmann
*
* This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
*
*    obob_subjectdb is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    obob_subjectdb is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
*/

import 'jquery';
import { replaceParam } from 'url-params-helper';

$('#show_inactive_cb').change(function (evt) {
    window.location.href = replaceParam('show_inactive', $(this).is(':checked'));
});

document.getElementById('filter_form').addEventListener('submit', (evt) => {
    evt.preventDefault();
    const all_exp_input = document.createElement('input');
    all_exp_input.type = 'hidden';
    all_exp_input.name = 'show_inactive';
    all_exp_input.value = document.getElementById('show_inactive_cb').checked;
    evt.target.append(all_exp_input);
    evt.target.submit();
});
