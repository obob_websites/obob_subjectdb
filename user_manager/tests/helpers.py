# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime
import time

from django.contrib import auth
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC  # noqa NB812

import obob_subjectdb.tests.helpers
import user_manager.authentication
from th_django.templatetags.th_django import date_format


class LoginLogoutMixin(obob_subjectdb.tests.helpers.UITestMixin):
    def login(self, user, may_fail=False):
        self.wait_until(EC.visibility_of_element_located((By.TAG_NAME, 'body')))
        navbar_toggle_button = self.selenium.find_element_by_id('navbar-button')
        if navbar_toggle_button.is_displayed():
            time.sleep(1)
            navbar_toggle_button.click()
        self.wait_until(EC.element_to_be_clickable((By.ID, 'login_dropdown')))
        login_button = self.selenium.find_element_by_id('login_dropdown')
        time.sleep(1)
        login_button.click()
        self.wait_until(EC.visibility_of_element_located((By.NAME, 'username')))
        self.selenium.find_element_by_name('username').send_keys(user)
        self.selenium.find_element_by_name('password').send_keys(user)
        self.selenium.find_element_by_name('submit_login').click()
        if not may_fail:
            self.wait_until(EC.visibility_of_element_located((By.CLASS_NAME, 'alert-dismissable')))

    def login_fast(self, user):
        self.client.login(username=user, password=user)
        # self.client.force_login(
        #     auth.get_user_model().objects.get(username=user)
        # )
        cookie = self.client.cookies['sessionid']
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
        self.selenium.refresh()

    def logout(self):
        navbar_toggle_button = self.selenium.find_element_by_id('navbar-button')
        if navbar_toggle_button.is_displayed():
            navbar_toggle_button.click()
        self.wait_until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Logout')))
        self.selenium.find_element_by_partial_link_text('Logout').click()

    def add_user(self, username, additional_groups=None, expiration_date=None, is_superuser=False):
        self.show_view('user_manager.views.manage.CreateUser')
        self.selenium.find_element_by_id('id_username').send_keys(username)
        if expiration_date:
            self.selenium.find_element_by_id('id_expiration_date').send_keys(expiration_date.strftime(date_format()))

        if additional_groups:
            for group in additional_groups:
                self.find_element_by_text(group).click()

        if is_superuser:
            self.find_element_by_text('Superuser status').click()

        self.selenium.find_element_by_id('form').submit()

    def add_user_db(self, username, additional_groups=None, expiration_date=None, is_superuser=False):
        new_user = auth.get_user_model()(username=username)
        new_user.is_superuser = is_superuser
        new_user.save()

        if expiration_date:
            new_user.extendeduser.expiration_date = expiration_date
            new_user.extendeduser.save()

        if additional_groups:
            for group in additional_groups:
                new_user.groups.add(auth.models.Group.objects.get(name=group))

        new_user.save()

        ldap_instance = user_manager.authentication.LDAPBackend()
        user_object = ldap_instance.populate_user(username)
        user_object.save()

        return user_object

    def delete_user(self, username):
        usermodel = auth.get_user_model()
        user_id = usermodel.objects.get(username=username).id
        self.show_view('user_manager.views.manage.ListUsers')

        self.wait_until(EC.element_to_be_clickable((By.NAME, 'delete_button')))
        self.wait_until_notstale(lambda: self.selenium.find_element_by_name('delete_button'))

        delete_button = [
            x
            for x in self.selenium.find_elements_by_name('delete_button')
            if x.get_attribute('data-subject') == str(user_id)
        ][0]  # noqa
        delete_button.click()
        self.wait_for_table()
        self.wait_click_and_wait((By.ID, 'okButton'))

    def delete_user_db(self, username):
        auth.get_user_model().objects.get(username=username).delete()

    def edit_user(self, username, additional_groups=None):
        user_model = auth.get_user_model()
        self.show_view(
            'user_manager.views.manage.EditUser', kwargs={'user_id': user_model.objects.get(username=username).id}
        )

        all_groups = self.selenium.find_element_by_id('id_groups')
        for group_element in all_groups.find_elements(By.TAG_NAME, 'div'):
            input_element = group_element.find_element(By.TAG_NAME, 'input')
            group_name = group_element.find_element(By.TAG_NAME, 'label').text
            if input_element.is_selected() is not (group_name in additional_groups):
                input_element.click()

        self.selenium.find_element_by_id('form').submit()

    def edit_user_db(self, username, additional_groups=None):
        cur_user = auth.get_user_model().objects.get(username=username)
        cur_user.groups.clear()

        if additional_groups:
            for group in additional_groups:
                cur_user.groups.add(auth.models.Group.objects.get(name=group))

        cur_user.save()

    def _set_expiration_date(self, days):
        return datetime.date.today() + datetime.timedelta(days=days)
