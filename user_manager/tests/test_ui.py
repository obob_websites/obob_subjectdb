# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from django.contrib import auth
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

import th_django.test
import user_manager.views.login
import user_manager.views.manage
import user_manager.views.permission_errors

from .helpers import LoginLogoutMixin


class LoginTests(th_django.test.SeleniumTestCase, LoginLogoutMixin):
    def test_login_logout(self):
        self.index()
        self.login('b1019547')
        self.assertIsNotNone(self.selenium.find_element_by_link_text('Logout b1019547'))
        self.logout()
        self.assertIsNotNone(self.selenium.find_element_by_id('login_dropdown'))

    def test_wrong_login(self):
        self.index()
        self.login('wronguser')
        self.find_element_by_text('You are not logged in')
        self.find_element_by_text('Login failed.')

    def test_useradd(self):
        usermodel = auth.get_user_model()
        with self.assertRaises(usermodel.DoesNotExist):
            usermodel.objects.get(username='thht')
        self.show_view(user_manager.views.manage.CreateUser)
        self.assertIn('/NotLoggedIn/?', self.selenium.current_url)
        self.login('b1019547')
        self.add_user('thht')
        user = usermodel.objects.get(username='thht')
        self.assertEqual(user.extendeduser.added_by.username, 'b1019547')
        self.logout()

    def test_add_user_not_in_ldap(self):
        usermodel = auth.get_user_model()
        self.show_view(user_manager.views.manage.CreateUser)
        self.login('b1019547')
        self.add_user('thht_not_in_db')
        with self.assertRaises(usermodel.DoesNotExist):
            user = usermodel.objects.get(username='thht_not_in_db')  # noqa
        self.logout()

    def test_delete_user(self):
        usermodel = auth.get_user_model()
        self.show_view(user_manager.views.manage.CreateUser)
        self.login('b1019547')
        self.add_user('thht')
        self.add_user('thht2')
        self.delete_user('thht2')

        self.assertFalse(usermodel.objects.get(username='thht2').is_active)

        with self.assertRaises(NoSuchElementException):
            self.find_element_by_text('thht2')

        self.assertIs(len(self.selenium.find_elements_by_tag_name('tr')), 3)

        self.wait_click_and_wait((By.ID, 'show_inactive_cb'))

        self.assertIs(len(self.selenium.find_elements_by_tag_name('tr')), 2)

        self.find_element_by_text('thht2')

        self.wait_click_and_wait((By.NAME, 'edit'))
        self.selenium.find_element_by_id('form').submit()

        self.assertTrue(usermodel.objects.get(username='thht2').is_active)

    def test_db_helpers(self):
        self.add_user_db('thht')
        self.add_user_db('thht2', ['Lab Manager', 'Investigator/Technical Staff'])

        self.assertEqual(auth.get_user_model().objects.get(username='thht').first_name, 'Max')

        self.index()
        self.login('thht')
        self.assertIsNotNone(self.selenium.find_element_by_link_text('Logout thht'))

        self.logout()
        self.login('thht2')
        self.assertIsNotNone(self.selenium.find_element_by_link_text('Logout thht2'))
        with self.assertRaises(NoSuchElementException):
            self.find_element_by_text('User Manager')

        self.delete_user_db('thht')
        self.logout()
        self.login('thht', True)
        self.assertIsNotNone(self.find_element_by_text('You are not logged in'))
        self.assertIsNotNone(self.find_element_by_text('Login failed.'))

        self.index()
        self.edit_user_db('thht2', ['User Administrator', 'Lab Manager'])

        self.login('thht2')
        self.assertIsNotNone(self.find_element_by_text('User Manager'))

    def test_expire_user(self):
        self.index()
        self.login('b1019547')

        self.add_user('thht', expiration_date=self._set_expiration_date(days=10))
        self.add_user('thht2', expiration_date=self._set_expiration_date(days=-10))
        self.add_user('thht3', expiration_date=self._set_expiration_date(days=-10), is_superuser=True)
        self.add_user('thht4', expiration_date=self._set_expiration_date(days=10), is_superuser=True)
        self.show_view(user_manager.views.manage.ListUsers)
        self.assertIs(len(self.selenium.find_elements_by_tag_name('tr')), 5)

        self.wait_click_and_wait((By.ID, 'show_inactive_cb'))

        self.assertIs(len(self.selenium.find_elements_by_tag_name('tr')), 2)

        self.logout()
        self.login('thht')
        self.assertIsNotNone(self.find_element_by_text('Login successful.'))
        self.logout()
        self.login('thht2')
        self.assertIsNotNone(self.find_element_by_text('Login failed.'))
        self.login('thht3')
        self.assertIsNotNone(self.find_element_by_text('Login successful.'))
        self.logout()
        self.login('thht4')
        self.assertIsNotNone(self.find_element_by_text('Login successful.'))

    def test_expire_user_db(self):
        self.index()
        self.login('b1019547')

        self.add_user_db('thht', expiration_date=self._set_expiration_date(days=10))
        self.add_user_db('thht2', expiration_date=self._set_expiration_date(days=-10))
        self.add_user_db('thht3', expiration_date=self._set_expiration_date(days=-10), is_superuser=True)
        self.add_user_db('thht4', expiration_date=self._set_expiration_date(days=10), is_superuser=True)
        self.logout()
        self.login('thht')
        self.assertIsNotNone(self.find_element_by_text('Login successful.'))
        self.logout()
        self.login('thht2')
        self.assertIsNotNone(self.find_element_by_text('Login failed.'))
        self.login('thht3')
        self.assertIsNotNone(self.find_element_by_text('Login successful.'))
        self.logout()
        self.login('thht4')
        self.assertIsNotNone(self.find_element_by_text('Login successful.'))
