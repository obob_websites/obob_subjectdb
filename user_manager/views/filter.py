#  Copyright (c) 2016-2025, Thomas Hartmann
#
#  This file is part of the OBOB Subject Database Project,
#  see: https://gitlab.com/obob/obob_subjectdb/
#
#   obob_subjectdb is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   obob_subjectdb is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import django_filters
from django.contrib import auth


class UserFilter(django_filters.FilterSet):
    class Meta:
        model = auth.models.User
        fields = ['username', 'name']

    username = django_filters.CharFilter('username', 'icontains')
    name = django_filters.CharFilter(method='name_filter', label='Name')

    def name_filter(self, qs, name, value):
        return qs.filter(first_name__icontains=value) | qs.filter(last_name__icontains=value)
