# Copyright (c) 2016-2025, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

from django.contrib import auth
from django_auto_url.urls import reverse_classname_lazy
from django_tables2 import A, DateColumn, Table

from th_bootstrap.django_tables2.columns import Column, DeleteColumn, EditColumn, Many2ManyColumn


class ListUsers(Table):
    class Meta:
        model = auth.get_user_model()
        fields = ['username', 'first_name', 'last_name', 'email', 'is_superuser', 'groups']

    username = Column(verbose_name='Username')
    first_name = Column(verbose_name='First Name')
    last_name = Column(verbose_name='Last Name')
    email = Column(verbose_name='Email', orderable=False)
    is_superuser = Column(verbose_name='Is Superuser', orderable=False)
    groups = Many2ManyColumn(verbose_name='Groups')
    expiration_date = DateColumn(verbose_name='Expiration Date', accessor=A('extendeduser__expiration_date'))
    delete = DeleteColumn(
        'id',
        'confirm_delete',
        reverse_classname_lazy('user_manager.views.manage.DeleteUser', return_url_name=True),
        needs_permission='auth.delete_user ',
    )
    edit = EditColumn(
        reverse_classname_lazy('user_manager.views.manage.EditUser', return_url_name=True),
        'id',
        needs_permission='auth.change_user',
    )

    def __init__(self, data, **kwargs):
        show_inactive = kwargs.pop('show_inactive', 'False')
        self.base_columns['delete'].visible = not show_inactive

        super().__init__(data, **kwargs)
