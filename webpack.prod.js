const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const fs = require('fs');
const path = require('path');
const common = require('./webpack.common');

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    keep_fnames: false,
                },
            }),
        ],
        usedExports: true,
        splitChunks: {
            chunks: 'all',
        },
        sideEffects: true,
    },
});
